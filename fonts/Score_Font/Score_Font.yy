{
    "id": "c9135b3e-2090-4e9e-af99-54c0365fb3c1",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "Score_Font",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Open Sans",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "428fead7-42dc-4c55-adcd-280f8044396c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cddafd92-2627-4919-8319-43bb6450260a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 171,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a9dd660d-28dc-47f1-a1cb-e6e76b2c58a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 103,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bf8e6ced-3fd7-481c-a916-2bbff91714c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c755063c-ede1-4c6d-b2d4-0bfb3d19026d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "323b786d-6a2e-43c0-82d2-a711dab41e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bffc74bd-b55d-4307-99dd-1dcb30330070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8e3e12b9-bf01-49d5-bb60-0a06d8dd86d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 218,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5cfadeb3-07d8-4219-bf86-4064fa225317",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 143,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d7fdfba8-b6a8-455c-99c2-952bee4a25eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 157,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "756f702a-8f09-4bba-97cf-454d11871393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "297ccb16-eff3-4ade-89e3-9d7b8416b9c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "080e01e2-ff0c-4677-9bc4-6109853b806b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 207,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "926c0cc4-8639-40b0-ac29-06e5c4aca8d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 150,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f3212e1c-cbb8-4804-a364-8c5ab61b647f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 183,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6b147c89-a782-414f-8829-ed3b4f93ff48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 68,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f121184f-b0cb-4c08-975a-6fb559e9549c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2de20b0c-92a3-49df-9ce3-2bfca5e6c7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "660764a7-ba11-442b-9d07-a53cee58838d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "50821c1e-cd66-49fb-b20a-bf00269a47d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9eb54aa3-e076-4740-bbdd-888be1c8ce60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3fe6bcf4-033c-446b-8f05-4b15a22ac760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "aca591a7-8bd0-4ba9-a5fb-767f34c576ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "778e62c6-4c56-49f8-b35a-6f4dc91d190e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1b16ece6-aa2a-438c-b1d7-1a083a8fa6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e66db9c7-5083-4f48-8d3f-c13be73f6d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6ab5dd17-0b74-43fa-824e-71b1aa3b1053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 195,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0c606544-dbb5-4444-8c58-9000d7c2544b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3b4fec12-d164-498b-b3ba-c268e18c3b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6ac9dc1a-a1f4-41e1-9b51-b7e792f0da76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "998f9094-d0fc-4a26-878a-178718b51f51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cbc0f5d5-a77f-4980-8605-f0de23e905f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "76b8c363-9bf0-46c7-b1e4-b1127f803e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "dade5a76-5d38-4fbb-9ec1-ca7398ce5faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2f2ddda0-5109-42ab-b101-5052223d218e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3505a008-a400-4c58-86d8-d22a5740cb1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8a95f02b-d87a-400c-8be9-772bb6ee3a2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b169744c-a5be-4db7-a058-24b3541536ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "592b58fd-2c4b-41b5-864a-6e7f70ee341e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c829819b-8f5c-4ff2-b6d7-01d3dede2c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1c7430e2-3575-4e3c-8077-93845e5852cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 26
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "16aaf76b-dc69-49c7-8433-09d96ccbb043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 233,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ade36fec-3ba3-42d9-ba39-22c34d3abde1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 95,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f8044d87-4c54-4752-844e-00b94eb818a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 26
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a65394ba-3a22-45ec-b6e1-09860a9f37fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 241,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a3ac2e01-8c4f-4bc3-9bf3-0e9032136e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "da595ba4-5e79-4d43-a992-c678205248f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6078da69-0e24-4533-b4b7-8e5590ded4a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3d37ee8b-0799-47a8-b6f1-70d235c27bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 109,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ed0ea387-7420-45a9-a02a-21b0af27eb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9877dac8-1c9f-411d-919c-1caaa83ce7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "73df7faf-aa1b-4da8-98f6-19955941b59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 164,
                "y": 26
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "02a023b2-a088-484a-bee2-ba8140bae1a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 175,
                "y": 26
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2b416400-75e3-4311-89cb-760de110d651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "153c39c0-0617-458f-86fd-659b898ef590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "47ecfb52-9065-4b88-ad7b-11ea0004b74f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c1f1c9e5-9e28-430d-8572-a9f967982903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6783005e-a274-4330-ac2b-97ec996f9ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3c852599-edb2-426b-9a4b-ca18075d7cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 241,
                "y": 26
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "38541e92-aac6-4437-8f37-5f7c2a6bbf73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 189,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ed970b08-2594-411d-b55a-983adbdf1e08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 74
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "411f644b-c021-4804-a60c-335b1c1a224b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 164,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "90fdea8c-cbab-48b9-9b4f-20a375aa0570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 153,
                "y": 26
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b8aca8ea-2126-4498-be63-f43aaab8e05c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "93b2d754-22a9-4e87-a898-e6754fceefd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 127,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2ce9bc56-23ca-4cae-95be-e1b7abd882b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dfe056de-ce13-4789-a656-f846599fe1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 142,
                "y": 26
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9a3195cd-118c-4004-abff-4f62f0bd4b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "20e477ed-54db-4fe2-867a-93eda4e48a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 26
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "579d92ad-4fda-40ba-8555-f747cdeb1f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 219,
                "y": 26
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ae8f21e7-f0a4-4b49-94b0-fc1a2ef1be4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 86,
                "y": 74
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "141e232a-9928-472b-ae8c-b5e4417f10e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 208,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "41a1722f-8ebb-4df0-94e1-34e50a1c54be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 197,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fec93d0f-e0f4-4ef2-92f7-e72320595d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 223,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6b246249-c9ab-478f-a271-88a6169b3dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 111,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "dbdaa1d3-9255-4f0f-b39a-0386b62f786c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4adcfc76-1a32-46a4-8169-5af53d621c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dd17c965-8f66-4862-9fd2-b9078312948c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8c839b6f-65a4-4e4b-b263-22bb4e440cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a4c10fa4-7382-45ce-a8eb-6474224b7e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c6dcfc25-5eaf-4530-a577-2b6852322cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 131,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9db9c87c-6507-4fdc-a4c7-996273ec7705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eb68e6a5-2df6-4506-85fd-49f2705bf991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 135,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a42c791c-d467-4787-b771-f72df713113a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f64d69dc-6cf6-4775-8666-7614219b45f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b800a650-909f-4a9f-a524-0608e89e34b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "15db2a5c-100d-46ea-8b6f-ad9c92387de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "696f9013-a796-4cf7-a566-8d9fe7d55ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fdb5faf4-ccbb-4399-9857-84a1b6d89462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 62,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0424a20f-8303-4eef-a8d0-9e2b76c0b42b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "653edf6a-2ab7-4590-a61a-d9d99aad7291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6fd67f62-64f0-4043-822d-3ed51bc4907e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "242f1b67-0547-4623-b101-ba871302adb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 228,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "02f9e818-f5e7-4c91-b50d-2b1b4412107b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 59,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "51b6c9cd-5ea8-4b3c-b739-624dddc6eae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 50
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "e96711bf-1c07-45e0-9d5c-8d0302ea8411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 65
        },
        {
            "id": "71e82cb7-4885-4d10-aef3-f49d51c281ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 99
        },
        {
            "id": "d944c965-5bbb-48f1-848b-7914d7ab41a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "a942bf31-6f9c-4a88-8e52-7b65ab83aac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 101
        },
        {
            "id": "6fa13f43-e9f4-49f0-bdb6-eda9e5781029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 111
        },
        {
            "id": "2ce79bfc-fa37-44e4-a718-b37a916dfe07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "01219cbf-e73c-425d-b545-341ced42c513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 192
        },
        {
            "id": "b71bc46b-fc48-45d6-aa4b-dcb4521e62d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 193
        },
        {
            "id": "4273a12d-eb6b-4c27-b482-833790f49b29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 194
        },
        {
            "id": "87d43223-aba0-450e-8de6-6882341b448f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 195
        },
        {
            "id": "1c397ebf-4fca-4b29-a9b7-e55c2621e7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 196
        },
        {
            "id": "0975500c-66b0-4ee0-bb5e-3536d2edd8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 197
        },
        {
            "id": "10c5d2d5-a9e7-4574-b514-259de9e0bf3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 224
        },
        {
            "id": "a0a4e30a-bb01-422c-bf54-164e45b30445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 231
        },
        {
            "id": "6680dfad-0d92-4cea-a9d1-c4fd8978ef1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 232
        },
        {
            "id": "2eb56b3a-0cbf-4c70-bd9e-19804de064e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 233
        },
        {
            "id": "dbc460a1-7472-4a7d-be80-14df90a163cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 234
        },
        {
            "id": "ba6f5152-3ad8-4101-858c-d3cc689c0bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 235
        },
        {
            "id": "af6d4a1b-81ff-48ff-b77a-c988d9a6b723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 242
        },
        {
            "id": "a518ec2c-c3ba-467b-8502-2c99fb986867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 243
        },
        {
            "id": "74a0c83c-5c0d-48cf-a248-b8e88550fdbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 244
        },
        {
            "id": "cbdf6e02-fab5-4a15-8868-94f8aeefde2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 245
        },
        {
            "id": "6ee17c20-6f26-4b50-9fc6-b2a468ff2f39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 246
        },
        {
            "id": "936442be-27d0-48b1-aaa6-860f82af18e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 248
        },
        {
            "id": "ed9a96a0-b779-4701-bd6f-aaaeb41f8e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 256
        },
        {
            "id": "f3751e47-3798-4deb-9ea2-dced42647f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 258
        },
        {
            "id": "2ba1cc6d-bc70-47fc-9af2-b3f2480f0cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 260
        },
        {
            "id": "50f1c4e2-7431-44d8-ae69-5429d058e57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 263
        },
        {
            "id": "9ea502fe-dab6-486f-b18b-e6dd145a8953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 265
        },
        {
            "id": "981ea97e-7158-462d-bad4-a8855d89b57b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 267
        },
        {
            "id": "c637f7b0-9b44-460c-9ec9-e22e6bfd5cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 269
        },
        {
            "id": "bef8dc07-bfe8-448e-92b6-f40ca50cae8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 271
        },
        {
            "id": "3acdd720-b090-4769-af69-e29a9ca69e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 273
        },
        {
            "id": "22ac815a-74aa-4a9b-b5bc-213120829f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 275
        },
        {
            "id": "e1b9b345-a5e8-4b55-88c0-6269961da404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 277
        },
        {
            "id": "64780e97-5888-44dd-8ec0-0e77e6d8fae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 279
        },
        {
            "id": "0580ca60-cc21-439f-b860-12332601cdd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 281
        },
        {
            "id": "1183b762-c4a5-4bf8-ab36-34a2f4926fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 283
        },
        {
            "id": "bbe8e52f-4ab5-47f5-a436-2acf6e780356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 333
        },
        {
            "id": "ea1156bf-4942-4fea-8733-3e3626bc4e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 335
        },
        {
            "id": "369b66b6-1a20-4d12-aad0-5b0bc7ccf799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 337
        },
        {
            "id": "1edacb44-924b-48de-a307-e4ccdb3fe129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 339
        },
        {
            "id": "df78b3ea-fdd8-4936-a209-73b5c7e6c26c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 417
        },
        {
            "id": "07113d0a-bc19-4102-9a5e-fc58da170047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 506
        },
        {
            "id": "e411d448-dfe5-40dc-aa6b-f25ce4214bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 511
        },
        {
            "id": "c4a98782-9cd0-41fe-a5bd-491c5cd29411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 902
        },
        {
            "id": "630ea36b-69b7-4317-bf9f-9662a61251e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 913
        },
        {
            "id": "3b202a6e-a3b3-4b1e-a763-467cadf01ecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 916
        },
        {
            "id": "cdbf254e-a422-4773-bacc-08c75754990c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 923
        },
        {
            "id": "37043447-5850-45c9-81bb-a5c4fcd6a4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1033
        },
        {
            "id": "d58899ca-e59b-4248-b24e-1e21042065fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1040
        },
        {
            "id": "860b6a13-ccce-4eab-b7bc-45b474abd7d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1044
        },
        {
            "id": "6f13489c-bd96-45d3-a91c-9d3fbade1de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1051
        },
        {
            "id": "eeb1c78b-d8ef-4e80-94f0-03d1e71061bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1076
        },
        {
            "id": "79a6abc2-a2df-4f05-baef-d6ee7b91487e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1077
        },
        {
            "id": "51cbfe25-a95f-4b97-8a3d-34c2959c346c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1083
        },
        {
            "id": "6fba5392-86f0-4f3c-8810-d436cbaae3e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1086
        },
        {
            "id": "5a8be620-8784-4904-8bae-f4bcc1ceca4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1089
        },
        {
            "id": "24d5ef26-c359-4c5a-8590-3cc6961a9a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1092
        },
        {
            "id": "dcf57d72-1ee6-4aa1-a07d-f352a97f555d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1104
        },
        {
            "id": "28cb7b0f-3ffd-4ef1-a833-13e06ee3797e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1105
        },
        {
            "id": "535d37b2-2223-44bc-8502-520e0c5a6b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1108
        },
        {
            "id": "676025da-b373-4217-94ac-ca0fdac86e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1113
        },
        {
            "id": "d821d39a-b143-4c1d-806f-a6a7e4b4a15b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1126
        },
        {
            "id": "8452c6d6-2e5b-4b87-9c36-d6e942e53845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1127
        },
        {
            "id": "241db893-7e37-42b1-b7d7-9fd188fd1b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1139
        },
        {
            "id": "8cbae66c-f870-4fc4-8d59-c69076c9b211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1145
        },
        {
            "id": "33d00cb3-72ca-4932-b09b-89e1ad204f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1147
        },
        {
            "id": "56b8ecad-1543-4285-b645-1c2ba59b6af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1149
        },
        {
            "id": "ae17f295-9b17-4736-8876-e846a81ba8d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1153
        },
        {
            "id": "33ba726e-23ee-4e9b-be5f-8d095cfc3dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1193
        },
        {
            "id": "0fc5f439-c00e-4151-9fa7-91e1c6415067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1195
        },
        {
            "id": "28d8b733-a426-480b-b13f-177eb07af4d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1221
        },
        {
            "id": "45b5c828-86fa-47e2-8141-1d21acf8e3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1222
        },
        {
            "id": "5b013772-c977-47c7-84f5-8580c5f59981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1232
        },
        {
            "id": "6b614ec6-d87c-483c-bf0b-7c7884994a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1234
        },
        {
            "id": "e4e2b411-2e96-4d54-b3ab-4d8f4040cf8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1236
        },
        {
            "id": "0bd92842-6407-4cc9-a67f-e546fd964c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1239
        },
        {
            "id": "e54442f9-335c-4c05-94c1-d02065a573da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1255
        },
        {
            "id": "8f1dfb7c-fb69-49ff-a4f0-9eba8e358915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1257
        },
        {
            "id": "0ddb099a-8298-483e-b797-465686894bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1259
        },
        {
            "id": "1e863a8e-9b1c-4f0f-9a7b-70fd1d741052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1280
        },
        {
            "id": "2d4b3d1f-c7f9-44f8-b666-3004053613ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1281
        },
        {
            "id": "6ef11622-7a2a-4692-a9ad-510a3db55792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1282
        },
        {
            "id": "14968db6-c20f-46f3-8567-9a175266f5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1283
        },
        {
            "id": "d27b2ad1-571b-40f5-a6c9-99be069d3c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1288
        },
        {
            "id": "f02f1d23-0bb2-4893-ad95-facef690a0d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1289
        },
        {
            "id": "dadba31e-1653-4899-aca9-bc6939ddd09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1293
        },
        {
            "id": "572c9a17-a5b3-445d-bf2c-b5bc18665e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1297
        },
        {
            "id": "5a75f0be-396e-4376-aae1-f3ee276c91eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1298
        },
        {
            "id": "80ff0631-fff8-47b8-b4d0-36ecb46312c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1299
        },
        {
            "id": "05c76c55-f09d-4445-a8e4-e28cda55e9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7680
        },
        {
            "id": "e11fabad-0369-42e5-9580-f46fa62e0ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7840
        },
        {
            "id": "8083c4e7-ae28-4a21-91f0-aea09d53f69c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7842
        },
        {
            "id": "7079f6c9-13da-488c-98d9-07f947399bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7844
        },
        {
            "id": "fb06e660-acb3-45aa-831c-6304b0efeca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7846
        },
        {
            "id": "79ebd181-d21b-42a3-9ce0-596588497e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7848
        },
        {
            "id": "d90a7c1a-3361-4ad1-899a-c498aba60036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7850
        },
        {
            "id": "48a0ba6c-8e88-4bcb-9b7c-c3fecda2c5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7852
        },
        {
            "id": "1777d718-0f86-4edc-a43c-7f7382b7ceb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7854
        },
        {
            "id": "669d9aa7-5215-440c-b266-5a7fee5efe23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7856
        },
        {
            "id": "e03dbf79-60fb-43c7-86fa-b5d5dd481a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7858
        },
        {
            "id": "0ffbc429-c81f-4b67-9909-3b1c4eda3b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7860
        },
        {
            "id": "87693985-9b84-4cc7-a6bf-44a01f4c0ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7862
        },
        {
            "id": "3e0e0888-5373-46cd-9562-d63fb55bce7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7865
        },
        {
            "id": "cb04d6bd-01cc-47dd-a842-56886165f876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7867
        },
        {
            "id": "ab7bdab0-3577-425a-b765-36a3f1149751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7869
        },
        {
            "id": "6b90925f-c8ad-447f-bd9b-bec61165cc68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7871
        },
        {
            "id": "36d0a884-a68d-4968-ad57-7510a53d6e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7875
        },
        {
            "id": "5196bfa0-8a77-47c5-bc5b-2a08ead9ec85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7877
        },
        {
            "id": "4d30755c-4fa6-4d9d-8f55-a856049277c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7879
        },
        {
            "id": "e98380a7-b01d-41ce-9c3c-5c40ea86a461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7885
        },
        {
            "id": "635b44d7-c744-40c3-9037-b52abccf32be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7887
        },
        {
            "id": "4d2978e4-d5fe-420e-aab3-3c90f50e1add",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7889
        },
        {
            "id": "7897f701-acb3-4b49-bc06-9edd8aa56917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7893
        },
        {
            "id": "8fa4c711-a152-4f10-bd8d-a9a88c0d1225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7895
        },
        {
            "id": "c506cc06-74e9-4bd7-9deb-00a94c3daf1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7897
        },
        {
            "id": "a77f0806-81ab-4f2b-a996-6c007dba179a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7899
        },
        {
            "id": "4887ad39-e697-4c88-8359-a4ac1c8e0c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7901
        },
        {
            "id": "1b80e3b0-f378-4915-9eb5-d20096fb758e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7903
        },
        {
            "id": "398d4e38-f00c-40a2-8e68-92679eca4d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7905
        },
        {
            "id": "5d252a9f-f15e-494f-b2bc-9c029340ab1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7907
        },
        {
            "id": "d7a4f619-2533-4e56-8206-834288b95599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "a933e0c0-10fd-47c7-874e-ff3f9848e89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 99
        },
        {
            "id": "0e9e44be-d370-45be-9b2e-e058e1114c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "4fb322f8-3e97-4679-8ff9-b8f06a27458c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 101
        },
        {
            "id": "5c24600b-cab6-4750-b062-fe25e3d5bd64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 111
        },
        {
            "id": "a63ca93b-c3fc-4cc4-aa7d-b8df67db9505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "63ee9ca2-cf04-4d5e-8679-d1e3b3dd1d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "13bea3f3-3704-4bd6-a0b6-ac352038e41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "1b754f19-f674-4127-8dff-fd4bfddc275a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "820148b0-4f6a-45d4-8808-bdbc589409fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "b375eedf-d6cc-49a4-a55d-e2300aa87657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "bd144996-425f-4f41-8e27-5ecc69012961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "8e1c5e7f-d045-4429-a71b-0b24176d0eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 224
        },
        {
            "id": "5caed57b-0ba9-4a08-8ca3-c093ade28e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 231
        },
        {
            "id": "d84ac34d-c22d-42c2-a730-500fd7750c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 232
        },
        {
            "id": "b5814cd6-54c9-4dd8-9cd3-c4578520c49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 233
        },
        {
            "id": "b13b209c-f3fb-4aaf-87e5-f28633b0a0a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 234
        },
        {
            "id": "fd00b285-08e0-49a1-90dd-e22655df3f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 235
        },
        {
            "id": "21e32fd6-6f4d-44e6-b768-30a2f738996c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 242
        },
        {
            "id": "b64327ab-1e05-4795-80c7-350e21a02fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 243
        },
        {
            "id": "8972a029-2d8a-4b9f-856a-02e16d5bb6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 244
        },
        {
            "id": "4358a1eb-9c58-401c-b622-f1b9223ad794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 245
        },
        {
            "id": "5794df31-7c3a-4231-864d-27a3f86e9289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 246
        },
        {
            "id": "0ef64bb7-f377-471c-b935-a039d00e30c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 248
        },
        {
            "id": "1edf31c5-5c1e-4bc3-a029-485a879637ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 256
        },
        {
            "id": "ce755fca-2e88-4948-b181-dcd7d183329a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 258
        },
        {
            "id": "c6914973-8094-492d-af8c-23e45079ea55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 260
        },
        {
            "id": "0d54f609-0df2-4104-957a-6468dd1456e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 263
        },
        {
            "id": "ce980247-0a3e-4158-ba55-ab65910f0c85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 265
        },
        {
            "id": "c49ba637-60d3-43da-b215-bd2ae410ec73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 267
        },
        {
            "id": "7c09c295-0c14-435c-bb85-3918238816c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 269
        },
        {
            "id": "44894388-7c68-4e42-9007-dd42c9d5e4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 271
        },
        {
            "id": "dab34768-e0ab-47a3-bc2a-e7bacb703f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 273
        },
        {
            "id": "b0fc0bdb-1f0e-4be0-a8e5-42f97ea247bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 275
        },
        {
            "id": "1a94c47a-9016-445e-8421-9f9e2ae6fd9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 277
        },
        {
            "id": "dc88704e-6b2a-40d2-9f01-4b9992788acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 279
        },
        {
            "id": "62df9307-fb5e-4f8e-a0a6-0187870a7c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 281
        },
        {
            "id": "b5702f69-8518-4099-8555-7cf44bfbe0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 283
        },
        {
            "id": "b28d3d51-69e8-4325-a585-575623472264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 333
        },
        {
            "id": "b4d596e2-c4db-43b1-b756-b69d59734917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 335
        },
        {
            "id": "5ab25f9b-1a6b-402d-9586-18b4cbda8649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 337
        },
        {
            "id": "f7906178-8270-4e45-b0ef-ba8134704ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 339
        },
        {
            "id": "8abbc7ca-8bb5-4262-ad53-e2fd14be708f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 417
        },
        {
            "id": "6bc59b9c-0bee-4fef-bc9b-178d01da9fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 506
        },
        {
            "id": "6d27b698-969b-4b38-8580-06f2157a471d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 511
        },
        {
            "id": "059773ae-c2bb-4e4b-b3e0-f5572ec3e0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 902
        },
        {
            "id": "14b0af40-9bd2-4baa-b86a-618b4345d8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 913
        },
        {
            "id": "f311b75c-f88d-43ac-be32-125933089b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 916
        },
        {
            "id": "a529140f-17cc-43be-a89b-01cb2c4b0d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 923
        },
        {
            "id": "65671c7f-0408-4982-ac91-5a6bb7d13141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1033
        },
        {
            "id": "08cd4870-97b9-47d2-b06d-e464a4fa4bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1040
        },
        {
            "id": "a088aac0-80f9-4371-9b77-68d9813782db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1044
        },
        {
            "id": "636d5e6a-c700-4dbf-bdf7-e90d51214b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1051
        },
        {
            "id": "4d504fc7-7e8f-4088-a439-809d46b2cc91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1076
        },
        {
            "id": "92e689b8-1e32-4909-9de1-3c3c82ebece5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1077
        },
        {
            "id": "ca5d48cd-090a-463e-ae4b-1d68bcaf23f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1083
        },
        {
            "id": "f0c9beaa-7ac6-4d6d-a207-16f87d5987e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1086
        },
        {
            "id": "03718fee-4297-4e29-b674-25afc376503b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1089
        },
        {
            "id": "e07621cd-9768-47ff-a4bd-884f1901024a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1092
        },
        {
            "id": "5f96bbff-ff3b-4bff-871a-6a2716175eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1104
        },
        {
            "id": "56bb004a-48e3-4fa2-b1aa-77feca018200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1105
        },
        {
            "id": "36082b2c-bc8c-457a-bbb5-4f3a0a6f77eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1108
        },
        {
            "id": "6d0c993b-9392-419e-9f44-de75a5fb8f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1113
        },
        {
            "id": "89c6a0aa-0c82-456b-8638-dc2b62ac0faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1126
        },
        {
            "id": "12f11c36-3ba1-4424-aa34-2baed0cb3040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1127
        },
        {
            "id": "b4ca2e00-e5f8-4fa6-b731-d977023a1def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1139
        },
        {
            "id": "6e4e7307-b029-4a9a-a95d-30a8a1916e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1145
        },
        {
            "id": "e1ef74a5-2e6c-4cb1-9745-e263a85630e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1147
        },
        {
            "id": "93b55160-62b4-4266-a711-83d59fba7e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1149
        },
        {
            "id": "12530f9d-ad59-4c05-9edf-c67a23ee9606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1153
        },
        {
            "id": "75b1766e-6ea4-4c61-86a4-15366c0e6772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1193
        },
        {
            "id": "3eeea14d-c34c-4142-bd52-6a3766594dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1195
        },
        {
            "id": "359bd60a-dc9f-48b5-a728-216f636c05a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1221
        },
        {
            "id": "94b42cb8-e57f-4188-9b70-e2dc0384c78a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1222
        },
        {
            "id": "b1d89e2d-54fb-4752-b267-55e85c48a7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1232
        },
        {
            "id": "65cc7518-6d9f-4d6c-a203-3dde1bd0b750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1234
        },
        {
            "id": "6fad563d-5ab3-46b9-bf8b-e5efe4da90be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1236
        },
        {
            "id": "ce92d657-8ad0-4da4-8515-a1c68bf97f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1239
        },
        {
            "id": "48bf0680-bfe0-43f1-84b0-fb90dc78f349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1255
        },
        {
            "id": "b39aded8-f17e-402a-a544-b85e61fa168b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1257
        },
        {
            "id": "8edcea2a-041c-452d-b7c0-d515a8dfd0d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1259
        },
        {
            "id": "c9df2749-e4a9-4eda-a41b-a2fa97577d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1280
        },
        {
            "id": "82040292-fc5d-4a6b-bbed-30ea556c3db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1281
        },
        {
            "id": "912bea99-6aa8-4a5b-aaca-76ef209a3c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1282
        },
        {
            "id": "83511784-2525-4a14-8534-fbc5ee39eeae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1283
        },
        {
            "id": "00c6cec8-bcfa-435d-8fd2-aade6243d23b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1288
        },
        {
            "id": "56e80c12-af0c-4794-80d5-f9e21a2587ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1289
        },
        {
            "id": "ec14cbd8-9a49-43b2-9e70-d693563501a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1293
        },
        {
            "id": "c18a9d2a-f201-4550-815c-e88b0fd5bcde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1297
        },
        {
            "id": "f15b4ab5-4763-4d91-a950-41335163542c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1298
        },
        {
            "id": "852a90da-a641-4310-abda-95b099a83dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1299
        },
        {
            "id": "6227404b-bd43-4399-9a43-5b17a259a7bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7680
        },
        {
            "id": "01d55baf-78bc-456f-8f40-aa64ab2c4f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7840
        },
        {
            "id": "62716532-751c-42ab-b238-4e87a84d9d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7842
        },
        {
            "id": "ca509e53-eb80-4150-a8a5-b9bb328d7226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7844
        },
        {
            "id": "2d79f71d-2ab3-46a3-859e-0c0af7389fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7846
        },
        {
            "id": "0f2dc8a2-6943-4613-a757-40f0eafa4954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7848
        },
        {
            "id": "5839a928-b16d-4a64-95ac-d774d52bc9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7850
        },
        {
            "id": "e0bfcb38-bb4b-43fa-bfc0-2079a2630132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7852
        },
        {
            "id": "9c89441f-e906-4d36-9b20-3a9f01fb745d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7854
        },
        {
            "id": "a1cb1db7-5572-4285-9916-ddf8c71552c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7856
        },
        {
            "id": "fda2d5b9-ba93-400f-b913-41a11aff2c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7858
        },
        {
            "id": "9f5f17c6-30f0-4511-bf4d-a87666d1a943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7860
        },
        {
            "id": "784b61ef-5a9e-46be-8ded-d892c30d60b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7862
        },
        {
            "id": "b83f2661-48ac-4549-9c8e-2af694b8aeec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7865
        },
        {
            "id": "5a37a9d6-8ef7-47d8-8966-2aefada40f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7867
        },
        {
            "id": "192c7ef1-65fb-45ed-a63a-86fc39cd9c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7869
        },
        {
            "id": "a395026b-82d6-4100-ba13-3f985cef6644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7871
        },
        {
            "id": "edb47eef-ff0b-4a6f-bede-d8d8c28769e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7875
        },
        {
            "id": "0ae6b34c-15ac-4eb4-b7e9-13b6e3531d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7877
        },
        {
            "id": "4bc667f4-2d45-4fc7-a38d-ad71675df301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7879
        },
        {
            "id": "38609637-faea-48e0-9f8a-566fb12fec0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7885
        },
        {
            "id": "8f9784bb-15cc-47fa-9d10-aeb3d7968e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7887
        },
        {
            "id": "c26e7e90-ca42-4c6b-a293-3da40e00f9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7889
        },
        {
            "id": "3151ba19-7c3d-41d9-9b60-daf0dab7b24d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7893
        },
        {
            "id": "611e9093-25f3-46c2-a822-102017e1810e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7895
        },
        {
            "id": "b8544950-380b-4cf8-8450-5b04e158bdb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7897
        },
        {
            "id": "408fbc20-4125-4b73-a7b4-3b7701ca9f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7899
        },
        {
            "id": "c076a9df-e1b1-4b9f-83e5-37086b016358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7901
        },
        {
            "id": "8d610b2f-6531-4c07-ba38-196c022d1cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7903
        },
        {
            "id": "7215555c-8b15-4592-89c4-5d2f324f88ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7905
        },
        {
            "id": "908640ef-ac23-4368-aead-d43689e733b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7907
        },
        {
            "id": "68bb98f3-75d1-4758-a6a2-783238c9ad6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 74
        },
        {
            "id": "f8995b27-359c-42fe-afbd-37d4b40da622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 67
        },
        {
            "id": "ce5290d6-7f41-4cc8-8643-eb25bb30858a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 71
        },
        {
            "id": "613b1718-79b7-4211-83bb-a9d2d39b4c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 79
        },
        {
            "id": "ca51b1d4-e9fe-4024-9dfd-f896f5c178bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 81
        },
        {
            "id": "7889a200-1a15-4c80-994f-e9c1043356e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 84
        },
        {
            "id": "65a8995d-26b8-44c1-983e-39692db68001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "b16a5d57-f4a7-473f-a4e2-c3fbc1638e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 87
        },
        {
            "id": "8689fcae-706a-4f95-9139-2ae6182955ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 89
        },
        {
            "id": "f011e32a-a712-40db-ad42-893acff145df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 199
        },
        {
            "id": "64e5359a-ae7f-464b-9f14-634e6818a3d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 210
        },
        {
            "id": "eed0938e-bcd5-4abe-b0ff-e75a387071ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 211
        },
        {
            "id": "4f97f2d3-17e3-4174-8fc8-031794957d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 212
        },
        {
            "id": "e593cafd-e54c-4973-8245-0fd463faa86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 213
        },
        {
            "id": "f1a5a014-eb52-42e1-8de3-928476ef1e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 214
        },
        {
            "id": "9c5d24b6-6cb3-4ba5-b0e6-204a261c56ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 216
        },
        {
            "id": "8517d379-47ec-4a49-9ad1-163e060b0937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 221
        },
        {
            "id": "541a7a72-3a72-44a1-acbc-1111f855edeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 262
        },
        {
            "id": "4d192d70-ec22-4afb-8432-6cf9b622d8e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 264
        },
        {
            "id": "aa7d12e5-7808-4d54-b5ac-a4440c3d1d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 266
        },
        {
            "id": "0d89e421-2209-4503-9b31-9bcbf0354e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 268
        },
        {
            "id": "f1be9de2-f9ae-4684-a842-323297974ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 284
        },
        {
            "id": "a2e2f3fd-dd2c-414c-b421-5872bf2ca3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 286
        },
        {
            "id": "807bb546-a697-4f80-9acd-52244e0b7b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 288
        },
        {
            "id": "f8604b63-cc56-461f-85b3-694b4094f210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 290
        },
        {
            "id": "426f92c6-dcf5-4147-9b8e-ef5d2f8ba567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 332
        },
        {
            "id": "9e36ff6e-c2bb-4b18-ae20-56e5a101e564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 334
        },
        {
            "id": "27bcea44-fc75-491c-a6e5-63bf722bd769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 336
        },
        {
            "id": "4d63cb55-c921-44df-9b0b-e563a093e3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 338
        },
        {
            "id": "d92cc2a3-b8af-4f55-9d60-269bd4c9fd5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 354
        },
        {
            "id": "7844d2ad-1ebd-40d8-ac4f-9fac64973c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 356
        },
        {
            "id": "601e95fe-8813-4019-949e-9f2f1db8e5fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 372
        },
        {
            "id": "96e0c11c-fe6e-49e9-9ff2-a908eaa0fb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 374
        },
        {
            "id": "e49ca59a-e9db-46ab-8da3-feae78d31587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 376
        },
        {
            "id": "3bfba53e-b745-41e9-86bb-c632478fb313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 416
        },
        {
            "id": "6a676c90-6c87-4173-b8b0-aea6ed229dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 510
        },
        {
            "id": "38c0fc2e-e346-4f59-b4eb-ee2b6c4e948b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 538
        },
        {
            "id": "6e0d4071-7561-40d5-9ab6-0ca182ccd6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 932
        },
        {
            "id": "89b995eb-e675-4869-92a2-f27fd318c04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 933
        },
        {
            "id": "e46e6b28-e0b6-4888-907e-ab74ac8afcee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 934
        },
        {
            "id": "3b9d3b0e-f61c-4f3a-9c1c-f707832acfcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 936
        },
        {
            "id": "57d6b1d8-22cf-4acd-90e6-14129845c02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 939
        },
        {
            "id": "6ce42ef5-a809-4fe8-97b0-4b1184fe611d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 978
        },
        {
            "id": "387cdfc5-1055-445a-a865-7b7b0a7ff3d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1026
        },
        {
            "id": "af3c6def-817e-45a1-b0a6-394a4de053ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1028
        },
        {
            "id": "7057eb87-1eda-401d-8b09-e18e29b68d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1035
        },
        {
            "id": "4018a69d-37eb-4c59-aecf-af29720c50e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1054
        },
        {
            "id": "ac89ed8a-8d23-43f6-9964-d5a9fb1b3d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1057
        },
        {
            "id": "88f80e71-02c8-4129-b1ff-b177825943a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1058
        },
        {
            "id": "cc7b5728-ab20-4ed2-bd19-fed702778658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1063
        },
        {
            "id": "551a8924-5e20-4e6e-a3e4-438cafbbedef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1066
        },
        {
            "id": "55b79b4d-c810-4410-8f81-51c81d1ab10e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1090
        },
        {
            "id": "36a9df33-419e-448b-855f-4dd6a029feb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1095
        },
        {
            "id": "5240b874-d677-4eb3-859c-46f4c6adfb7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1098
        },
        {
            "id": "29f0d699-799e-40a7-922d-537cdf3b4dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1120
        },
        {
            "id": "972be1b5-5b65-4d29-96ff-b78428495c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1136
        },
        {
            "id": "5c5cc29a-807c-4b48-9034-52a96289b019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1138
        },
        {
            "id": "f64bd83b-0c78-4664-b343-206cd2962c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1140
        },
        {
            "id": "ec3b35ce-580e-448b-820c-398f6bd95ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1142
        },
        {
            "id": "8403a84e-4d31-4915-97bd-98cd4baa4a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1144
        },
        {
            "id": "a8db8dc1-c91a-4fe3-8296-aa10d1cf2540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1146
        },
        {
            "id": "0a5adda4-494e-4c37-b1b9-8fffd948feff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1148
        },
        {
            "id": "45d2d1c2-ab62-4e6a-bab9-7a53682f5d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1150
        },
        {
            "id": "532939b6-95c8-4ac9-8f19-7423f693afeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1152
        },
        {
            "id": "08cd087f-d1e1-4e20-81d7-58c277e1c8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1184
        },
        {
            "id": "0f711bac-d243-409e-9e0f-3f6c1b250b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1185
        },
        {
            "id": "d87a1f49-b4b5-42d6-bb6a-8b57dfc7119c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1192
        },
        {
            "id": "5b47f4d6-7878-490c-b7ce-53a08766e2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1194
        },
        {
            "id": "a41bdda8-5b59-4aaa-b552-6723b026c1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1196
        },
        {
            "id": "f7c5ba3e-2574-4521-8503-bdeeac0acc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1197
        },
        {
            "id": "74101f84-d54f-42bb-b12a-b026741c6f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1198
        },
        {
            "id": "a15ed753-2bf4-4864-8699-19e6508b53be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1200
        },
        {
            "id": "65f42a40-8505-42af-be4d-5eb9ebded866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1204
        },
        {
            "id": "cf6b0dcc-8ccd-4153-b290-4232fceec20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1205
        },
        {
            "id": "b5944104-f12e-4ee0-8c5f-c06c93b0ccc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1206
        },
        {
            "id": "447b2bec-8b11-4991-a5ce-9b7407d5438d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1207
        },
        {
            "id": "0083fcc5-d1c7-4ac1-b24d-40ab4535fecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1208
        },
        {
            "id": "1bd33ddb-c49f-49d6-8d83-f51375f7d6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1209
        },
        {
            "id": "b7e6a7e1-948f-4a85-8a1c-083350a71f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1212
        },
        {
            "id": "0364fa25-fbf5-4bd9-8ff8-d171be9a55ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1214
        },
        {
            "id": "a116c3fa-c1ac-4b98-914f-c91fd427ffb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1227
        },
        {
            "id": "cc3eb585-8168-4e6f-85a2-e41a7e1b6729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1228
        },
        {
            "id": "0d25501d-b3e5-4cc4-9ec2-06a7565ca974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1254
        },
        {
            "id": "cb29e58b-7694-4b14-bd9e-18eca273f9b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1256
        },
        {
            "id": "423d0bf4-7ee1-45b5-8aff-52a2e9488011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1258
        },
        {
            "id": "5fe79678-1f50-4e7e-8a8b-bcf8a7deb0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1268
        },
        {
            "id": "21006c5d-91a7-40e0-b351-c9a7876a65a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1269
        },
        {
            "id": "03907c23-d556-4b98-a122-29fb836982e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1284
        },
        {
            "id": "d3603407-0b5c-49b9-88f5-15c261782677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1285
        },
        {
            "id": "1d7643ca-e528-4e24-995e-6252ffc965c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1286
        },
        {
            "id": "979e49df-b097-4aa4-a54f-7ae3a495e59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1287
        },
        {
            "id": "dc02665c-a53a-4982-81bd-debbb55a4d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1292
        },
        {
            "id": "28089473-2a65-4760-9ea9-0236be39e157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1294
        },
        {
            "id": "34bd1b99-c3f7-4ca5-96c2-0eb638a37e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1295
        },
        {
            "id": "551af277-787e-4337-a174-58bd7b049137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7808
        },
        {
            "id": "d4ec7a7d-cc7f-4566-a2aa-c78c4d070341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7810
        },
        {
            "id": "7889e6eb-bf74-42b9-9240-b1216323cbf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7812
        },
        {
            "id": "cc6f5e4e-e65b-4db7-a32e-05c507589472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7884
        },
        {
            "id": "8e9e1b74-7b5c-4d99-9d26-e5fbdadd2c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7886
        },
        {
            "id": "b1aa174e-05cb-4fc9-a638-d28877305065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7888
        },
        {
            "id": "c0c7400b-5f32-473b-a126-46d8882cb054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7890
        },
        {
            "id": "3792e8e6-d03d-47dc-b8df-f62748ac23b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7892
        },
        {
            "id": "6cd9e362-6764-4e7f-8372-55a5ddb4d437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7894
        },
        {
            "id": "6b6286fd-5a9f-4fc8-8d58-e34cdec26820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7896
        },
        {
            "id": "cfd91132-e607-432a-87d8-05814878f288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7898
        },
        {
            "id": "b0cc59f1-6cfa-4039-8b94-1995d2f962dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7900
        },
        {
            "id": "acd9f70f-f35c-483e-a9a3-43772e2903d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7902
        },
        {
            "id": "4c281304-11d8-425d-b1b2-fce5a359ac4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7904
        },
        {
            "id": "79b0fc10-359b-4854-8647-8b1a29eafd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7906
        },
        {
            "id": "95dd8b47-8a16-4bb7-ab28-7bb2deaa4844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7922
        },
        {
            "id": "8a3514b0-f8fa-4857-a888-0966fd45034f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7924
        },
        {
            "id": "fa51aee8-12eb-4c81-bf4b-0f18394a5224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7926
        },
        {
            "id": "4c311a4d-5151-4a11-9042-aa47bcbe711b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7928
        },
        {
            "id": "1775e030-930e-41b1-93e8-4a4ec93e81ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 67
        },
        {
            "id": "92319fb8-8e5e-4bd9-b771-01b179197e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 71
        },
        {
            "id": "03714c53-42fa-4852-a48b-d9d2ad86332b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 79
        },
        {
            "id": "1388d4f6-a46e-4325-970e-54f773917541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 81
        },
        {
            "id": "4366aa57-ea6e-4e89-bb5e-2566fa805dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 84
        },
        {
            "id": "3d8ca058-5147-404c-ac05-44df437e5b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "9cdf68d3-c754-4b62-9909-c1ee58bf9d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "752dbd6d-401e-4eae-9527-01c5aff810b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 89
        },
        {
            "id": "c687dd50-1a25-4723-8bc9-1926a5d8a6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 199
        },
        {
            "id": "87cbc557-20f3-44db-89f2-f6815dbfd4dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 210
        },
        {
            "id": "cea0b78b-daa1-48ca-99b7-18a440146d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 211
        },
        {
            "id": "49a5a276-1e86-434f-8590-71b97ffdf2a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 212
        },
        {
            "id": "7c4296bb-3fb5-46cc-b838-b892d6703dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 213
        },
        {
            "id": "cc20c9b2-ca09-4fa0-bc0d-885f4f8eb571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 214
        },
        {
            "id": "bff000c7-504d-496d-8903-7933a3497424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 216
        },
        {
            "id": "4474f260-7650-431c-9354-6e3b1fefe514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 221
        },
        {
            "id": "2481c09c-70f3-4a2e-80b6-9db8bd733d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 262
        },
        {
            "id": "68eeabe8-5a87-475c-a64b-a02d12dc7c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 264
        },
        {
            "id": "661af058-e64a-444c-9c99-89f4d35c8d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 266
        },
        {
            "id": "0c35b51d-4d92-471c-b969-9101c6576515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 268
        },
        {
            "id": "ac2c5730-7848-4dc8-b056-f1a8756c2941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 284
        },
        {
            "id": "265ac81c-6107-41e8-b115-a9f9c21a6bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 286
        },
        {
            "id": "d230808a-d0e0-4ba6-90af-3edaca92f682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 288
        },
        {
            "id": "12ec45c9-f31f-4c91-912d-6ab03e8b7323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 290
        },
        {
            "id": "8c949257-071c-4b21-982f-5d5a03915775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 332
        },
        {
            "id": "8c3e0deb-e327-4159-95d1-cf3ce2c4722f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 334
        },
        {
            "id": "76835b8c-0a7f-44d8-89d1-7db70bb63754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 336
        },
        {
            "id": "13988a1d-92a8-41a2-b5dd-8b9f01eea449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 338
        },
        {
            "id": "31230b7a-ffd0-43c0-a892-be92763691ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 354
        },
        {
            "id": "e9609384-3c80-4fa2-b5b9-ca643f36a039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 356
        },
        {
            "id": "da68f66b-cffb-46a1-b6bb-806eea85c1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 372
        },
        {
            "id": "cfffc96f-fbfe-4036-9dd8-859ccb802838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 374
        },
        {
            "id": "71d30ade-5cd7-43bb-a3f7-7a182e6d461d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 376
        },
        {
            "id": "08ebb4d7-6b32-407e-a66b-5c071d7ab5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 416
        },
        {
            "id": "15b92535-335b-419f-9938-c4e704292258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 510
        },
        {
            "id": "cba9827f-917d-47be-beeb-20b67a947d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 538
        },
        {
            "id": "cb8340b8-1cef-4638-8016-24c8fd27ebce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 932
        },
        {
            "id": "a5127904-9760-4963-ba78-018b6ca7fc40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 933
        },
        {
            "id": "36c459b2-f73d-441a-b667-dd98acc7c11c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 934
        },
        {
            "id": "674667e3-5842-4170-b28d-c8478a722a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 936
        },
        {
            "id": "d7d8b9d9-8019-4dac-9752-a9ad7b1d694a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 939
        },
        {
            "id": "3ddc4893-c336-4512-b069-17535a0a8f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 978
        },
        {
            "id": "ae5fc926-0e01-46be-9141-6c2051ef8e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1026
        },
        {
            "id": "1f72ecd6-452f-4051-adef-f7504f53926e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1028
        },
        {
            "id": "4c641bd5-8fcc-4679-bfc5-78e694a28879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1035
        },
        {
            "id": "e3d82aeb-10d7-4ffa-b7fb-fac4560d4c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1054
        },
        {
            "id": "3b6ecd84-e5d9-4391-a991-397c44627f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1057
        },
        {
            "id": "5240f52e-9207-4158-9ba5-a42bce50b23f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1058
        },
        {
            "id": "f0599325-861d-4517-a50d-1dd6a61fd027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1063
        },
        {
            "id": "96438baf-990f-4d02-b0d5-c24552597e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1066
        },
        {
            "id": "9513da1c-b3d8-40a1-9e8f-4c1f385b0b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1090
        },
        {
            "id": "4a3d4458-bfe5-4b53-879d-65c9761dc5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1095
        },
        {
            "id": "2effde7d-b7f7-4ffd-a6eb-dc4a7bb3704f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1098
        },
        {
            "id": "34e47a2d-1c91-4952-86cd-063ed00a217c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1120
        },
        {
            "id": "607737a0-ad90-4ed3-8ad0-ad343ae61da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1136
        },
        {
            "id": "8d0edc3d-3ed2-4e74-9532-b83c4e772ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1138
        },
        {
            "id": "e944c22b-0040-4cbe-b755-b86cc5a298e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1140
        },
        {
            "id": "56f7bc92-3955-424c-9e31-09416b410214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1142
        },
        {
            "id": "921a5952-6a8b-422b-bd71-21e1838eefd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1144
        },
        {
            "id": "3d80d772-82f1-4b2c-aaed-32c2678f8750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1146
        },
        {
            "id": "fa276d9a-8499-4a1b-ab63-f3a3228c4cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1148
        },
        {
            "id": "28c7a2eb-78b1-4f19-8b32-dd64eb079de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1150
        },
        {
            "id": "769dbf1a-a872-4954-a558-a022bce92715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1152
        },
        {
            "id": "3e2c8209-342f-41c9-a8ec-0f1c4ac7e067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1184
        },
        {
            "id": "dfd7097f-587f-42bd-a097-fa116dd5c5d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1185
        },
        {
            "id": "f84de423-5bec-493c-b692-e28f2009f9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1192
        },
        {
            "id": "26f8bdaa-45fa-4ee8-9037-7275c52b7579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1194
        },
        {
            "id": "293b21af-001e-4c27-9c66-5a6430977757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1196
        },
        {
            "id": "05d0f098-0e82-421a-aa88-535a48a72a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1197
        },
        {
            "id": "233b5a8b-8c53-4e5e-8ffb-f0fba949d2c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1198
        },
        {
            "id": "c4056744-6baa-4c1e-a3c5-41b2f8d55731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1200
        },
        {
            "id": "ec48a4d9-476e-471e-a2eb-79bfecd82f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1204
        },
        {
            "id": "ef9e123e-a4d4-4668-8a84-27100b5223cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1205
        },
        {
            "id": "cc38b2a4-cc2a-49f9-937c-1ba49256cd90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1206
        },
        {
            "id": "a5928547-23b5-4fff-a1a1-b75dd6565a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1207
        },
        {
            "id": "c25334f3-0461-420f-97f4-46791bc9f449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1208
        },
        {
            "id": "29043cfd-94f8-4cb7-9f29-5f3008b60350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1209
        },
        {
            "id": "e017d099-43b8-43bd-a1cc-d5b453fe4e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1212
        },
        {
            "id": "cf936175-8de1-44e3-b0da-02389f57f35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1214
        },
        {
            "id": "20adfaf9-75ad-441f-bc52-667361e554f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1227
        },
        {
            "id": "f99a8fd1-5c7d-4632-889b-3a6e7af88263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1228
        },
        {
            "id": "b339d2ec-4ab8-4965-b3b8-066075da4743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1254
        },
        {
            "id": "69e23be4-ce9f-41ae-9818-77ac4945b9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1256
        },
        {
            "id": "43647a81-68b2-4991-97dc-b22ee2e9bb92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1258
        },
        {
            "id": "6f00f240-e2d1-4023-8cd4-67c5cd01e9c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1268
        },
        {
            "id": "459b14dd-1895-4f76-bcf9-bb1da8328b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1269
        },
        {
            "id": "7bcb07ca-c0ec-46ff-ad76-885b4f79159c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1284
        },
        {
            "id": "4f047023-6f1f-41aa-aa81-77e8bc8a47f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1285
        },
        {
            "id": "7e7a6010-c932-4c38-816c-b2fdde69cff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1286
        },
        {
            "id": "5bb7b9f2-494f-422f-a7bc-b00656f63de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1287
        },
        {
            "id": "7a5a4d24-c6b2-4e4a-af54-88c850a2da50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1292
        },
        {
            "id": "e7357825-76d3-4fb0-8386-db60d6252f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1294
        },
        {
            "id": "6fabf188-c0be-433d-85a5-267128e19242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1295
        },
        {
            "id": "6af29127-8916-47f3-8a91-8251e4b501b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7808
        },
        {
            "id": "d013ca55-afb5-46db-ad5e-c4925a3c2d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7810
        },
        {
            "id": "9d09c995-a3bf-453e-856f-cce77f5edfcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7812
        },
        {
            "id": "4c52e7a3-4ab4-486c-9499-63d82a91d6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7884
        },
        {
            "id": "bd4b1c59-77b3-4355-80a8-d6188cbf4fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7886
        },
        {
            "id": "bd6128b2-fdc0-4253-bf61-d6a417cdeef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7888
        },
        {
            "id": "d531e73a-128d-4908-b2be-2c75cd7f3014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7890
        },
        {
            "id": "10193c63-4629-463e-81b4-e2ac37416ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7892
        },
        {
            "id": "10ebb771-8829-4b7b-bd32-b74529cc018b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7894
        },
        {
            "id": "6258d189-b94b-4a66-80f2-942297d4a6ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7896
        },
        {
            "id": "46f5e72c-c9b0-4df8-8fad-9a6363ccf1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7898
        },
        {
            "id": "ca76fa97-8c2d-4911-96e0-431b6acbe31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7900
        },
        {
            "id": "596bc683-be49-4455-ad65-be10dc26cfed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7902
        },
        {
            "id": "0e1495c9-a585-4269-b0e9-d31dd3bf1b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7904
        },
        {
            "id": "c3fc76ed-ffa7-4429-8f3c-8b319d715cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7906
        },
        {
            "id": "fb47613b-9f6e-43db-8283-110a49369432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7922
        },
        {
            "id": "8cf121ed-f166-4c43-ad3b-c4acd0e64e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7924
        },
        {
            "id": "b3ff8a29-b937-4416-a9b6-d32fe14eb822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7926
        },
        {
            "id": "410cfcf8-780a-4cff-8bbf-66324b6dba4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7928
        },
        {
            "id": "f10339e5-3a3f-478e-a0f4-2bcfeb38c9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "b851f21b-1bfe-4a63-bcf2-5834cf52bca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "f4c5b644-c3e4-4538-9c4d-5897c4b61fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "b858a865-67f8-4fca-b16f-8ad2ebcadd30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "d4453178-0acd-47d0-a605-d90aa4f25ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "9de36815-eb1d-4584-8fdc-3c1138ef1be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "1ed15fd8-9658-47b7-bbc3-1d7e18e4a646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "30babd7a-903d-4ad8-9641-363e9a423851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "30536425-0162-42d1-b015-836fe9ad238d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "6fef0685-2a41-4dab-b596-caf247639d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "16a6fc66-8fea-4700-a1d7-4237865a2f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "d10ccd65-51da-4569-9154-4f40928b794e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "7f6dc087-519e-4d92-ab3f-7d95e4ba7cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "8111b038-997d-42e8-b7f2-c8ef5a939cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "2ebf7ea9-47c9-4c61-b0a3-aecaefe11070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "cb49c0f4-177d-47aa-ae29-9358efd893d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "6acd9d8d-3b7d-4b1b-acf2-63a6bff6286e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "02bd4f11-d157-4e32-ae95-c4543c67d74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "0956e058-67f2-4c9d-896c-f7d7587c936a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "c74af6e2-d4f8-49ea-9268-a520617f61e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "8a453598-1872-498d-afd5-366a43366d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8218
        },
        {
            "id": "4341934b-32ad-4cee-a992-80e6fee6414a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8222
        },
        {
            "id": "a79fd1ef-cd23-45e6-b32f-e68d746f5464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 34
        },
        {
            "id": "cb7fad9d-e8b3-4776-a922-dfe22708a263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "b1a2ed3e-1bfc-4f9a-82d0-618250f665bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "3d6782b2-56c2-4544-abcd-c15d8c1ba80e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "0974886c-39ee-462b-a0bb-7a76f48e79f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "da071a6f-3835-4f76-b382-e3538f79cc41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "1b1cbb0d-393a-4625-bb47-bd34f21337c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "03535dc3-9e4e-42f0-8c4c-8cde8df8065b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "ef57968a-0732-44b9-a535-12d0ba873fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "366b2570-0ef3-4555-8fe1-3152b92ce14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "b908a6da-d18f-4694-8d94-8fa097a7d4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "cb2dd333-0d9c-4e00-96bc-81e284cc524c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "cf753e5c-a356-463d-bd8a-b72458dfc3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "f9b8be44-c182-4307-b142-e759049da09f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "ed0919e5-6459-457e-a9e7-e21e55cfbe00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "bd5f467c-22ac-4ac8-81df-266577c5f57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "bb99639a-0d08-4f72-be05-543798ee9a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "fee99a11-062b-4ba0-884a-63a7fcff4f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7680
        },
        {
            "id": "5c5acc4f-f5df-4d32-b251-0c73f2a0d007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7840
        },
        {
            "id": "55c1f6b7-c8fc-4dbf-a699-6b00b4c97d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7842
        },
        {
            "id": "eef35c98-6d2f-4612-b74a-9071b5238fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7844
        },
        {
            "id": "c2d5a6e8-41b3-4dd4-b838-b21295e18a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7846
        },
        {
            "id": "91f03cfe-6719-4406-abc8-cf56792bbd25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7848
        },
        {
            "id": "8e3f5422-00bb-4fdd-9ddc-4fce521e3d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7850
        },
        {
            "id": "4e6ba934-b188-482e-b5be-8d427478e517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7852
        },
        {
            "id": "753bd2db-6e8e-4416-ae82-90099171f76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7854
        },
        {
            "id": "edf8449b-7505-4062-8ddb-456a8423f1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7856
        },
        {
            "id": "b697ebe2-f260-4008-8b22-d3d625e45903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7858
        },
        {
            "id": "4909c95e-e567-43b8-ad2c-03b963e9e93b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7860
        },
        {
            "id": "364267e2-d92d-482b-86c7-af4d2146fd5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7862
        },
        {
            "id": "83a009df-cc41-4fb9-b250-64b26cd7f5ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8218
        },
        {
            "id": "ff11b659-ed1f-4290-9c67-9ba5d7edaccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8222
        },
        {
            "id": "98c95d82-6eac-4d6c-b31c-52563975a815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "b43f1a17-ba99-4dee-b38f-dbdffc3a8f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "2e490d3c-7637-48dc-bf1e-4b03c888f0b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "db7af7f5-50f7-406d-a64b-9d0d00aef5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "3a6cf7c6-f353-4f75-9cec-36c50c74c01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "ee7967b5-eacf-4153-a912-5eb2980d0a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "aa0a6639-5b74-4d12-a2cd-84dad17efec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "219a0867-0294-4da6-b154-925bd37b31bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "37910cee-91be-4345-98e9-63d15a509584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "7b887cb6-4acc-4c35-af52-50afcf7a3b84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "a6261643-f2ed-4288-aa73-2bba4ae2f40b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "2f6d7e01-11cc-4b59-948e-c76f58bf5fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "c9fe0488-15ce-44ec-8182-02ec78261b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "986e9e48-a855-455a-a66e-f8c07bc67ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "89736a2f-0d5f-4761-ba40-4aabbc0f46dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "19e28343-132c-4d0c-b52b-66a340c4850d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "799fd87a-495e-4cdd-9705-2f9870835a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "975f2def-7feb-4fe4-8a67-17da122555b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "325b8c48-241a-4e93-ab3e-41b72b7164aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "493c8c60-a302-473e-b360-751be447fa42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "2949b3a4-f687-49b0-980e-188cb248358d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "5290fc52-c460-4de8-9ce6-3e837104cf87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "c7e14b3d-2b32-4318-87e5-a18940eb6f7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "86e5b0b2-f405-41b5-80ce-9f7fe5a65b3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "f97cdb6d-5fa9-447a-919d-33eb9e0e140e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "8bfdb25a-6339-41da-81a5-b261e6379132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "071f837a-9883-4e29-a628-9da0fa21ebeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "39c3e7bb-2e54-42ef-94f9-105b8bc16e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "d848af1a-0284-4519-a356-d7a024b5d73c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "10a0227c-6e9b-4141-a5d1-1e4fbccac67e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "7e55c835-3a19-4994-b150-53ae1f68a778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "7ef576be-717b-41db-90f1-ff4786ee67f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "5439fff2-6c20-4004-b710-7c7fa6709011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "aebee6a8-d062-4f31-8197-9fc68f81f66a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "865f6214-b8e6-48a4-ae0e-418ffe6901eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "b82a5fd8-3cf6-4d52-9c2c-249e2610a68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "51119bcc-f3de-458c-8b4f-06e042467a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "11720982-1499-4443-a896-be35cfd29539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "fe61c1dc-23e3-458d-b75b-98162f03e983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "2238c8d6-8074-4336-8439-2b999e4eefc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "058c0c00-af12-4daa-bbd7-3604c948ff2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "94d73770-47bc-49e8-b581-e7ff32a5bc0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "74aa9f80-4878-45fb-99a4-f42b1d399bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "de7bcccc-31fa-4c28-ad49-06d7c3d8c306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "58718511-c39c-4b5f-a282-20dc75bd9c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "1e07c97b-8a52-42c2-b1d1-e6ada9fb7c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "ec8aa878-0eb2-4117-a47b-6343604f684a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "9f344b30-df88-4fb2-b15e-8b006b777ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "7b99d1c1-1dc9-4faa-b65f-6d917fe327cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "94b36746-ef61-477f-9292-b9e0d21e2af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "2f202b32-4202-4573-8da6-089bb451ac47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 263
        },
        {
            "id": "71332ea3-84a6-4782-8e9e-a080ba097379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 265
        },
        {
            "id": "0fba21c7-2be2-4e04-a5fb-856fd256d321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 267
        },
        {
            "id": "529d993b-1770-40a4-b42c-cd426bbfe0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "cd868f49-20ce-43ae-8e99-0f3b78a27695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 271
        },
        {
            "id": "bb249fd7-a8a2-4f28-b8f4-8af6b4f1b387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 273
        },
        {
            "id": "6d2fbe7c-7d41-4a23-b9a3-af3d0ef86c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "0e7460d8-73e1-4ff0-82af-060a35100446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "794de8a9-3e60-4c5e-bd84-0cf1a7f28349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 279
        },
        {
            "id": "6eaee264-e324-4208-80c5-ec32e0512fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 281
        },
        {
            "id": "864c28f8-1532-4baf-ad0b-d2c9ebe0728e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "3f2fb898-a811-4cf9-abb8-04e99417a917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 285
        },
        {
            "id": "29f023e8-486b-459e-ab91-6ba6494c61b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 287
        },
        {
            "id": "074ffd0e-4b0a-487c-b625-342b02581c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 289
        },
        {
            "id": "021ed586-ef60-4cfe-a87c-825f5e618515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 291
        },
        {
            "id": "7b5ccf0e-7c47-41fd-be2b-1cda2550dacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 312
        },
        {
            "id": "17bfe4d6-a665-4d43-991d-306b19794f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "a248c28b-a826-4e29-89ba-b45da0c4884d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "8da13281-ece6-46cc-8ed8-4e974a0f09fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "822da8f2-d366-40d6-941f-ad9bbf238df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "70fea43d-5382-41ae-a317-5d7a14c816b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "4f50b53b-4cf1-43a8-85da-0ffb8f9cf7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 337
        },
        {
            "id": "388bb783-9abf-4e96-a40f-20b53750602a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "bf7faadd-47a6-47a5-914e-3093f5189cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "f4f1a407-d60d-4a64-a894-253d57963b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "56f90eca-6175-4c5b-9122-ed9026520085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "ada808c4-f971-4957-a30f-9f7a326c77fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "7a2cdbb9-5dca-4d61-865e-2b95784f1925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "cf07b291-6dc2-4d6f-b6c3-4ffb34ee8fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "5a61eb10-393c-4d6d-bafc-d274fbd5fca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "1dd628ff-f506-46ba-aa2e-6d2ef62213c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "18864b57-6c04-4b38-935f-2d4a27693d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "56c7f06b-9a1f-4885-968b-ae5bcc898668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "ec69c0de-06e6-4e78-bc95-ce11ec6449c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 417
        },
        {
            "id": "0087a0e2-47d4-48e8-a5f9-648cf5ca150c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 432
        },
        {
            "id": "76221cd9-44e9-456f-b755-b5ad8bc88705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "c9bc0b82-8a4e-415a-bbcd-4aa0d09827d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "42298ca4-ba50-4e57-912a-7890ca5ed0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "7242e100-3c2c-4d54-919e-6ffd660530d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 511
        },
        {
            "id": "3f0465b7-b792-4d08-bae2-2cfaee8d8777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "9276b215-2972-4d9c-8b09-56d5406dfb15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7680
        },
        {
            "id": "4ab74854-19f9-4015-b225-df7b66b84b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7681
        },
        {
            "id": "c770f8c6-ff31-4a9d-ae87-7ca8b31a9df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7743
        },
        {
            "id": "d2ec2e85-24b9-4cdf-b619-433441f78ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "c5b106ea-3d08-4718-b7e7-1a96fe9301f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7841
        },
        {
            "id": "4d9a1df6-6d3d-4508-a9dd-c919fed63984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "87aa3e92-72b4-4d4d-8fb5-e32bb06d3f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "a87f1a54-b6b5-4434-89f1-2ee79fac2dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "b9b4f03b-1053-4526-b1fc-c53f3acbb341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "e0a0fe77-b63b-4152-b5ad-c71d2f019786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "51cdd6a8-751b-4eae-871f-b81a09f1817c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "b9e95f3d-0a62-431f-b4a6-6b3b0fcc8681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "6d647dab-3a57-460f-a797-ce084ef8d7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "12ad3a07-6a19-4d9a-afef-4f8bd256047a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "592f29cf-8945-4fd6-89c1-1efef12200c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "c9c416b0-27b6-4529-ae64-d02e729d0787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "cd082cdd-9897-4481-8806-646268fb399b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "d5faa371-c505-4e56-81d5-5159ffcc6126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "27495343-28f6-4fcd-9a60-d8b5d5aec069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "63b60707-e09a-46f2-b00d-f5e18eaae7bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "01f13790-a63c-4bc2-8dd4-3e10edbb4a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "306b23da-42f5-4903-b6dd-20b2ba4c2f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "02eead11-17fb-4679-887a-392424b47bad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "136730b2-13ef-4f7d-ba5d-c2318295e49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "1899f20d-46b4-42fe-a3bc-eb6b52c41dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "8021c875-64fc-4eb2-9e19-017964de9df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "2c45316e-72ad-47f5-b903-0bca7386d133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7865
        },
        {
            "id": "d19ceb83-c6df-4580-bf5f-3483fb44016f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7867
        },
        {
            "id": "ca99517c-9a44-42a3-b8fe-187eab5ae7ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7869
        },
        {
            "id": "9a5bfa58-2447-4d1e-9941-035194daccb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7871
        },
        {
            "id": "2155161c-52ef-4f39-99f1-9dce0983d69d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7875
        },
        {
            "id": "59a25e24-a4c7-49c8-b162-10f1b755e147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7877
        },
        {
            "id": "8838dcac-bb34-4126-8bfd-28d935b0dbc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7879
        },
        {
            "id": "78835036-da15-4480-bda3-cf3ad9db4f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7885
        },
        {
            "id": "9c3912e0-e41e-416d-8f84-d0327c193242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7887
        },
        {
            "id": "d72efb01-e3d1-4f16-9578-5ff62f434d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7889
        },
        {
            "id": "1988c136-29c7-449d-92aa-fb1ec52db1ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7893
        },
        {
            "id": "af91e9f8-47b8-4b3f-b6c8-c97ffe34e725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7895
        },
        {
            "id": "c9265de0-c5f8-4998-a8f3-ad33e3ffa933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7897
        },
        {
            "id": "71b8b852-d97c-4ebd-8364-35cb4e8bcc48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7899
        },
        {
            "id": "7562728d-7572-44aa-b185-f50d7376c874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7901
        },
        {
            "id": "69e54851-6cf4-4b61-b14c-4de1687f0b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7903
        },
        {
            "id": "0d051acd-4dea-42cc-aca9-a06bc1241999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7905
        },
        {
            "id": "26505444-a0d9-415b-a16f-a59895930ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7907
        },
        {
            "id": "cb6f57c3-6e0d-4e86-8ada-47632340a210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7909
        },
        {
            "id": "c7d32fb5-8344-4584-9e90-14f629ef7a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7911
        },
        {
            "id": "b279dff9-2138-4867-ad5c-de6bf804e57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7913
        },
        {
            "id": "9ba23119-ea72-4d00-a54c-f62d2951bf90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7915
        },
        {
            "id": "ecc0ad1d-33a1-44a5-93bb-33ec03233159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7917
        },
        {
            "id": "bfc69419-44d8-4aa0-8481-e967ee6f6b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "93e8089b-b8f0-4bde-ac21-5fbdbded761c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7921
        },
        {
            "id": "578aeccf-9a56-4a01-8137-f3d1d53c413f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8218
        },
        {
            "id": "1d3485fc-9c4f-4a40-9696-20616de765e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8222
        },
        {
            "id": "ece3937d-3138-484f-8305-12832ae0b871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "7da56546-beaf-48e4-b60f-3b6126b8b0de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "7074e395-9679-4c69-b97f-5fad683518a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8218
        },
        {
            "id": "32ef5b85-2b9b-4620-8c7f-06c52aa3a322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8222
        },
        {
            "id": "a96bf07c-333b-4f67-bf5f-f4e76255e819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "3f5bc04b-38a7-456b-a353-ff54a63f5843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "26cf2937-8f66-4cea-9094-5000d19c0362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "d9da8739-59c3-436b-a978-b0c8dedc59bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "4e3fc76c-4c12-4e0e-a6aa-ec8e211bcb35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "7ea954bf-fc63-478c-b0a2-6c6079ef20e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "ee47aae3-b579-4c85-af06-8cbee5f067df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "0997835e-790b-45c5-9346-3ee60ad47844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "fd02097d-67f5-41ff-b0d8-eb131d100115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "a2bc7673-6436-4308-ba44-e37684a3f2ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "d0973a4e-ab65-48da-8c7f-a5b3f3d7c843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "7b5b66e0-deb5-4ed9-8d84-c54355afbecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "91f899a7-9a5f-46ce-bb21-f075ccd720a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "d4994744-9512-443f-8753-3d999de78506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "b3eb5989-4a11-43e9-8d1a-a12da36929cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "9ac9c847-cf1b-440b-a861-27c0a7db87b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "d8b780fd-879c-4626-ac6f-b336bf4d658d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "4ddef232-6450-408a-8432-930c823778db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "eff7f84b-20c3-4b72-9bda-78d032a9798b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "9fc9cc08-22f5-4e9a-a16c-de9798bac270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "0affaaea-b395-4463-b292-75ac36d6c891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 225
        },
        {
            "id": "5d9c30f3-19c4-4133-b150-ef0563cef0ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "ef7fdf95-b552-44d5-b21a-86ef961d8eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "abf3c635-1d55-4101-a52d-9854d31fcfaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "3b40e25b-30c2-4d54-b796-b6b0979dfc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "cee0ae03-f1cb-4a7d-a459-15f0c59f98d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "24d10161-c5b7-4ae1-9cdb-add911c4e22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "78c516d1-ba5e-4154-8548-530d5ed77586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "c49f41f2-1d68-425d-9deb-f6d8c9dc2bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "340ab3aa-07ca-49ce-abba-8ca1efa5595d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "01b8bdf7-9a69-4f5c-b2d8-bf91a2ad9815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "67e74f9d-9966-4d5e-a91b-688a5cb534df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "219d0bce-a452-4909-88ca-959fecc2405c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "45a1cd8f-9879-4997-892f-284d096e0f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "d5dee453-bf0a-4fa4-b6ac-e6e79576bbe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "ed0c0237-f26e-4371-b615-50a75264d749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "891d552e-dfea-4348-827d-1f61e33b1434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "af1de7e8-f3e7-4297-855e-6053d628c9f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "1b74f3e7-920d-4e43-92d8-82d2886434f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "6306fb39-3b62-4849-88f0-d917faf3ce1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "4bc24c50-cbae-4262-b86c-b706cf48fbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 259
        },
        {
            "id": "46c3745e-2dc2-4048-a9d7-b32bc51bbaf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "a958b25a-3b53-42fa-9723-1ea93fe3168b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 261
        },
        {
            "id": "ba0b65df-13f7-4727-849b-a05ecbe90666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "0ea1de2e-5f50-4239-9936-747599e45062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "86baf9c2-2d99-4aa8-86f0-05102c1a2009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "e9c3850b-ca48-474c-a6aa-651d83ac4ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "0181f5d9-f5c3-4eae-b33e-efa2bc4e537b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 271
        },
        {
            "id": "dc4e5615-325f-446e-bfd0-8f149e126b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 273
        },
        {
            "id": "2829d2bc-1294-4e5b-a069-68ddb7c2728b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "d6c06f49-9c1d-4654-8825-5ac6e08b9f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "b12f2a7d-a9f9-42b2-8e9c-49518c18dd9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "368c01e5-cd88-4473-bc4b-8339894d26b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "79dea249-c522-401d-9b74-9bb8dab89b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "35bd839a-4adc-47a0-b12a-a7d0954d020e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "06b068a6-1b7b-4ce3-8457-1aa4a786fb86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "859f6ab0-1d70-49ed-9a40-801944b82b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "b9691c2e-c7a1-4907-beeb-c33a125004b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "b3be182b-4b40-458c-a4a2-10e0f98a8823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 417
        },
        {
            "id": "a2039afe-4d83-45d0-b968-cfe61ba6a8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "a838954e-82fb-4915-9b70-c07a4b5bba8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "2e4a43a1-041d-47c1-b6fb-b0fd4f401a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 509
        },
        {
            "id": "b5ab777a-ce0c-4530-ac73-331fd8e0989e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "8d6e21ff-0056-4a88-8c7a-3a48303921d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7680
        },
        {
            "id": "41622871-2570-4637-a90a-6dbfd580326d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7681
        },
        {
            "id": "5f2a9a3f-2d77-458c-b61d-3484ce3ad088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "290776db-ea8f-4f48-bdaf-33c740cbf74c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7841
        },
        {
            "id": "a446e7c3-dc2b-499f-849c-3421f3b637b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "1953db97-0c8e-4faa-8566-cfc765ca463b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7843
        },
        {
            "id": "68f30d75-30f4-4b85-ac55-bb66a0c77e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "7cd7570d-5590-4dfe-bef8-aaf5de2548ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7845
        },
        {
            "id": "de1b8ceb-a8a2-4c8c-a313-646bef8ed4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "762733b2-cbeb-4e11-a94a-fffb1c1503a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "c750f40c-cf86-4a35-84e4-ff167a0367be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7849
        },
        {
            "id": "45cc7339-c69a-4128-a849-360a1b0fb3f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "da09c71d-c467-4d17-817a-439261249e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7851
        },
        {
            "id": "8cd64607-b169-4b49-b688-574201ff50a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "a7dba75d-b043-492b-ab82-241a89c7f514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7853
        },
        {
            "id": "b9effef5-4b04-4b86-9b92-8f1d985c448a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "d75c33c4-7c17-43ea-b560-ec26cb5b2637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7855
        },
        {
            "id": "8303abb6-3876-4ddd-b874-4476c10bc497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "2e4adcf7-804b-4c4f-81b9-c0855b142e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7857
        },
        {
            "id": "58ded9bb-3ee3-4dd1-a054-1f964e8a19a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "c9ce5122-232a-4a0e-b591-230b809c742a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7859
        },
        {
            "id": "d034f101-8a6f-46f8-8988-959ce1d62a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "178cec8b-929e-4ad5-836b-2d76cde41601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7861
        },
        {
            "id": "657294f1-c8e6-42c6-83b0-4eff785cf0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "e49b05b6-b950-4da2-bb4b-fe8df85a6fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7863
        },
        {
            "id": "cf2e07e3-5ae6-40c7-a68b-f3c36486e3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7865
        },
        {
            "id": "1efd51b4-5e31-4c9a-a9f8-1925bd1547c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "b1309138-c8a8-4eb9-ad03-e4227ee39ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "76807090-9732-4cd3-affe-059acb38cb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "364c9f42-c217-4772-b75a-97771da63dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "af9ced5a-bda5-41f1-8d85-7c6b303aaf5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "4c109832-f299-4dd8-a51b-b8f0a4f12de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "3794db3f-61f5-4e85-ab4f-169e4748da48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7885
        },
        {
            "id": "3775d5e9-9a41-432b-af95-444aeee75476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7887
        },
        {
            "id": "4e9d0244-f466-4704-95d1-57b1c11dbd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7889
        },
        {
            "id": "10083202-279a-4379-b56a-4f42183a6f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7893
        },
        {
            "id": "64279596-e770-45c8-891a-482aa07edcb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7895
        },
        {
            "id": "d250ba00-1e90-4478-91a3-139317fc3954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7897
        },
        {
            "id": "57c124c6-6c38-46f7-8030-5fb1c84d60df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7899
        },
        {
            "id": "8a89e89f-2424-414a-9fa7-ec147d1e6ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7901
        },
        {
            "id": "474a30a2-1cd8-41da-8255-44241649f9a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7903
        },
        {
            "id": "41581d3e-036e-417e-8923-d7e79cc59f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7905
        },
        {
            "id": "7b4f1e97-0dea-4ede-8cc0-da0eeaa7692c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7907
        },
        {
            "id": "0ecd1075-fa22-426a-b344-9fb0f533828b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8218
        },
        {
            "id": "c452f02b-7f1b-424b-b115-f2a8d36060f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8222
        },
        {
            "id": "5cffd636-5533-487f-b3ff-ba6adc00e725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 74
        },
        {
            "id": "872f42c0-d301-4e13-a7df-cb8247391893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "a62d4ff8-a5c9-4fb2-93f6-d3106172489d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 39
        },
        {
            "id": "1203082b-9811-441b-98a7-7d6fc76f2429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "9c2f84dc-4c8c-4b8a-976a-1a573d7b17d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "2903b8c1-c8eb-4219-823a-beb3a07909b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}