{
    "id": "a79f4ec1-c401-455f-af88-916852794f4a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "Tip_Font",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d2adf260-c190-4234-96b2-e7e9bbb8dae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "130de11f-2b54-42cf-801f-9304b6b7119d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5454e386-0a64-433d-947d-3c769ccaf80a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d45321e7-0315-46b8-af45-724ddcb5b9e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ccbf28b5-6671-4372-9c0f-5f24541b249b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d5d9325c-66b5-43ae-8ffc-58e9f406a1ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7b6f7af5-b829-4f74-bd9b-38cefb6409d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "90166021-0c0f-429a-b87b-677908e3bce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9a27bf65-dbc0-42c3-94e2-16ec2a3488f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b2884f00-40e2-4443-89b5-7fa29474a1bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8e3fa10a-6298-4e8e-aa52-1072aae67ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0265a7f4-9923-492c-a373-88e9e028a7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ee8b911c-b4d2-4758-aeaf-6fb3678b8294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 151,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c23406fe-f580-42a0-85fc-40577729a9ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2a515acd-0274-4e9a-9f73-6aac12fd26a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 156,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "014d2724-8f88-49bb-8690-f2599fc7c79c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "acea9ebc-67e1-4d98-ad65-9bf24796d6a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bd8fcbca-06d0-4532-983c-a50121258b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "be1e37f5-7818-4989-a97f-ca9bbcf122bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5276a8a5-7e6b-42e8-852f-415254c735c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 207,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "30719218-188c-410a-9df8-ec67662dc6e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "29d8afa4-cfe5-4c43-99b6-67a64ea5f12c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "86c22488-2689-45af-a1d0-b6c2ed32531d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "93e7b41c-db1f-40cf-b7de-a587de77c5d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cc80ddb8-0ee4-4cbf-a073-5dc429b12b3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0ec473a1-4ea6-4cef-bb69-d7a2ba7f95f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a9d15712-97c6-4081-a74b-9e3fa02a25d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a3b3328d-94b2-48b6-99f6-37b46c9ac135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c30cc83f-3690-4db1-be30-1996c26aca78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "dcacc6ba-7dc9-4b58-a4b1-8a65bf069275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2e05187d-b2a1-4d33-b33f-c01e15f0abfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "11f78612-5cc6-4545-8f85-485f7b9985ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "529447d2-8729-41f6-ad17-23ebe38aa3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "51ed663a-d57e-41ee-9a68-c5d2a62988a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c5110460-b8dc-487c-88e3-b534ff095782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 141,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e706f913-22c0-41b5-b109-90050e0a3548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b463c36d-c491-483f-b2f6-a0be3082a196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a82b671f-006d-4b6c-bced-f666252ed255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8c5d1ea1-5961-40b2-85d8-e2cdb954a527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 22
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1f412125-40da-40bd-92c3-35ffaceb5697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "67b48c81-6861-4fc1-8f10-586b781ebc81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e444e8fb-16ac-44e9-a3e5-83785ef72281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 171,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f02b921f-5771-4f22-845b-325a7c0b0e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "73f7c29a-b236-48c3-b204-4f9878114786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "615215d6-513c-438c-b73b-19b9aa367e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3d3d50ae-8ddb-4f57-a6a5-41e0245a90b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a3df2f55-ca67-49ae-81d9-79aee17c7c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3e7e4880-9204-437e-a9fc-b3473f043b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c294e51f-488d-4a37-82f4-a237de49eae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 152,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a8c02a86-ee2f-4797-9b97-3a69f1ad704d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0236e66e-b602-448f-9692-cd2ce1e83149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5e51b540-b6da-447d-bd3d-6590c422996c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1fa0abc0-f5eb-4cbb-be81-6505c30a99d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "04e19b42-1e8b-48cb-9fed-3abf18719c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "138b0a00-ae38-4bc3-9f5c-3d84dab3a430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c2e32c1f-d0ec-40c0-a0c2-db63f436b48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c39dde78-9e1a-47ff-ae00-d10d604fe460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cfe4e5ac-b19b-4db4-ac3b-12a4fc36e1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "069550f3-67af-4916-b0da-efe6cee996f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "89f8480e-9acc-41a6-8943-6f7d52f9d317",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 145,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ebcb9885-633d-49f5-9196-46fa2f23a50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ca1e7d8d-045d-404e-a8cd-bf2960bf9619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 127,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e9305622-a90e-4449-8bb0-198039cb3954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "177a7b87-2952-4faa-ae4b-a4e314917fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f9bb044f-9870-4945-a6b6-829a2244e005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 133,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6c983e2d-4c32-4014-bbb6-299cc3c0f7b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "910d918f-7645-49c6-bea6-5792af272c0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5657876e-ae58-4952-92b5-31d64c645f50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d15fe0c5-5bf3-42f1-9749-86c28fbd842f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ad03e556-a35c-449a-afe9-b6a9d4210673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 108,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8e97b416-50ec-4621-9ee6-dd278acecacc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fb269631-7276-4a4d-8a97-e1b888b73e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "98839175-4824-4641-a1e8-1c2184e5ccbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "30f8cf04-c446-4e8e-a570-2a803bca9705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 194,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4e87ac48-f335-469a-aaa8-e7504b4d9503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d47fd027-fde6-49c8-a930-d993eaf2b48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6494f8ea-3c6f-458c-9a62-bf5f34e6b052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8dd15098-e18d-4965-bec5-c836867cb2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b4d6fe44-1b53-4e9c-96e4-5e4dd04e3093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "594ade20-d405-47f9-b48d-aa84d570ee5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "dfe3a65e-6d65-4d83-85b3-02b03129dff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5af3c722-369b-418b-8cf4-9ff028f75f12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 111,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c751a5e7-d61f-4324-8333-7ed4d1ffe520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bb1cdc7a-6330-48f7-87c9-376640269692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "01e0b04e-3392-4c18-b58f-8b4f557a3012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3628e078-6339-4664-b10c-e4667673aad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0db84656-a475-403b-9f59-0f7dc33b9914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 181,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d9b05aa1-dfb9-4102-be6a-8852a0e1c168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d184f1a7-c58c-4ad0-bd15-77a428f58e72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a95e2664-41f4-43dc-8a7c-bc8929931c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "445bc24b-c5a4-424c-98d8-a3c18f59eedd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e1020d4f-3397-4295-98a9-c6599e9c8128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e493278f-4745-4092-aa13-8900e6818eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 190,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2a0ea06a-e016-4c22-b09d-e81b0b798b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6de6a972-6496-458f-94c7-5a6f36ac0747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 163,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "71ba42cf-aeef-44c4-9a31-483f39c21f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "2be2b30f-1884-49f6-968b-650baa80ba02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "7fb7d524-15cc-4f12-b934-ed2fe4802ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "ab9b77f1-4a9c-452f-8f4c-f95adfab93a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "ef630de1-d64e-4257-ac5d-374fd6f70839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "4ca11d2e-5164-4478-9bc5-55f04c2af97b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "d59fe2c2-41ec-4082-8b5f-65f2b9d893c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "1b77cbe7-94fd-4beb-9cb0-ce488265f897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "90888c25-e624-4c17-bf4f-0a7fbc304e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "dba3c1a2-3935-4923-9f55-a3ddbe8058c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "2666a00b-5f9f-46df-a72d-e6ff98e7ab1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "0312d989-35b2-4487-a58f-a037938568cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "82acdffb-e381-48e8-8f4a-25e4c3f87324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "f3913dfc-c575-423d-8526-f26b0c19f5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "cb964c5b-7777-486f-a337-9c0ac56ba203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "3c2c3c93-e5f4-44e8-9006-d95276f650a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "efaa0d28-a85c-46cc-8bb1-8f82d4e96518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "92e265e2-a74e-4ea9-8b56-9fee0c54f877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "d96b570d-2f19-4bbd-ac5f-0d0ea611681e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "bc7adcb9-8622-47e1-998f-dd201f0b6b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2140c2d9-c25e-4891-b41f-dd9ef5151c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "64b269a1-9d97-427b-90ee-d8c5f1723293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "1472f327-8d37-4cbd-9ef4-9f1c6dbcff7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "6d87620f-d024-4e0f-a164-4fc9a353e1bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "60c65b7f-ea03-4891-8d42-5c00de1a2b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "98e4e928-1cd1-4127-bcac-595cb6217095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "1c4d2562-54cc-455d-9919-4390bbd3e086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "e5435dc4-63b3-40b8-aa17-9596e68c0cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "f68572db-ab4d-4231-ae10-7f4d593cf268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "7a892729-b0ab-4bd5-89c4-84732a9b8295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "226a7872-5eec-4fe1-a384-6b157f865960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "96658b28-454f-4b45-a941-1320252dfeb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "442c97a5-f26a-4e84-9fc8-b3888c0dac95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "8a23fce8-723f-4db4-974f-1c7664264b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "1b0dbabb-4cd6-4804-81a1-7ca1a355d333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "22d0ac3d-6620-4f73-b5e6-1863fe2292c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "935735c4-5b33-46bd-85b4-5963088fd67e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "d40fff13-c72a-46a8-9f57-ec069e8b8a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "82829b53-862e-437d-8239-650799844f39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "7d22e4e5-b8f0-4606-827c-665f23d2ba26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "29df1f3d-f170-41c1-b85b-2f40d76f9bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "9abd2f55-5b3f-4db9-91b7-519832d16aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "9639ecdc-01ac-43d0-bcb2-60823b5e7679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "7b494d79-f756-40f4-a066-fa65c4faa5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "633c8201-6260-4a03-8247-b288476269ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "fb0e1ebc-605f-4055-91ce-d345b370f6df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "b336ce33-a659-4652-9f09-8a8c32f5698e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "ac91b729-1a2b-4d17-b67f-e81b6da5d370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "61e17771-b786-4042-a733-a3d9803e4e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "5ad647cc-5454-4001-8ffa-f9af3572ba64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "fcab9b40-d1f2-49cd-82c0-f27e45f30798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "227e2ae3-751e-4846-9f87-a1153bdadd8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "25c47d72-fd5c-44a6-98ad-a4aabed82d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "3f3c9a35-135d-43dd-8e3d-d8eaf649253c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "078be8fd-3aad-4b45-ae88-8078ed786327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "6cdc205d-a340-4b28-84f8-82d6aec441b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "4274c906-66c3-4698-b8b9-d8e3187a78fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "4b410c2d-2e39-4dec-8e1d-f37b75cb6aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "10ded4ea-1b03-4e0c-897f-6fdb19c70589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "06b03aca-ce3c-47c0-8ebd-416c9a673a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "bc01655f-c71e-4097-9e4d-a1d8e24c683c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "9b44535d-4b78-40e9-96b0-870801ce782e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "a8d2095c-74ef-4108-a2fd-e6092dbff18b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "afcc360e-390b-451b-a88d-f43fa06b56d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "8b163623-b671-4dd1-ba83-f9c5308e8d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "792844ee-e085-4a25-a1bc-523f8b12c273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "b0cbf66f-17f0-4c8a-9750-4e5474508f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "45f8a516-d397-4800-bf4c-c8a09d60fb68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "68027404-e33b-4291-94a4-559f4e11c9f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "269ea381-face-4a25-821a-36a44a2c8bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "72cda84a-bf17-4d42-a363-2414cb88064e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}