{
    "id": "cec2c7fa-39ba-4d31-81b8-7c1f4a41557e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "Level_Font",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "41438a41-d38f-427e-b43c-49e0570a61f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 93,
                "y": 72
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "db54b702-e369-40c6-8242-18535aaad8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 68,
                "y": 72
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3bc430b0-4336-45bd-ac4e-5204ba6b948a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bb5a0e2c-d789-4f7a-a25b-3d6eeb57f0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2c8dcbbe-f213-4bf3-a9b6-c6c8ef00d7e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c5c86545-542d-4241-95ba-e2ec45b31eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a5cd6c96-0f5a-4327-9826-d0ac283f8219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 102,
                "y": 16
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e4fcb05a-b34c-48bd-b5b2-672d19527686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 113,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c85370b5-0c44-4e31-bf44-d861adf2e1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 14,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "67a68d6b-60b9-4976-89e1-c91912b9ba5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4cf963ab-f1dd-423f-ba7c-3a6929f1a9de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 120,
                "y": 58
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "34d84a98-34f0-4334-b637-b36c4c37e1db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "399d1a5c-e765-4842-bb80-63b3aa00d538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 103,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "86133301-de83-4aa9-925b-8bf2b7e705f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cc997257-f3d3-43cb-8bed-3963acbc22ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "83984ad1-9a56-4a80-a463-634b24bfccaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 38,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c8b09d82-fbbe-422f-8cb4-a29429478855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "be6bc278-fc12-4d1c-8316-d30f5677da6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 44,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "82986b9b-3692-4643-8c0b-6e60aa00080d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8e12831e-26ca-437f-a899-6cc81b68608a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8ed82ab5-491d-4a14-9328-b414c4cf0611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fc33d0cc-55f1-48a2-9161-3faf6a9a944d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c5ea00a6-201e-4df0-a114-7eaf7ddfde94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "98c1bd1c-4cb0-4ec8-b9cb-0a166600e509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6ae03780-1ad9-4842-9b7b-1368135dc6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a937005b-4e27-49bd-b9a0-446538664ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4689f2ef-cbe8-4efd-a26e-14ee736dc34d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 117,
                "y": 72
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7b582fe4-2e4e-4d41-b06b-e84421e603fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 73,
                "y": 72
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d6686d43-9962-4c1e-910c-325e5cd72db1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f59b2aa3-1375-4421-b785-25963c6c092d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "63f25ba8-d011-47d2-9198-b9eb0a084284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5c092863-756e-48ad-ba75-4f582bc654dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f6a3d1cc-0f5b-4787-83e9-d8f371e41a8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "19360b19-293f-4439-8b53-15f359c4af36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b01140c9-4840-422a-b143-7cfff21d88fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 30
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b1622af1-0d25-400f-a5c2-2e7d01090a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 16
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "556587e4-3a08-42f2-83d9-8356c1cc6eb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7c98bd12-aa7c-4344-9dd4-81fe8862b538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 30
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c70df874-5edd-4a6f-a60d-9da0182c4e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 30
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "319e58c9-6aca-40ad-8784-d5a0c743afc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 16
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bddd7767-3816-4f2a-8495-90edbce9d839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e5a20d6c-ed40-4e71-9a1c-c326a6872173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 6,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "93962f24-745b-401c-b0fd-3e590c56d492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 58
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8d87c1b0-d4fc-4aff-ad23-48003b7fa1d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 32,
                "y": 16
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "373fbeb3-2012-49de-96c2-42c47a82ffe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6419ec8b-e522-4cbe-9b03-859403d4a29d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0a102388-98bd-4c7b-a300-a1f6dc552a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 82,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ae0bb475-c4e7-4146-af13-00a3ee1e0770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5382c5dc-b8f1-4d83-a9a4-1b5bef77e14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 112,
                "y": 16
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c9a1b15f-979e-4e7a-9373-d1d1b2999a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0d74a435-d0bd-49d8-9a80-0d36fcf9ae6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "761c03db-8712-44c5-a70b-5329b7c27411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0bf684f2-ff30-4e7a-9374-8bfc550ea67c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4f6402ab-5bd7-4570-a75c-448adaba9427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 16
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "abb566c7-8562-41cb-8741-e0c2da2787fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 22,
                "y": 16
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6529803d-b677-408e-9384-3ccd17e0f031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d786edbe-1868-48dc-95d5-3d5122130e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 42,
                "y": 16
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "62ad5700-ba89-4c07-af65-46ad5a6cf5ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9c73a182-264e-450d-8cdf-30f7845ba97d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "989e4a47-dbc7-4e4e-8710-8512e27f82d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 78,
                "y": 72
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "324e97a4-436e-4689-bdc0-599b97b30ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 50,
                "y": 72
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "662f4bfe-c31d-4ee1-b07d-cad5f0bc690c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 108,
                "y": 72
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2b66d25b-856f-4e2d-bd73-7ac61f1f3822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 106,
                "y": 58
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "894b0a66-8683-4d07-b082-c0810bff6713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 72,
                "y": 16
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5446aceb-de98-4ea5-99cc-ac189a754236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 98,
                "y": 72
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4c8c7093-121a-4cb7-a8c0-25079be987b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c9010e2d-e0cd-49b7-b198-3b8e9e96e3ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8a9dc1f5-cc7f-4971-9b35-9f2416849c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d114aa32-9ef4-4675-95e4-31b47c0470f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4fbaf642-4059-42c6-953c-e4f6432d77c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 104,
                "y": 30
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6364b131-4167-4fa5-a7e9-9380a6367d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 62,
                "y": 72
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ba56f95c-6417-4198-bc55-05d876a57c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 44
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a752b09e-72aa-4a54-a04b-593789e557e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7fb9b3e6-3e8c-42e8-83cb-9ae65bcf45ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 121,
                "y": 72
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "50d88bda-f904-4f4a-842a-fbe578627ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 83,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a3e8d749-a7e6-41c9-b286-e15c0a09d1da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "07d3c305-0e07-4c3a-b731-8e2b21d10e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 10,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1f49abf8-e82c-4274-b673-8ab93dc31dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0c226be2-7bef-4de7-95f2-073478f97f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fe196589-a15c-4144-840d-93197d7f7bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "54e270c5-6892-4b05-b09f-ab4399f579d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3b83ff04-6f9d-4304-ac5c-e395593f3ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 96,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "478edaa9-17d8-4a5f-bbc5-6ccd3fc9c8fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 32,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ab3ed852-7230-473a-9ac7-716a73cf9915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2e02887a-a3d6-47c3-ba67-91f65392b58b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 88,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2691259a-cedb-4d47-8338-f68da7f64b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 88,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5bab130a-cba4-4c00-9f19-0ce337ecd650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 80,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7bd60b20-a9b1-4ebd-835a-50f35e0d4fa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 92,
                "y": 16
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bd5e60ad-addd-4afa-8d5d-d067fafed8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 72,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "dc3b4082-f2e2-42b2-bc8f-d1db7fea8d64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 64,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0dff3809-28cf-4b63-a853-37d03e016f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "793016d7-5f45-4650-aeb1-e42b8ccdc27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 72
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "20f9ef95-8755-406e-bcf8-fd0349f01b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 14,
                "y": 86
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9f88e45f-37e0-430f-8d27-1e1a26886f67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 56,
                "y": 72
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6c80f59d-6a0e-46ab-9046-3a9d61adfd50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 44
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "51f42e6d-6eea-4a9c-8cd9-576802290bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "ba30835b-d142-4793-b8ac-12f79d9ce305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "5731b876-64e5-46fc-a092-d2a1a64102ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "88d60ea1-a67c-49b8-9aaf-2114b825dd14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "0dbd1b72-676a-40ed-b7a0-dfacac52e192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "4418ae73-050f-48fd-a33e-01809660213a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "347de413-37f1-414e-81f7-b00ef9307530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "fe9afac5-796f-46ad-8c53-fb426ff2ab74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "bde7b6bc-a88a-4067-a605-4145f30a9617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "853b9e23-c9a9-44e2-9216-c5e98602ba3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "cf6a53cd-bc9e-4e05-bb83-7faf8870d461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "3b98258f-26d1-4364-bf88-9e76a7240d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "ab0a5b15-9dde-4f0c-96d9-573bfa85e3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "0710822f-95f0-4390-a552-61e4c0fdcc62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "dc97e7d7-4236-4030-8f33-f22cf0ba38b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "b075dd9c-4dea-4f1c-969d-0c9cde653b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}