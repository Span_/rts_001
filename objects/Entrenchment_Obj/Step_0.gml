/// @description 

Building_Step();

if (cooldown > 0) cooldown -= 1;

if (!complete) return;

switch (state){
	case state_enum.idle:
		
		sprite_index = Entrenchment;

		if (instance_exists(Enemy_Unit_Obj)){
			var foe = instance_nearest(x,y,Enemy_Unit_Obj);
			if (distance_to_object(foe) < aggroRange)
			{
				state = state_enum.attack;
			}
		}
		
		break;
	case state_enum.attack:
	
		if (instance_exists(Enemy_Unit_Obj)){
			var foe = instance_nearest(x,y,Enemy_Unit_Obj);
			if (distance_to_object(foe) < aggroRange)
			{
				if(cooldown < 1){
					//keep image facing enemy
					if (foe.x < x) {
						sprite_index = Entrenchment_Attack_L;
					} else {
						sprite_index = Entrenchment_Attack_R;
					}
					image_index = 0;//restart the animation
					
					part_system_position(global.Part_Emitter, foe.x, foe.y);
					part_emitter_burst(global.Part_System, global.Part_Emitter, global.Bullet_Impact_Part, 1);
					
					foe.curHealth -= damage;
					//bullet = instance_create_layer(x,y,"Inst_Units_Layer", Bullet_Obj);
					//bullet.damage = damage;
					//bullet.direction = point_direction(x,y,foe.x,foe.y);
					//bullet.direction = bullet.direction + random_range(-1,1);
					//bullet.image_angle = bullet.direction;
			
					cooldown = attackSpeed;
				}
			} else {
				state = state_enum.idle;
			}
		}
		break;
	default:
		Unit_Idle();
		state = state_enum.idle;
}