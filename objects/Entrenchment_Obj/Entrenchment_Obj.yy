{
    "id": "b2d20c3a-cedb-43fd-a65a-bf35aab4a932",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Entrenchment_Obj",
    "eventList": [
        {
            "id": "c95857d2-0a4f-4c4a-8a6a-24c7ff6b8914",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2d20c3a-cedb-43fd-a65a-bf35aab4a932"
        },
        {
            "id": "268c62a3-303b-4703-89b0-9d0e16ffa908",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2d20c3a-cedb-43fd-a65a-bf35aab4a932"
        },
        {
            "id": "f4650890-4f21-4ec3-9d18-d1cec4482389",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b2d20c3a-cedb-43fd-a65a-bf35aab4a932"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "3215dc94-d254-402f-ac6e-1138b1aa3de7",
    "visible": true
}