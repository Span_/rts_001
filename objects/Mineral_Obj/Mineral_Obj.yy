{
    "id": "dae57c98-5628-4db0-b575-0a99323bb4bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Mineral_Obj",
    "eventList": [
        {
            "id": "5e291976-bac6-413a-886e-4d8eeaa3a87e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dae57c98-5628-4db0-b575-0a99323bb4bd"
        },
        {
            "id": "e02aef90-1c95-48f0-9ccf-a000a941b1b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "dae57c98-5628-4db0-b575-0a99323bb4bd"
        },
        {
            "id": "9b38e6a9-8232-4bb5-92ff-db27a38bc38b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dae57c98-5628-4db0-b575-0a99323bb4bd"
        },
        {
            "id": "032c7f5a-4c28-4543-8e80-7d8b9a14201d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dae57c98-5628-4db0-b575-0a99323bb4bd"
        },
        {
            "id": "ff7938af-b9e5-4450-b184-87e9d9d007dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dae57c98-5628-4db0-b575-0a99323bb4bd"
        },
        {
            "id": "5d6b289d-a2e9-4a1d-9f1b-7b7f36cd8398",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dae57c98-5628-4db0-b575-0a99323bb4bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bc29aca6-c515-42ee-aee4-5fbd85504e54",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "67c24776-6b60-488e-8578-f2a8715373ec",
    "visible": true
}