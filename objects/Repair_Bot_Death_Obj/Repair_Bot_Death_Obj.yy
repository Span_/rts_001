{
    "id": "818f571b-143f-4068-8d3a-aec31a58bb49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Repair_Bot_Death_Obj",
    "eventList": [
        {
            "id": "49607f9d-a5dd-4402-ae0c-ed051e78452f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "818f571b-143f-4068-8d3a-aec31a58bb49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
    "visible": true
}