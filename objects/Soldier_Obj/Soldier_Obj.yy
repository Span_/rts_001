{
    "id": "524a5f94-4619-472f-beae-641a9cf7480a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Soldier_Obj",
    "eventList": [
        {
            "id": "9d88ae95-8988-4154-8141-8f0011e80937",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "524a5f94-4619-472f-beae-641a9cf7480a"
        },
        {
            "id": "6f55c621-fef9-4af9-bd17-b2ba9e0e29b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "524a5f94-4619-472f-beae-641a9cf7480a"
        },
        {
            "id": "144b5552-c8a0-4ada-b2c9-31d3498a3e49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "524a5f94-4619-472f-beae-641a9cf7480a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "8b604942-a833-4e54-9af4-3833af2400a1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "6db5eb6c-358e-4419-bcc9-02faf6b3b6c4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "606bf862-fe5a-4b9f-8c3d-973b771420ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "330fa231-b4d4-46a7-a964-618a3a5465ee",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "9b6ac377-9210-4587-896b-3cd9a7b37a21",
    "visible": true
}