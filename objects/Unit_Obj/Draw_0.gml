/// draw selected


if (!surface_exists(Lighting_Obj.light_surface)){
	//Lighting_Obj.light_surface = surface_create(room_width, room_height);
	//light_surface = surface_create(view_wport[0], view_hport[0]);
	Lighting_Obj.light_surface = surface_create(room_width,room_height);
	//layer_element_move(light_surface,global.Lighting_Layer);
}
surface_set_target(Lighting_Obj.light_surface);
gpu_set_blendmode(bm_subtract);
draw_set_alpha(1);

draw_set_color(c_white);
draw_circle(x,y,aggroRange,false);
draw_set_alpha(0.2);
draw_circle(x,y,aggroRange * 1.2,false);
gpu_set_blendmode(bm_normal);
draw_set_alpha(1);
surface_reset_target();


if (selected) {
	draw_set_alpha(0.25);
	draw_circle_color(x, y, 8, c_lime, c_lime, true);
	draw_set_alpha(0.1);
	draw_circle_color(x, y, 8, c_lime, c_lime, false);
	
	//draw aggroradius
	draw_set_alpha(0.25);
	draw_circle_color(x, y, aggroRange, c_blue, c_blue, true);
	
	draw_set_alpha(1);
}


hp = curHealth / maxHealth * 100;
draw_healthbar(x-5,y+12,x+5,y+10,hp,c_black,c_red,c_lime,0,true,false);

draw_self();