/// @description stop

if (selected) {
	state = state_enum.idle;
	if (target != -1) {
		if (instance_exists(target)) instance_destroy(target);
		path = -1;
	}
	
}