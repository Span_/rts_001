{
    "id": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Unit_Obj",
    "eventList": [
        {
            "id": "93955281-3b1d-4ce7-a923-cff5819577d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        },
        {
            "id": "3295b591-b378-43f2-86d9-f0f34b503202",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        },
        {
            "id": "65e5c10b-79b5-4196-8632-c4fcd6c9ab3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        },
        {
            "id": "ef6de9c1-7f48-4952-a0f6-ebe070c39cfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        },
        {
            "id": "487a2ee3-dd4f-4cc3-9124-38af30aeb94d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        },
        {
            "id": "7b2a4490-5930-45be-9f43-97f26252befb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        },
        {
            "id": "7bc7dfbd-7863-465b-a8f1-595395c23501",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        },
        {
            "id": "84dd2b81-4f2c-4340-a48b-f553228333bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c8f7eb0c-99e7-4af3-a0d5-28500298a59d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ebadfb14-2b0e-4693-9da6-404cf6b99f11",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c172750d-5bc5-48a0-a2ea-759c7afa53a5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "29913239-7c8b-4ad8-9dbd-6cf644c70351",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "baad7568-b102-4bfb-a0e3-149c816815e2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}