/// @description Make GUI Array
Build_Cost_Data_Init();
GUI_Array_Init();

//instance_create_layer(64,64,"GUI_Layer_1", Square_Button_Obj);


global.wCamInit = camera_get_view_width(view_camera[0]);
global.hCamInit = camera_get_view_height(view_camera[0]);

enum menuArray {
	main_menu,
	build_units,
	build_buildings
}
curMenu = menuArray.main_menu;

//make new one , main menu
//build units
instance_create_layer(global.GUI_Array_Build_Menu[1,0],global.GUI_Array_Build_Menu[1,1],
					global.GUI_Array_Build_Menu[1,2], global.GUI_Array_Build_Menu[1,3]);
global.GUI_Array_Build_Menu[1,3].toolTip = global.GUI_Array_Build_Menu[1,4];

//build buildings
instance_create_layer(global.GUI_Array_Build_Menu[2,0],global.GUI_Array_Build_Menu[2,1],
					global.GUI_Array_Build_Menu[2,2], global.GUI_Array_Build_Menu[2,3]);
global.GUI_Array_Build_Menu[2,3].toolTip = global.GUI_Array_Build_Menu[2,4];
		
//for (var i = 0; i < array_height_2d(GUI_Array); i++;) {
	   
//   instance_create_layer(GUI_Array[i,0],GUI_Array[i,1],GUI_Array[i,2], GUI_Array[i,3]);
	  
//}

Init_Grid_Path_Units();
//units_selected = 0;
//buildings_selected = 0;

global.all_buildings_list = ds_list_create();

global.units_selected_list = ds_list_create();

//hotkey control groups
global.units_selected_list_0 = ds_list_create();
global.units_selected_list_1 = ds_list_create();
global.units_selected_list_2 = ds_list_create();
global.units_selected_list_3 = ds_list_create();
global.units_selected_list_4 = ds_list_create();
global.units_selected_list_5 = ds_list_create();
global.units_selected_list_6 = ds_list_create();
global.units_selected_list_7 = ds_list_create();
global.units_selected_list_8 = ds_list_create();
global.units_selected_list_9 = ds_list_create();