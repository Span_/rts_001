/// @description 

//clear the list if not holding down shift, otherwise, add to the group
if (!keyboard_check(vk_shift)) {
	
	
	for (var i = 0; i < ds_list_size(global.units_selected_list); i ++) {
		global.units_selected_list[| i ].selected = false;
	}
	
	ds_list_clear(global.units_selected_list);
	
}