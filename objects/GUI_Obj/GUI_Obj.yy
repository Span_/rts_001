{
    "id": "71ba586a-c5c0-4857-9b3b-f5c5624b3ed6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GUI_Obj",
    "eventList": [
        {
            "id": "be6e2e98-2eda-4739-8f34-8872e842f8d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71ba586a-c5c0-4857-9b3b-f5c5624b3ed6"
        },
        {
            "id": "969b89a5-d8e9-4832-a0d6-de24b183be54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "71ba586a-c5c0-4857-9b3b-f5c5624b3ed6"
        },
        {
            "id": "2951fd1e-59a0-41d0-9048-8a6c3ae10081",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "71ba586a-c5c0-4857-9b3b-f5c5624b3ed6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2166f53-d291-4ee0-b960-1864d899886d",
    "visible": true
}