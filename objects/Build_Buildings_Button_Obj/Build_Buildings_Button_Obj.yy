{
    "id": "cc54616b-3b29-49bd-83dd-7cb4a7d4fcda",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Build_Buildings_Button_Obj",
    "eventList": [
        {
            "id": "25eff8e8-1ed3-4a2d-8be4-7aebce634f36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cc54616b-3b29-49bd-83dd-7cb4a7d4fcda"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "65ae33b0-3ad1-46a9-bc80-6de39fd9e41b",
    "visible": true
}