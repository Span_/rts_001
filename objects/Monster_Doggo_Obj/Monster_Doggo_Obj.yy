{
    "id": "f4903dab-e5f6-43cb-b64b-41e956ba0d3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Monster_Doggo_Obj",
    "eventList": [
        {
            "id": "acb4f0ad-c3d1-440f-9c57-71d3e3e87fa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4903dab-e5f6-43cb-b64b-41e956ba0d3b"
        },
        {
            "id": "f17b9b6a-9a60-4f3c-942e-008d153b3861",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f4903dab-e5f6-43cb-b64b-41e956ba0d3b"
        },
        {
            "id": "04c44ae7-160e-4a21-aec1-7381cda8588c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f4903dab-e5f6-43cb-b64b-41e956ba0d3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "f19a1a59-808c-4100-a4f1-7f9989373986",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4bd735c4-6ad0-42d2-b816-1e216b2452bc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "ba05b0cb-ce28-48af-9482-044e967cacfd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "e6334fdc-5ca5-4a15-8425-70723a3401d7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1bf5158e-e1af-4ad7-b221-d4772e1e68e9",
    "visible": true
}