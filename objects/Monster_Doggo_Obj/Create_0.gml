/// @description 

//foe = -1;
target = -1;
destination = -1; //for attack moves
path = -1;
pathPos = 1; //position along the path
//pathing = false; //currently moving along the path
//pathDepart_X = -1; //if we need to depart the path, save the progress
//pathDepart_Y = -1;

aggroRange = 75;
attackRange = 10;
movementSpeed = 2;
damage = 3;

expVal = 10;

maxHealth = 25;
curHealth = maxHealth;

attackSpeed = 30;
cooldown = 0;

//retreatDuration = 20;
//retreatCooldown = 0;

//wanderSpeed = 1;
//wanderDuration = 3;

//state = "idle"

state = state_enum.idle;

//bad doggies
//state = state_enum.attack_move;
//destination = instance_create_layer(room_width/2,room_height/2,global.Inst_Units_Layer,Target_Obj);

