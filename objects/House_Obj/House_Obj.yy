{
    "id": "a1a1d6f2-99c2-4769-b06a-0f017160854a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "House_Obj",
    "eventList": [
        {
            "id": "b4567de2-fcf9-43bb-b476-696496d94691",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1a1d6f2-99c2-4769-b06a-0f017160854a"
        },
        {
            "id": "884ba5bb-2c7d-4c9b-b7d6-9abcf155c392",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a1a1d6f2-99c2-4769-b06a-0f017160854a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c15ab208-5b7c-4717-9915-ff0c9e3ec078",
    "visible": true
}