{
    "id": "9543ef56-ddb1-492a-ac21-ce8742e47ab1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_House_Button_Obj",
    "eventList": [
        {
            "id": "e4cdf8d8-8266-4746-99b2-8a10737ead4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9543ef56-ddb1-492a-ac21-ce8742e47ab1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f37db43b-2888-4e16-89ae-e747b20f3327",
    "visible": true
}