{
    "id": "ac166353-e661-42ae-b01a-74f9320f7991",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Horse_Mage_Death_Obj",
    "eventList": [
        {
            "id": "cd276ebc-b4df-4654-9eb3-b7e2e30c25b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ac166353-e661-42ae-b01a-74f9320f7991"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
    "visible": true
}