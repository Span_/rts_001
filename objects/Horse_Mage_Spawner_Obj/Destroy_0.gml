/// @description explode blue


effect_create_above(ef_explosion, x, y, 0.01 * sprite_width, c_blue);


var d = instance_create_layer(x,y,global.Inst_Units_Layer,Horse_Mage_Obj);
d.image_xscale *= 2;
d.image_yscale *= 2;
d.image_blend = c_red;
d.damage *= 4;
d.curHealth *= 10;
d.maxHealth *= 4;
d.movementSpeed /= 2;