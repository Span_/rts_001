{
    "id": "22313eb1-d518-4a09-bbd5-ac1a2f348aa4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Horse_Mage_Spawner_Obj",
    "eventList": [
        {
            "id": "d1f6a63b-ea21-4d23-8ba6-141fc8b66c6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22313eb1-d518-4a09-bbd5-ac1a2f348aa4"
        },
        {
            "id": "24120bb6-b726-4d1c-9a24-2c46a1e33946",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22313eb1-d518-4a09-bbd5-ac1a2f348aa4"
        },
        {
            "id": "514cd262-0f13-471e-93c0-2ea44dea2406",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "22313eb1-d518-4a09-bbd5-ac1a2f348aa4"
        },
        {
            "id": "a6b2ae8c-14c9-47d5-a117-12cf8aebb45a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "22313eb1-d518-4a09-bbd5-ac1a2f348aa4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c1c90edd-7d55-4e03-9a84-26a5b20b6c48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "7f8b5152-c59d-4ab3-ba5a-f449b2c6aa89",
    "visible": true
}