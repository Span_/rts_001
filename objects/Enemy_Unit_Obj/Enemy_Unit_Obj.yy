{
    "id": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Enemy_Unit_Obj",
    "eventList": [
        {
            "id": "a0bdce0b-59ae-49dc-b787-cb4b571d398c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2"
        },
        {
            "id": "4244a8a8-7d5c-4a75-b328-545651e82f82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2"
        },
        {
            "id": "a7d136db-d37e-450e-9538-659444914159",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2"
        },
        {
            "id": "0b9caf19-990d-4104-a78c-7a82ccb46cd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2"
        },
        {
            "id": "fd7b2de5-c011-4ed0-b827-3ddd5fc3348b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2"
        },
        {
            "id": "7588a4f9-755c-4755-bbb4-f22425cf1f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "84cfa824-edd4-492d-9374-79b958c3509f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c8f7eb0c-99e7-4af3-a0d5-28500298a59d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "0a350643-893e-4896-b5ba-8450de6265c8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9c866398-2357-4892-9c0f-117e8a07e1f2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "267a8fd1-20d2-4bb8-a5ce-258b1498e3f6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "dfeca5a5-7203-4081-a5de-bc7445b760b1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}