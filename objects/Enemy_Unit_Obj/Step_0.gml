/// @description Enemy AI

if (curHealth <= 0) instance_destroy();


if (cooldown > 0) cooldown -= 1;
if (retreatCooldown > 0) {
	retreatCooldown -=1;
} else {
	if (instance_exists(Unit_Obj) || instance_exists(Building_Obj)){
		
		foe = instance_nearest(x,y,Unit_Obj);
		foe_building = instance_nearest(x,y,Building_Obj);
		
		//look for nearby foe
		if (distance_to_object(foe) < aggroRange)
		{
			if (distance_to_object(foe) < 1) {
				//speed = 0;
				//attack
				if (cooldown <= 0) {
					//attack
					foe.curHealth -= damage;
					cooldown = attackSpeed;
				}
				//retreat
				//retreatCooldown = retreatDuration;
				////state = "retreat"
				//direction = point_direction(foe.x,foe.y,x,y);
				//speed = movementSpeed;
				//image_angle = direction;
				//Upright_Sprite(1);
			
			} else {
				//chase
				//move_towards_point(foe.x,foe.y,movementSpeed);
				mp_potential_step_object(foe.x, foe.y, movementSpeed, Path_Avoid_Obj);
				image_angle = direction;
				Upright_Sprite(1);
			
			}
			//no nearby foe, look for foe building
		} else if (distance_to_object(foe_building) < aggroRange){ 
			
			if (distance_to_object(foe_building) < 5) {
				//speed = 0;
				//attack
				if (cooldown <= 0) {
					//attack
					foe_building.curHealth -= damage;
					cooldown = attackSpeed;
				}
			
			} else {
				//chase
				//move_towards_point(foe_building.x,foe_building.y,movementSpeed);
				mp_potential_step_object(foe_building.x, foe_building.y, movementSpeed, Path_Avoid_Obj);
				image_angle = direction;
				Upright_Sprite(1);
			
			}
			
		} else {
		//target out of range
		//idle
		Enemy_Unit_Idle();
		}
	} else {
		//no target
		//idle
		Enemy_Unit_Idle();
	}
}