{
    "id": "326a4d01-d165-41ea-8acd-745a1a8cfe06",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_Worker_Button_Obj",
    "eventList": [
        {
            "id": "7a321511-2565-458a-8c36-e530ff9349fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "326a4d01-d165-41ea-8acd-745a1a8cfe06"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0a99f56-42de-497c-a3fb-8c7312a33ceb",
    "visible": true
}