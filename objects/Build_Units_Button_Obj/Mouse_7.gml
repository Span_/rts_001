/// @description 

GUI_Obj.curMenu = menuArray.build_units;
//build units menu
for (var i = 0; i < array_height_2d(global.GUI_Array_Units); i++;) {
	   
   instance_create_layer(global.GUI_Array_Units[i,0] * camera_get_view_width(view_camera[0]) / global.wCamInit + camera_get_view_x(view_camera[0]),
						global.GUI_Array_Units[i,1] * camera_get_view_height(view_camera[0]) / global.hCamInit + camera_get_view_y(view_camera[0]),
						global.GUI_Array_Units[i,2], global.GUI_Array_Units[i,3]);
	//tooltip
	global.GUI_Array_Units[i,3].toolTip = global.GUI_Array_Units[i,4];
	//set init x
	global.GUI_Array_Units[i,3].xPosInit = global.GUI_Array_Units[i,0];
	//set init y
	global.GUI_Array_Units[i,3].yPosInit = global.GUI_Array_Units[i,1];
}
//add back button
instance_create_layer(global.GUI_Array_Build_Menu[0,0] * camera_get_view_width(view_camera[0]) / global.wCamInit + camera_get_view_x(view_camera[0]),
					global.GUI_Array_Build_Menu[0,1] * camera_get_view_height(view_camera[0]) / global.hCamInit + camera_get_view_y(view_camera[0]),
					global.GUI_Array_Build_Menu[0,2], global.GUI_Array_Build_Menu[0,3]);
//tooltip
global.GUI_Array_Build_Menu[0,3].toolTip = global.GUI_Array_Build_Menu[0,4];
//set init x
global.GUI_Array_Build_Menu[0,3].xPosInit = global.GUI_Array_Build_Menu[0,0];
//set init y
global.GUI_Array_Build_Menu[0,3].yPosInit = global.GUI_Array_Build_Menu[0,1];

//destroy current menu
instance_destroy(global.GUI_Array_Build_Menu[1,3]);
instance_destroy(global.GUI_Array_Build_Menu[2,3]);