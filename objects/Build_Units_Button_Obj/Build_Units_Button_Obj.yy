{
    "id": "5d5ac3e0-dc12-4c12-bb03-cce8a67b5b42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Build_Units_Button_Obj",
    "eventList": [
        {
            "id": "1086b9ff-2766-4b78-ae53-6818505cead5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "5d5ac3e0-dc12-4c12-bb03-cce8a67b5b42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83d58843-47ae-44d2-a1fb-43db37933084",
    "visible": true
}