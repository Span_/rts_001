///@description Set & Apply States

//Unit_Step();
if (!counted) {
	//add self to unit score
	counted = true;
	if ( instance_exists(Score_Obj)){
		with (Score_Obj) units +=1;
	}
}


if (curHealth <= 0) instance_destroy();

if (cooldown > 0) cooldown -= 1;


Soldier_State_Scr(Sniper_Idle, Sniper_Attack, Sniper_Move, global.Bullet_Impact_Part);

//switch (state){
//	case state_enum.idle:
//		Unit_Idle();//friction = 0;
		
//		sprite_index = Sniper_Idle;

//		if (instance_exists(Enemy_Unit_Obj)){
//			var foe = instance_nearest(x,y,Enemy_Unit_Obj);
//			if (distance_to_object(foe) < aggroRange)
//			{
//				state = state_enum.attack;
//			}
//		} else if (instance_exists(Enemy_Building_Obj)) {
			
//			var foe = instance_nearest(x,y,Enemy_Building_Obj);
//			if (distance_to_object(foe) < aggroRange)
//			{
//				state = state_enum.attack;
//			}
//		}
		
//		break;
//	case state_enum.attack:
	
		
//		if (instance_exists(Enemy_Unit_Obj)){
			
//			var foe = instance_nearest(x,y,Enemy_Unit_Obj);
//			Soldier_Attack_Scr(foe, global.Bullet_Impact_Part, Sniper_Attack);
			
//		} else if (instance_exists(Enemy_Building_Obj)) {
			
//			var foe = instance_nearest(x,y,Enemy_Building_Obj);
//			Soldier_Attack_Scr(foe, global.Bullet_Impact_Part, Sniper_Attack);
			
//		} else { state = state_enum.idle; }
		
//		break;
//	case state_enum.move:
//		sprite_index = Sniper_Move;
//		Unit_Move();
//		break;
//	default:
//		Unit_Idle();
//		state = state_enum.idle;
//}
