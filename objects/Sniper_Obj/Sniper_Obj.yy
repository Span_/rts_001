{
    "id": "87e30452-f9fe-4ffa-b37d-118b33dbe437",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Sniper_Obj",
    "eventList": [
        {
            "id": "6312567f-27fb-4b1a-a40f-b9df1a1e1c49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87e30452-f9fe-4ffa-b37d-118b33dbe437"
        },
        {
            "id": "f3f13bdb-020f-4108-98be-3b82f4c93186",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87e30452-f9fe-4ffa-b37d-118b33dbe437"
        },
        {
            "id": "f9617747-4c4d-46ff-90b0-f1c5865f3a86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "87e30452-f9fe-4ffa-b37d-118b33dbe437"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ac7c674d-ac57-41fc-8c83-20f25a6e99f0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "3b440f38-22e3-4c08-b281-6fdd14ce6d41",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "9c7ac291-4d9d-43e0-bc37-879ed0f6cb89",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "3faccabe-1833-44bb-89a2-cfcaa557ab66",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "cdebb653-997e-4ed7-93b7-8bb7bf29b5b5",
    "visible": true
}