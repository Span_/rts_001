{
    "id": "9615edc0-b2ba-4112-a630-1f02a8d33960",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_Farm_Button_Obj",
    "eventList": [
        {
            "id": "27f7cc6f-1ad3-4928-a08b-fcaf91172d61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9615edc0-b2ba-4112-a630-1f02a8d33960"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c86819c3-011c-4180-a1aa-97002a4758d1",
    "visible": true
}