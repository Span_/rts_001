{
    "id": "84cfa824-edd4-492d-9374-79b958c3509f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Terrain_Collide_Obj",
    "eventList": [
        
    ],
    "maskSpriteId": "594d9ceb-6318-49d0-8f29-d69fca17783a",
    "overriddenProperties": null,
    "parentObjectId": "bc29aca6-c515-42ee-aee4-5fbd85504e54",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}