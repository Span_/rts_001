/// @description init

target = -1;
destination = -1; //for attack moves
path = -1;
pathPos = 1;

aggroRange = 95;
attackRange = 95;
movementSpeed = 2;
damage = 2;

expVal = 15;

maxHealth = 50;
curHealth = maxHealth;

attackSpeed = 60;
cooldown = 0;

//retreatDuration = 20;
//retreatCooldown = 0;

//wanderSpeed = 1;
//wanderDuration = 3;

//state = "idle"

state = state_enum.idle;