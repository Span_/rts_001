{
    "id": "9848e87f-475a-4bb5-a780-2c0e135319bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Horse_Mage_Obj",
    "eventList": [
        {
            "id": "9e559ef6-bf7e-40a5-9107-73e9a4e036c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9848e87f-475a-4bb5-a780-2c0e135319bd"
        },
        {
            "id": "1081bc86-bb7a-4fbc-bb08-6e7573e1036c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9848e87f-475a-4bb5-a780-2c0e135319bd"
        },
        {
            "id": "7726c10b-dc91-4ae4-892a-08093bdf7cf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9848e87f-475a-4bb5-a780-2c0e135319bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "040b298e-86be-474b-91e2-2b0c55490f00",
    "visible": true
}