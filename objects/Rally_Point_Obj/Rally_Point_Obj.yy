{
    "id": "5ad8a077-7af0-4bba-acbd-8e93fbbd5f22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Rally_Point_Obj",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c6d86ddc-f5c3-4a5d-97ba-72e34ef64d1a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ee6ad00-b138-456a-8a7c-39b07b4db56f",
    "visible": true
}