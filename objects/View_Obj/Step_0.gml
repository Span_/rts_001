/// @description move / zoom


//if (keyboard_check(vk_right)) view_xport[0] -= ViewMoveSpeed;
//if (keyboard_check(vk_left)) view_xport[0] += ViewMoveSpeed;
//if (keyboard_check(vk_up)) view_yport[0] += ViewMoveSpeed;
//if (keyboard_check(vk_down)) view_yport[0] -= ViewMoveSpeed;

if (keyboard_check(vk_right)) {
	camera_set_view_pos(view_camera[0],
		camera_get_view_x(view_camera[0]) + ViewMoveSpeed,
		camera_get_view_y(view_camera[0]));
}
if (keyboard_check(vk_left)) {
	camera_set_view_pos(view_camera[0],
		camera_get_view_x(view_camera[0]) - ViewMoveSpeed,
		camera_get_view_y(view_camera[0]));
}
if (keyboard_check(vk_up)) {
	camera_set_view_pos(view_camera[0],
		camera_get_view_x(view_camera[0]),
		camera_get_view_y(view_camera[0]) - ViewMoveSpeed);
}
if (keyboard_check(vk_down)) {
	camera_set_view_pos(view_camera[0],
		camera_get_view_x(view_camera[0]),
		camera_get_view_y(view_camera[0]) + ViewMoveSpeed);
}

//mouse near edge moves camera;
//if (window_view_mouse_get_x(0) < 0.1 * window_get_width() ) {
//	camera_set_view_pos(view_camera[0],
//		camera_get_view_x(view_camera[0] - ViewMoveSpeed),
//		camera_get_view_y(view_camera[0]) );
//}
	



if (mouse_wheel_down()){
	//zoom out
	//view_set_wport(0, view_wport[0] + 32);
	//view_set_hport(0, view_hport[0] + 32);
	
	//max zoom
	if( (room_width > camera_get_view_width(view_camera[0]) + zoomRateW) 
		&& (room_height > camera_get_view_height(view_camera[0]) + zoomRateH) ) {
		
		camera_set_view_size(view_camera[0], 
			camera_get_view_width(view_camera[0]) + zoomRateW, 
			camera_get_view_height(view_camera[0]) + zoomRateH);
			
		//pan left and up a little
		camera_set_view_pos(view_camera[0],
			camera_get_view_x(view_camera[0]) - zoomRateW/2,
			camera_get_view_y(view_camera[0]) - zoomRateH/2);
			
	}
	
		
	//camera_set_view_border(view_camera[0], view_wport[0] + 32, view_hport[0] + 18);
} else if (mouse_wheel_up()) {
	//zoom in 
	//view_set_wport(0, view_wport[0] - 32);
	//view_set_hport(0, view_hport[0] - 32);
	
	//don't let the camera get too small
	if ( camera_get_view_width(view_camera[0]) > 256 ){
	
	camera_set_view_size(view_camera[0], 
		camera_get_view_width(view_camera[0]) - zoomRateW, 
		camera_get_view_height(view_camera[0]) - zoomRateH);
		
	//pan right and down a little
	camera_set_view_pos(view_camera[0],
		camera_get_view_x(view_camera[0]) + zoomRateW/2,
		camera_get_view_y(view_camera[0]) + zoomRateH/2);
	}
}


//keep camera within bounds of map
if( camera_get_view_x(view_camera[0]) < 0) {
	camera_set_view_pos(view_camera[0],
		0,
		camera_get_view_y(view_camera[0]));
}
if( camera_get_view_y(view_camera[0]) < 0) {
	camera_set_view_pos(view_camera[0],
		camera_get_view_x(view_camera[0]),
		0);
}
//check far side of screen
if( camera_get_view_x(view_camera[0]) > room_width - camera_get_view_width(view_camera[0])) {
	camera_set_view_pos(view_camera[0],
		room_width - camera_get_view_width(view_camera[0]),
		camera_get_view_y(view_camera[0]));
}
if( camera_get_view_y(view_camera[0]) > room_height - camera_get_view_height(view_camera[0])) {
	camera_set_view_pos(view_camera[0],
		camera_get_view_x(view_camera[0]),
		room_height - camera_get_view_height(view_camera[0]));
}