/// @description init

//TODO make h & w move speed, move slower when zoomed in more
ViewMoveSpeed = 10;

zoomRateW = camera_get_view_width(view_camera[0]) / 20;
zoomRateH = camera_get_view_height(view_camera[0]) / 20;

//move camera to middle of room
camera_set_view_pos(view_camera[0], 
	room_width/2 - camera_get_view_width(view_camera[0]) / 2, 
	room_height/2 - camera_get_view_height(view_camera[0]) / 2)

//for mouse near edge of screen
//mouseBoundary = 0.1; //percent of screen
//camera_set_view_speed(view_camera[0], ViewMoveSpeed, ViewMoveSpeed);
//camera_set_view_border(view_camera[0], mouseBoundary * camera_get_view_width(view_camera[0]),
//	mouseBoundary * camera_get_view_height(view_camera[0]));