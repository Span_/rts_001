{
    "id": "81469422-c5d3-4e00-9558-580d06625d15",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_Repair_Bot_Button_Obj",
    "eventList": [
        {
            "id": "e954d1d7-6e18-4f44-a40e-4fda7c5fa332",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "81469422-c5d3-4e00-9558-580d06625d15"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ad4af53-a9d5-4e1f-8ea3-2b0283198a34",
    "visible": true
}