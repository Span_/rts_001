{
    "id": "f010f993-a24f-4da6-8a70-486a5f7972d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Chaos_Obj",
    "eventList": [
        {
            "id": "8ab7bf86-bc0f-473a-8e98-fae4f846148f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f010f993-a24f-4da6-8a70-486a5f7972d3"
        },
        {
            "id": "7da6c1c7-2b37-4a5b-84fe-a102bca209fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f010f993-a24f-4da6-8a70-486a5f7972d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
    "visible": true
}