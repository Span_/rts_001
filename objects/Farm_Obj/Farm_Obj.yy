{
    "id": "e3c31d34-3173-41e4-b7ae-3653561330e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Farm_Obj",
    "eventList": [
        {
            "id": "400b40f2-0228-4ca5-9e54-c0adff201439",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e3c31d34-3173-41e4-b7ae-3653561330e6"
        },
        {
            "id": "17fb0da4-05ca-4076-bb1b-df51705ce200",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e3c31d34-3173-41e4-b7ae-3653561330e6"
        },
        {
            "id": "9b0afeea-f793-49ed-88ae-b11673a20b1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e3c31d34-3173-41e4-b7ae-3653561330e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "8812b3d5-fd14-4115-91a9-e0cbfa075dc5",
    "visible": true
}