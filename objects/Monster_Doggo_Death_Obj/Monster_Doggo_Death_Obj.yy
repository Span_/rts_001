{
    "id": "6b424ded-38cf-4527-9d94-bc1733e0b177",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Monster_Doggo_Death_Obj",
    "eventList": [
        {
            "id": "49919483-e159-4834-9b6d-b5c7ae5ee679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6b424ded-38cf-4527-9d94-bc1733e0b177"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
    "visible": true
}