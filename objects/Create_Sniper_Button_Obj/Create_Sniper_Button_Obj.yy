{
    "id": "c9531844-0cd6-48f4-8830-a6be1b1fa231",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_Sniper_Button_Obj",
    "eventList": [
        {
            "id": "4ea880be-4d8e-41b3-8eb6-3865ca63bf1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "c9531844-0cd6-48f4-8830-a6be1b1fa231"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "426c8039-e3e1-4549-855a-7d7e040bdd70",
    "visible": true
}