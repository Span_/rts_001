{
    "id": "92d1e379-872d-4f45-9dfc-2a5c3a83ae07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Worker_Obj",
    "eventList": [
        {
            "id": "c6626ace-0d4c-4817-8a58-8be1e791350e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92d1e379-872d-4f45-9dfc-2a5c3a83ae07"
        },
        {
            "id": "db64a42e-9a72-4818-8b51-828cc0ccaa64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "92d1e379-872d-4f45-9dfc-2a5c3a83ae07"
        },
        {
            "id": "7944be03-095d-4999-9699-46bf18d2b83a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "92d1e379-872d-4f45-9dfc-2a5c3a83ae07"
        },
        {
            "id": "2372ffbb-71c0-412c-b2a3-33b1f7b17f5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "92d1e379-872d-4f45-9dfc-2a5c3a83ae07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "2bcd712b-9436-4aea-a7e4-fbd62ed821fa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "0cc3c47c-9e09-4be0-8481-1b8074e79391",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "0287210f-1520-4c8d-b791-6fb89daad359",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "b2979e1c-a4cc-4cb0-9dcc-3b7c3375514f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
    "visible": true
}