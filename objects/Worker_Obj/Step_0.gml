///@description Set & Apply States

//Unit_Step();
///Unit_Step();
if (!counted) {
	//add self to unit score
	counted = true;
	if ( instance_exists(Score_Obj)){
		with (Score_Obj) units +=1;
	}
}


if (curHealth <= 0) instance_destroy();

if (cooldown > 0) cooldown -= 1;

switch (state){
	case state_enum.idle:
		Unit_Idle();
		minerals = -1;
		sprite_index = Worker1_Idle;
		
		
		//go after some nearby resources
		if (instance_exists(Mineral_Obj)){
			minerals = instance_nearest(x,y,Mineral_Obj);
			
			//mine if in attack range
			if (distance_to_object(minerals) < harvestRange) {
			
				state = state_enum.attack;
			
			//move to attack range if in aggrorange
			} else if (distance_to_object(minerals) < aggroRange) {
				
				target = instance_create_layer(minerals.x, minerals.y, global.Inst_Units_Layer, Target_Obj);
				state = state_enum.move;
		
		
			}
		}
		
		break;
	case state_enum.attack:
		
		//speed = 0;
		if (instance_exists(Mineral_Obj)){
			minerals = instance_nearest(x,y,Mineral_Obj);
			if (distance_to_object(minerals) < harvestRange) {
				sprite_index = Worker1_Attack;
				
				
				if (minerals.x < x) {
					image_xscale = -1;
				} else {
					image_xscale = 1;
				}
				
				//attack
				if (cooldown <= 0) {
					//attack
					if (minerals.currentResources > harvestAmount){
					
						minerals.currentResources -= harvestAmount;
						if(instance_exists(Score_Obj)) Score_Obj.resources += harvestAmount;
					} else {
						if(instance_exists(Score_Obj)) Score_Obj.resources += minerals.currentResources;
						minerals.currentResources = 0;
					}
					cooldown = harvestSpeed;
				}
			} else {
				state = state_enum.idle;
			}
		}else {
			state = state_enum.idle;
		}
		
		break;
	case state_enum.move:
		
		//sprite_index = Worker1_Move;
		Unit_Move(Worker1_Move);
		
		//if on the way to mine minerals, mine them when close enough
		if ( instance_exists(minerals) ) {
			if (distance_to_object(minerals) < harvestRange) {
				state = state_enum.attack;
			}
		}
		
		
		break;
	default:
		state = state_enum.idle;
		//Unit_Idle();
}
