{
    "id": "ade7b43c-11ef-47df-be0e-f710f97ef652",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_Entrenchment_Button_Obj",
    "eventList": [
        {
            "id": "09eb7b23-038f-40ff-b91c-182a16dafc74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "ade7b43c-11ef-47df-be0e-f710f97ef652"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "407f5f4a-04a0-401d-a450-7125a9559b50",
    "visible": true
}