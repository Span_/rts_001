{
    "id": "6241b425-d0ca-48ff-a667-fdbe6da7e7e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Bullet_Obj",
    "eventList": [
        {
            "id": "6e0a0ba0-e1dc-46a1-9778-e68f15b7725c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6241b425-d0ca-48ff-a667-fdbe6da7e7e2"
        },
        {
            "id": "282508a8-cbc5-467c-be03-bda8c8cb2031",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6241b425-d0ca-48ff-a667-fdbe6da7e7e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3ba4dd73-73b2-4352-98d7-1017cbf2a317",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "54e9e2e5-54d4-46a3-ac28-8cf57727be7c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "19dc325e-6797-4aa4-a581-30825b46baa8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "f1f2e6ba-3618-41f5-9a6b-27b8c98e8721",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "feb188bc-9b3a-42be-b4ee-27e473a45a97",
    "visible": true
}