/// @description score

//visible = true;

//overall data
units = 0;
resources = 500;
maxUnits = 10;

//unit specific
damage = 0;
curHealth = 0;
maxHealth = 0;
maxSpeed = 0;
kills = 0;

unitsPerHouse = 5;


level = 0;
curExp = 0;
maxExp = 100;