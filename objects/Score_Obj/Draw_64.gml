/// @description draw itself

//var cx = camera_get_view_x(view_camera[0]);
//var cy = camera_get_view_y(view_camera[0]);
//var cw = camera_get_view_width(view_camera[0]);
//var ch = camera_get_view_height(view_camera[0]);

//draw on viewport
var cx = view_get_xport(0);
var cy = view_get_yport(0);
var cw = view_get_wport(0);
var ch = view_get_hport(0);

draw_set_font(Score_Font);
draw_set_color(c_white);
draw_text(cx + cw * 0.1 , cy + ch * 0.1, "Units: " + string(units) + " / " + string(maxUnits));
draw_text(cx + cw * 0.1 , cy + ch * 0.1 + 16, "Resources: " + string(resources));

draw_self();