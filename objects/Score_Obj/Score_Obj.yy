{
    "id": "6ee53c20-eed0-4b28-b260-edbc54c1ed9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Score_Obj",
    "eventList": [
        {
            "id": "74562d79-40f3-43b8-9309-c6ca5d6cb2cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ee53c20-eed0-4b28-b260-edbc54c1ed9f"
        },
        {
            "id": "02e6850f-2c23-4131-ae10-0e1b105c08ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6ee53c20-eed0-4b28-b260-edbc54c1ed9f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2166f53-d291-4ee0-b960-1864d899886d",
    "visible": true
}