{
    "id": "6acaba16-7afb-4867-8ad7-a06e23923269",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Wall_Obj",
    "eventList": [
        {
            "id": "da46782b-4da4-4e97-8ea3-7a5339d21898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6acaba16-7afb-4867-8ad7-a06e23923269"
        },
        {
            "id": "ef502e3e-4480-4627-9ce7-26aa80ec350a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6acaba16-7afb-4867-8ad7-a06e23923269"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6d637535-1cb5-4ba8-af27-b6257d2dcd81",
    "visible": true
}