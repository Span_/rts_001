{
    "id": "b583391a-2075-4427-bd95-14144f4ef5b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Lighting_Obj",
    "eventList": [
        {
            "id": "6d4cbcc2-7cb3-41cf-89cc-2257eea2302b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b583391a-2075-4427-bd95-14144f4ef5b5"
        },
        {
            "id": "64982ec4-1a18-4fbd-aae3-6d3481f00ece",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "b583391a-2075-4427-bd95-14144f4ef5b5"
        },
        {
            "id": "7809f38b-66ec-4e6c-82ac-be591f024c1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "b583391a-2075-4427-bd95-14144f4ef5b5"
        },
        {
            "id": "d44d0d06-dc5e-42b4-ad93-917528f6d178",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "b583391a-2075-4427-bd95-14144f4ef5b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2166f53-d291-4ee0-b960-1864d899886d",
    "visible": true
}