
/// @description blackness



if (!surface_exists(light_surface)){
	light_surface = surface_create(room_width,room_height);//camera_get_view_width(view_camera[0]), camera_get_view_height(view_camera[0]));
	//layer_element_move(light_surface,global.Lighting_Layer);
}


surface_set_target(light_surface);
//gpu_set_blendmode(bm_normal);
draw_set_color(c_black);
draw_set_alpha(1);
//fog of war:
draw_rectangle(0,0,room_width,room_height,false);

//draw_self();

draw_set_alpha(1);
gpu_set_blendmode(bm_normal);

//draw_self();
surface_reset_target();

//draw_clear_alpha(c_black, 1);
//gpu_set_blendmode(bm_add);
//surface_reset_target();
//gpu_set_blendmode(bm_normal);
