{
    "id": "4357733c-c04f-4b8a-b9c1-b3ce74816dc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Chaos_Spawner_Obj",
    "eventList": [
        {
            "id": "95c95add-a7b1-4e81-ba90-a6634c15737d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4357733c-c04f-4b8a-b9c1-b3ce74816dc8"
        },
        {
            "id": "30352257-322c-409a-9c61-9cc4222cb59b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4357733c-c04f-4b8a-b9c1-b3ce74816dc8"
        },
        {
            "id": "c57f0599-1e14-4c52-9756-8c32ee5a0988",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4357733c-c04f-4b8a-b9c1-b3ce74816dc8"
        },
        {
            "id": "356820ad-7c0c-43b0-82bb-84c3585d5e30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4357733c-c04f-4b8a-b9c1-b3ce74816dc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c1c90edd-7d55-4e03-9a84-26a5b20b6c48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
    "visible": true
}