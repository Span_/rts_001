{
    "id": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Button_Obj",
    "eventList": [
        {
            "id": "e8b02346-adb2-4eab-b918-a39b01f65fd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        },
        {
            "id": "21647900-afc7-4a5c-87c9-08cb02231633",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        },
        {
            "id": "c44f3f48-e6dc-4052-9b9d-7d958c4839b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        },
        {
            "id": "fd40b697-9af4-4f70-b728-ad815f0310cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        },
        {
            "id": "325a332a-ffd1-4147-a46e-d5d448754fd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        },
        {
            "id": "10dcfeeb-4ff3-464d-85be-b6d3da7da545",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        },
        {
            "id": "3ce3ebbe-b655-470f-ac32-6f1818a6c5a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        },
        {
            "id": "efb0e761-02bb-4c67-8d08-2b492c52c017",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "02b95072-6521-492e-ae8c-fd8b04573ed1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}