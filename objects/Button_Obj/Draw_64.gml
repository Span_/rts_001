

if (hover) {
	draw_set_colour(c_white);
	draw_set_font(Tip_Font);
	draw_set_halign(fa_right);
	draw_set_valign(fa_bottom);
	draw_text(window_mouse_get_x(), window_mouse_get_y(), toolTip );
	
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
}