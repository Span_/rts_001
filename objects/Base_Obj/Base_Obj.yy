{
    "id": "b5835b40-448c-414c-bff4-91a9731fe02b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Base_Obj",
    "eventList": [
        {
            "id": "968a38af-b908-48de-a3fd-dfd3f8c3882a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5835b40-448c-414c-bff4-91a9731fe02b"
        },
        {
            "id": "8a23756f-caa6-42ea-918e-6625926bb47f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "b5835b40-448c-414c-bff4-91a9731fe02b"
        },
        {
            "id": "b4ccbf58-6eeb-4ca8-8acf-a1f0e6754fb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b5835b40-448c-414c-bff4-91a9731fe02b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "9c1c16fb-3785-4618-a246-d668b92fc6d4",
    "visible": true
}