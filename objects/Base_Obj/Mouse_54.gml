/// @description 

if (selected){
	
	if (target != -1 && instance_exists(target)) {
		with (target) instance_destroy();
	}

	target = instance_create_layer(mouse_x, mouse_y, "Inst_Units_Layer", Rally_Point_Obj);

}