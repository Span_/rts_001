{
    "id": "2630e677-8299-429e-93f9-518773eaabe7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Soldier_Death_Obj",
    "eventList": [
        {
            "id": "199f792a-0cb2-4c8d-8be3-5afee9d73798",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2630e677-8299-429e-93f9-518773eaabe7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
    "visible": true
}