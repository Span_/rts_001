{
    "id": "583d935b-539a-42c2-8871-15651aaccda7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Mouse_Obj",
    "eventList": [
        {
            "id": "af479050-d3d9-4e05-baa3-517ba014d2cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "583d935b-539a-42c2-8871-15651aaccda7"
        },
        {
            "id": "ff2b559c-b23f-4b23-a0ed-c8d714a7fa14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "583d935b-539a-42c2-8871-15651aaccda7"
        },
        {
            "id": "cf160915-bacf-4035-a78d-c24f12ab4a3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "583d935b-539a-42c2-8871-15651aaccda7"
        },
        {
            "id": "b23ba7d3-77f9-4a9c-87e2-968fb88d966e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "583d935b-539a-42c2-8871-15651aaccda7"
        },
        {
            "id": "90a52a9d-304c-4366-af98-7cad7edd622a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "583d935b-539a-42c2-8871-15651aaccda7"
        },
        {
            "id": "f359fae9-236c-46ea-8093-b3b0ae833c12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "583d935b-539a-42c2-8871-15651aaccda7"
        },
        {
            "id": "0f4ef82a-d0f8-4fcf-aea9-51f8aa5b6a37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "583d935b-539a-42c2-8871-15651aaccda7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d4c513e-d35f-46e8-bfc7-462c401321ce",
    "visible": true
}