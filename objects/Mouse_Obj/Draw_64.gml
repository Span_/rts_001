/// @description 
//from step
x = window_mouse_get_x(); //mouse_x;
y = window_mouse_get_y(); //mouse_y;

//x = mouse_x;
//y = mouse_y;

//if theres somthing to place
if (objectToPlace != -1) {
	
	//snap x,y position
	x = mouse_x - mouse_x % 16; //subtract tile size remainder 
	y = mouse_y - mouse_y % 16;
	
	//if it will collide with anything
	if (!place_free(x,y)) {
		
		image_blend = c_red;
		
	} else {
		image_blend = c_green;
	}
}