{
    "id": "ff158862-25f4-4903-b9d1-5167710e9f1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Worker_Death_Obj",
    "eventList": [
        {
            "id": "b0e12c20-16de-4791-988c-767eb828fc83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ff158862-25f4-4903-b9d1-5167710e9f1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
    "visible": true
}