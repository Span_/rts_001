{
    "id": "12dbb337-749a-4e4b-9964-8d1241ca8c44",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Back_Button_Obj",
    "eventList": [
        {
            "id": "f1acc79f-812e-479f-8b66-c3a457afb475",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "12dbb337-749a-4e4b-9964-8d1241ca8c44"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d411f1d-f5b7-4ba1-b39d-9dcd959505e9",
    "visible": true
}