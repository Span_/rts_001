/// @description 


//image_alpha = image_alpha * 2;

switch (GUI_Obj.curMenu){
	case menuArray.build_units:
		//destroy each
		for (var i = 0; i < array_height_2d(global.GUI_Array_Units); i++;) {
	   
		   instance_destroy(global.GUI_Array_Units[i,3]);
	  
		}
		//make new one , main menu
		//build units
		instance_create_layer(global.GUI_Array_Build_Menu[1,0] * camera_get_view_width(view_camera[0]) / global.wCamInit + camera_get_view_x(view_camera[0])
							,global.GUI_Array_Build_Menu[1,1] * camera_get_view_height(view_camera[0]) / global.hCamInit + camera_get_view_y(view_camera[0]),
								global.GUI_Array_Build_Menu[1,2], global.GUI_Array_Build_Menu[1,3]);
		//tooltip
		global.GUI_Array_Build_Menu[1,3].toolTip = global.GUI_Array_Build_Menu[1,4];
		//build buildings
		instance_create_layer(global.GUI_Array_Build_Menu[2,0]* camera_get_view_width(view_camera[0]) / global.wCamInit + camera_get_view_x(view_camera[0])
								,global.GUI_Array_Build_Menu[2,1]* camera_get_view_height(view_camera[0]) / global.hCamInit + camera_get_view_y(view_camera[0]),
								global.GUI_Array_Build_Menu[2,2], global.GUI_Array_Build_Menu[2,3]);
		//tooltip
		global.GUI_Array_Build_Menu[2,3].toolTip = global.GUI_Array_Build_Menu[2,4];
		
		//set init x
		global.GUI_Array_Build_Menu[1,3].xPosInit = global.GUI_Array_Build_Menu[1,0];
		//set init y
		global.GUI_Array_Build_Menu[1,3].yPosInit = global.GUI_Array_Build_Menu[1,1];
		//set init x
		global.GUI_Array_Build_Menu[2,3].xPosInit = global.GUI_Array_Build_Menu[2,0];
		//set init y
		global.GUI_Array_Build_Menu[2,3].yPosInit = global.GUI_Array_Build_Menu[2,1];
		
		GUI_Obj.curMenu = menuArray.main_menu;
		//destroy self
		instance_destroy();
		break;
	case menuArray.build_buildings:
		//destroy each
		for (var i = 0; i < array_height_2d(global.GUI_Array_Buildings); i++;) {
	   
		   instance_destroy(global.GUI_Array_Buildings[i,3]);
	  
		}
		//make new one , main menu
		//build units
		instance_create_layer(global.GUI_Array_Build_Menu[1,0] * camera_get_view_width(view_camera[0]) / global.wCamInit + camera_get_view_x(view_camera[0]),
								global.GUI_Array_Build_Menu[1,1] * camera_get_view_height(view_camera[0]) / global.hCamInit + camera_get_view_y(view_camera[0]),
								global.GUI_Array_Build_Menu[1,2], global.GUI_Array_Build_Menu[1,3]);
		global.GUI_Array_Build_Menu[1,3].toolTip = global.GUI_Array_Build_Menu[1,4];
		//build buildings
		instance_create_layer(global.GUI_Array_Build_Menu[2,0] * camera_get_view_width(view_camera[0]) / global.wCamInit + camera_get_view_x(view_camera[0]),
								global.GUI_Array_Build_Menu[2,1] * camera_get_view_height(view_camera[0]) / global.hCamInit + camera_get_view_y(view_camera[0]),
								global.GUI_Array_Build_Menu[2,2], global.GUI_Array_Build_Menu[2,3]);
		global.GUI_Array_Build_Menu[2,3].toolTip = global.GUI_Array_Build_Menu[2,4];
		
		//set init x
		global.GUI_Array_Build_Menu[1,3].xPosInit = global.GUI_Array_Build_Menu[1,0];
		//set init y
		global.GUI_Array_Build_Menu[1,3].yPosInit = global.GUI_Array_Build_Menu[1,1];
		//set init x
		global.GUI_Array_Build_Menu[2,3].xPosInit = global.GUI_Array_Build_Menu[2,0];
		//set init y
		global.GUI_Array_Build_Menu[2,3].yPosInit = global.GUI_Array_Build_Menu[2,1];
		
		GUI_Obj.curMenu = menuArray.main_menu;
		//destroy self
		instance_destroy();
		break;
	
}
