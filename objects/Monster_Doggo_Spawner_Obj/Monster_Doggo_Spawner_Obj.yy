{
    "id": "b2a9f704-d1bb-48c6-974c-fd9bdf7ed642",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Monster_Doggo_Spawner_Obj",
    "eventList": [
        {
            "id": "0ba91e04-e37a-49b1-af35-963385298d56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2a9f704-d1bb-48c6-974c-fd9bdf7ed642"
        },
        {
            "id": "46431671-ea3d-4040-b502-192e4b2b2e47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b2a9f704-d1bb-48c6-974c-fd9bdf7ed642"
        },
        {
            "id": "cc48edcb-cd6e-4c6d-8319-c6562d920a32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2a9f704-d1bb-48c6-974c-fd9bdf7ed642"
        },
        {
            "id": "2f2c6647-7fcc-46ed-875f-9488e9e36303",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b2a9f704-d1bb-48c6-974c-fd9bdf7ed642"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c1c90edd-7d55-4e03-9a84-26a5b20b6c48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "52028736-4df1-4c30-871c-5d5f4a3198a7",
    "visible": true
}