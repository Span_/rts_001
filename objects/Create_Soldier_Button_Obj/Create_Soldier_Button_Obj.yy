{
    "id": "32f4dce5-677c-4c18-9df5-0d14913e79ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_Soldier_Button_Obj",
    "eventList": [
        {
            "id": "d5259e71-c220-49d4-b08e-c5bfcd68b414",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "32f4dce5-677c-4c18-9df5-0d14913e79ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "69071c74-0102-4e68-b862-c08a9b53647b",
    "visible": true
}