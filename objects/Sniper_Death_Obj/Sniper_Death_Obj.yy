{
    "id": "543ec9ac-9400-463e-80a8-8d5ab72dd6d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Sniper_Death_Obj",
    "eventList": [
        {
            "id": "873dfd6c-015a-46fa-b7c6-165145f3488b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "543ec9ac-9400-463e-80a8-8d5ab72dd6d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
    "visible": true
}