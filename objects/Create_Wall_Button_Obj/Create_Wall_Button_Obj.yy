{
    "id": "1962b6c0-d95f-4821-a198-8c97bcdc4828",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Create_Wall_Button_Obj",
    "eventList": [
        {
            "id": "84cfa642-47b5-486d-b21d-69e8928e2364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "1962b6c0-d95f-4821-a198-8c97bcdc4828"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02b95072-6521-492e-ae8c-fd8b04573ed1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0d4342a-82ea-451f-9eb7-97a6be193c77",
    "visible": true
}