{
    "id": "e5ad829e-b1fd-4861-8543-e146e33e4daf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Building_Obj",
    "eventList": [
        {
            "id": "369909e2-db32-4a96-965a-7a60e80a6f88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e5ad829e-b1fd-4861-8543-e146e33e4daf"
        },
        {
            "id": "8d1c7456-b0a4-4a1a-bd71-71ac02b6548f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "be8bd7c7-ff87-41ff-83f2-714cf9368ee2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e5ad829e-b1fd-4861-8543-e146e33e4daf"
        },
        {
            "id": "6b86eb0e-15d8-4bd9-aefb-080ea7932c2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "e5ad829e-b1fd-4861-8543-e146e33e4daf"
        },
        {
            "id": "f54b68a6-dfb6-40e0-8202-961e0fc71ca5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e5ad829e-b1fd-4861-8543-e146e33e4daf"
        },
        {
            "id": "b638a275-d7f2-4cfb-8caf-00a475ed781e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e5ad829e-b1fd-4861-8543-e146e33e4daf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bc29aca6-c515-42ee-aee4-5fbd85504e54",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}