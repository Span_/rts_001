/// @description  

if (selected) {
	draw_set_alpha(0.25);
	draw_circle_color(x, y, sprite_width/2, c_lime, c_lime, true);
	draw_set_alpha(0.1);
	draw_circle_color(x, y, sprite_width/2, c_lime, c_lime, false);
	draw_set_alpha(1);
}

surface_set_target(Lighting_Obj.light_surface);
gpu_set_blendmode(bm_subtract);
draw_set_alpha(1);

draw_set_color(c_white);
if (aggroRange != -1) {
	draw_circle(x,y,aggroRange,false);
	draw_set_alpha(0.2);
	draw_circle(x,y,aggroRange * 1.2,false);
	//draw_set_color(c_blue);
	//draw_set_alpha(1);
	//draw_circle(x, y, aggroRange, true);
	
} else {	
	draw_circle(x,y,sprite_width * 1.5,false);
	draw_set_alpha(0.2);
	draw_circle(x,y,sprite_width * 1.8,false);
}
gpu_set_blendmode(bm_normal);
draw_set_alpha(1);
surface_reset_target();

hp = curHealth / maxHealth * 100;
draw_healthbar(x-sprite_width/4,y+sprite_height/2 + 1,x+sprite_width/4,y+2 + sprite_height/2,hp,c_black,c_red,c_lime,0,true,false);

draw_self();