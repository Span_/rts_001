///@description Set & Apply States

//Unit_Step();
///Unit_Step();
if (!counted) {
	//add self to unit score
	counted = true;
	if ( instance_exists(Score_Obj)){
		with (Score_Obj) units +=1;
	}
}


if (curHealth <= 0) instance_destroy();

if (cooldown > 0) cooldown -= 1;

switch (state){
	case state_enum.idle:
		Unit_Idle();
		bldg = -1;
		//if (instance_exists(target)) instance_destroy(target);
		target = -1;
		sprite_index = Repair_Bot_Idle;
		
		for (var i = 0; i < ds_list_size(global.all_buildings_list); i ++){
			bldg = global.all_buildings_list[|i];
			if (distance_to_object(bldg) <= repairRange && bldg.curHealth < bldg.maxHealth) {
				
				state = state_enum.attack;
				break;
			} else if (distance_to_object(bldg) < aggroRange && bldg.curHealth < bldg.maxHealth){
				//target = bldg;
				target = instance_create_layer(bldg.x,bldg.y,global.Inst_Buildings_Layer, Target_Obj);
				state = state_enum.move;
				break;
			}
		}
		
		//go after some nearby resources
		//if (instance_exists(Building_Obj)){
			
		//	for ( var i = 0; i < instance_number(Building_Obj); i ++) {
		//		//check each building if it is damagaded and within range
		//		if (distance_to_object(Building_Obj[|i]) < aggroRange 
		//			&& Building_Obj[|i].curHealth < Building_Obj[|i].maxHealth){
					
		//			//bldg = Building_Obj[i];
		//			if (distance_to_object(Building_Obj[|i]) < repairRange) {
		//				state = state_enum.attack;
		//			} else {
		//				target = instance_create_layer(Building_Obj[|i].x, Building_Obj[|i].y, global.Inst_Units_Layer, Target_Obj);
		//				state = state_enum.move;
		//			}
					
		//			break;
					
		//		}
		//	}
			
		//}
		
		break;
	case state_enum.attack:
		
		//speed = 0;
		if (instance_exists(Building_Obj)){
			bldg = instance_nearest(x,y,Building_Obj);
			if (distance_to_object(bldg) <= repairRange && bldg.curHealth < bldg.maxHealth) {
				sprite_index = Repair_Bot_Attack;
				
				
				if (bldg.x < x) {
					image_xscale = -1;
				} else {
					image_xscale = 1;
				}
				
				//attack
				if (cooldown <= 0) {
					//attack
					//repair the building, consume resources to do so
					bldg.curHealth += repairAmount;
					
					
					cooldown = repairSpeed;
				}
				
				if (bldg.curHealth >= bldg.maxHealth) {
					bldg.curHealth = bldg.maxHealth;
					state = state_enum.idle;
					
				}
				
			} else {
				state = state_enum.idle;
			}
		}else {
			state = state_enum.idle;
		}
		
		break;
	case state_enum.move:
		
		//sprite_index = Repair_Bot_Move;
		Unit_Move(Repair_Bot_Move);
		
		//if on the way to mine bldg, mine them when close enough
		if ( instance_exists(target) ) {
			if (distance_to_object(bldg) <= repairRange){
				state = state_enum.attack;
			} 
			//else if (distance_to_object(target) < sprite_width) {
			//	state = state_enum.idle;
				
			//}
		}
		
		
		break;
	default:
		state = state_enum.idle;
		//Unit_Idle();
}
