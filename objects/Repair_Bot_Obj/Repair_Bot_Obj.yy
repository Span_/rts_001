{
    "id": "2c057783-6ff2-474a-953d-fb3f85c81f91",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Repair_Bot_Obj",
    "eventList": [
        {
            "id": "b7304658-470c-4321-9501-ee475956972c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c057783-6ff2-474a-953d-fb3f85c81f91"
        },
        {
            "id": "a2cb5620-c164-4f3e-90ab-148098e3c69b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2c057783-6ff2-474a-953d-fb3f85c81f91"
        },
        {
            "id": "1252ecb1-c15e-46c9-a810-3373f83b52bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "2c057783-6ff2-474a-953d-fb3f85c81f91"
        },
        {
            "id": "3612c20a-395e-4d01-803f-4432874a5b00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2c057783-6ff2-474a-953d-fb3f85c81f91"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6da9a4f6-e72f-433e-a82c-6b5d905a7f8c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "90e55ff6-0a3a-46ca-9da1-32933ea5ad3d",
    "visible": true
}