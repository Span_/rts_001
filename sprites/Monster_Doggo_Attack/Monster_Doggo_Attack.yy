{
    "id": "7144de0c-7012-46cf-a4c6-ebce0a8f9763",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Monster_Doggo_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 4,
    "bbox_right": 10,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d7277d1-9510-4f2b-bf3c-2442eee1616b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7144de0c-7012-46cf-a4c6-ebce0a8f9763",
            "compositeImage": {
                "id": "b3130f1c-bcf5-41cf-8cf4-f7b4afa1a773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d7277d1-9510-4f2b-bf3c-2442eee1616b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88399be7-6e75-48e0-b459-c225edb9b501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d7277d1-9510-4f2b-bf3c-2442eee1616b",
                    "LayerId": "155f9e8f-dc9d-468a-96c3-688a6b51f23a"
                }
            ]
        },
        {
            "id": "2f6e5ce3-673f-4b64-9996-86111e59230a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7144de0c-7012-46cf-a4c6-ebce0a8f9763",
            "compositeImage": {
                "id": "a67ae47d-5973-437f-ba29-14fa25f059ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f6e5ce3-673f-4b64-9996-86111e59230a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "217bde61-b848-430a-b83d-5f08dd143f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f6e5ce3-673f-4b64-9996-86111e59230a",
                    "LayerId": "155f9e8f-dc9d-468a-96c3-688a6b51f23a"
                }
            ]
        },
        {
            "id": "8e050035-6ab4-45b6-ad44-b1b2565b95f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7144de0c-7012-46cf-a4c6-ebce0a8f9763",
            "compositeImage": {
                "id": "d02e666d-39d3-491e-bc83-6bbb71517e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e050035-6ab4-45b6-ad44-b1b2565b95f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07d14d35-8951-4be8-bc29-0d831cebd573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e050035-6ab4-45b6-ad44-b1b2565b95f5",
                    "LayerId": "155f9e8f-dc9d-468a-96c3-688a6b51f23a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "155f9e8f-dc9d-468a-96c3-688a6b51f23a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7144de0c-7012-46cf-a4c6-ebce0a8f9763",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 9
}