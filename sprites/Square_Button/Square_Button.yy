{
    "id": "bc66f81c-f4ac-494d-a82a-fa803972d6f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Square_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 9,
    "bbox_right": 56,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16c41aad-8f04-4e9b-bec4-43a76b1520b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc66f81c-f4ac-494d-a82a-fa803972d6f6",
            "compositeImage": {
                "id": "c2c622c7-3745-4747-ae94-b05a5817a7d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16c41aad-8f04-4e9b-bec4-43a76b1520b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2e590f1-3adf-4ba7-9f37-e1d15a40023b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c41aad-8f04-4e9b-bec4-43a76b1520b4",
                    "LayerId": "6f815305-dfe3-4cac-9808-434d259bb130"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6f815305-dfe3-4cac-9808-434d259bb130",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc66f81c-f4ac-494d-a82a-fa803972d6f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}