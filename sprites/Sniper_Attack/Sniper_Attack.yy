{
    "id": "62858809-c2ed-4b78-92e2-88c4bed6e916",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sniper_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 14,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67d6dc48-c698-4dc4-a3b1-c4f5e860ab4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "compositeImage": {
                "id": "ed4fe1e0-cdac-4194-b83d-9a8e3c661bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67d6dc48-c698-4dc4-a3b1-c4f5e860ab4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2f4464-c24a-4138-b257-5ad98b236d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d6dc48-c698-4dc4-a3b1-c4f5e860ab4f",
                    "LayerId": "654eb15a-87ee-42a2-bacb-9105017f46dc"
                }
            ]
        },
        {
            "id": "db413857-b53f-478d-9ff6-6440d8d953e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "compositeImage": {
                "id": "7f012ee7-0bcf-419d-88ca-6dbfbf83dad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db413857-b53f-478d-9ff6-6440d8d953e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45c484b-6ae4-4a7b-a433-a8b68139b1a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db413857-b53f-478d-9ff6-6440d8d953e0",
                    "LayerId": "654eb15a-87ee-42a2-bacb-9105017f46dc"
                }
            ]
        },
        {
            "id": "4eb9330d-23cd-442f-b875-5a88615a94fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "compositeImage": {
                "id": "abf3dbea-d01b-4369-a5c1-6813c28c6f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb9330d-23cd-442f-b875-5a88615a94fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da339395-3500-4c57-afa8-1a0f3e5d7d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb9330d-23cd-442f-b875-5a88615a94fe",
                    "LayerId": "654eb15a-87ee-42a2-bacb-9105017f46dc"
                }
            ]
        },
        {
            "id": "4727016c-6659-4367-be80-f488c796a7ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "compositeImage": {
                "id": "ab857116-3e35-407d-ab04-5c12907684ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4727016c-6659-4367-be80-f488c796a7ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fc9819c-c503-44c5-979b-d7a7dacefc4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4727016c-6659-4367-be80-f488c796a7ba",
                    "LayerId": "654eb15a-87ee-42a2-bacb-9105017f46dc"
                }
            ]
        },
        {
            "id": "d663c2da-5ba6-4eda-a396-105b899bda49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "compositeImage": {
                "id": "5bcada5b-035b-4892-96e6-f4db3ad48a2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d663c2da-5ba6-4eda-a396-105b899bda49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb74993-54c1-478c-a288-147f7fa918b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d663c2da-5ba6-4eda-a396-105b899bda49",
                    "LayerId": "654eb15a-87ee-42a2-bacb-9105017f46dc"
                }
            ]
        },
        {
            "id": "1f3b3cf2-05ef-4186-bcf0-25a7ad9e983e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "compositeImage": {
                "id": "e48bfc44-a8b1-440c-81e5-99fcdfb1188f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3b3cf2-05ef-4186-bcf0-25a7ad9e983e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a3723b-c52e-4529-ad89-de686f0bfdaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3b3cf2-05ef-4186-bcf0-25a7ad9e983e",
                    "LayerId": "654eb15a-87ee-42a2-bacb-9105017f46dc"
                }
            ]
        },
        {
            "id": "2bbecd98-865d-478d-9457-12bb76514470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "compositeImage": {
                "id": "74bad563-73d6-40b3-a0bd-225982781bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bbecd98-865d-478d-9457-12bb76514470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91d4cb7a-b465-4d02-b2ec-2d2b4d3a4a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bbecd98-865d-478d-9457-12bb76514470",
                    "LayerId": "654eb15a-87ee-42a2-bacb-9105017f46dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "654eb15a-87ee-42a2-bacb-9105017f46dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62858809-c2ed-4b78-92e2-88c4bed6e916",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 8
}