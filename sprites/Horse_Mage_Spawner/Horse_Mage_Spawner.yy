{
    "id": "7f8b5152-c59d-4ab3-ba5a-f449b2c6aa89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Horse_Mage_Spawner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 22,
    "bbox_right": 42,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6eb45fc8-3eeb-425d-a48a-b0eb36e84fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f8b5152-c59d-4ab3-ba5a-f449b2c6aa89",
            "compositeImage": {
                "id": "ae44d0d3-812e-4dea-830f-31f3a8aced9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb45fc8-3eeb-425d-a48a-b0eb36e84fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed9c1ffe-9e4e-4886-b42e-510b0638c9ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb45fc8-3eeb-425d-a48a-b0eb36e84fca",
                    "LayerId": "66f56adf-8241-4e81-97d7-f48b051d70f6"
                }
            ]
        },
        {
            "id": "2095ab96-cec8-4149-b11c-1ef3c88b926a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f8b5152-c59d-4ab3-ba5a-f449b2c6aa89",
            "compositeImage": {
                "id": "68b9aec1-0959-404b-bf58-b22e187c13ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2095ab96-cec8-4149-b11c-1ef3c88b926a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbcbc60e-88af-4054-b03f-7892cb275627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2095ab96-cec8-4149-b11c-1ef3c88b926a",
                    "LayerId": "66f56adf-8241-4e81-97d7-f48b051d70f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "66f56adf-8241-4e81-97d7-f48b051d70f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f8b5152-c59d-4ab3-ba5a-f449b2c6aa89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}