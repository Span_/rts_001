{
    "id": "407f5f4a-04a0-401d-a450-7125a9559b50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_Entrenchment_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1500be75-86f0-4044-889b-23bb5fc90f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407f5f4a-04a0-401d-a450-7125a9559b50",
            "compositeImage": {
                "id": "f0ecbda4-bead-4ae1-8655-34fc0378a26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1500be75-86f0-4044-889b-23bb5fc90f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "380d4cbb-dd6f-4bdb-b7a8-590c521ba5e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1500be75-86f0-4044-889b-23bb5fc90f53",
                    "LayerId": "7ae8c05c-1a23-4ad0-bf5e-0a9769baf29f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7ae8c05c-1a23-4ad0-bf5e-0a9769baf29f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "407f5f4a-04a0-401d-a450-7125a9559b50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}