{
    "id": "6d637535-1cb5-4ba8-af27-b6257d2dcd81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Wall_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93a677e7-c786-48a4-98c9-94aab02a1110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d637535-1cb5-4ba8-af27-b6257d2dcd81",
            "compositeImage": {
                "id": "0c7d0106-365c-40ca-80eb-466b731414ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a677e7-c786-48a4-98c9-94aab02a1110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21bacf4e-a252-4a61-9b9a-201261806c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a677e7-c786-48a4-98c9-94aab02a1110",
                    "LayerId": "ea3ad2f8-9387-421d-88bb-a630838bf385"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ea3ad2f8-9387-421d-88bb-a630838bf385",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d637535-1cb5-4ba8-af27-b6257d2dcd81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}