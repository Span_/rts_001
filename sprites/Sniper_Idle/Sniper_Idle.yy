{
    "id": "cdebb653-997e-4ed7-93b7-8bb7bf29b5b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sniper_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 14,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c49b5a8b-e16f-487c-96f0-4ad53466d486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdebb653-997e-4ed7-93b7-8bb7bf29b5b5",
            "compositeImage": {
                "id": "98824db6-e8fe-4869-8e10-3d49fb3823db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49b5a8b-e16f-487c-96f0-4ad53466d486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cdc7b55-44cf-428d-9c59-7767ac46683f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49b5a8b-e16f-487c-96f0-4ad53466d486",
                    "LayerId": "4e2799a3-2812-4912-ae49-34331ae5cab4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4e2799a3-2812-4912-ae49-34331ae5cab4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdebb653-997e-4ed7-93b7-8bb7bf29b5b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 17,
    "yorig": 8
}