{
    "id": "a921393c-6316-457c-aa9a-2a9d5597a087",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Monster_Doggo_Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37c84745-0587-4921-a436-b965da552660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "compositeImage": {
                "id": "f88518a6-db8a-4ff7-84ca-23ecace97c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c84745-0587-4921-a436-b965da552660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8876abb8-d862-452e-9684-0a692f1d930d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c84745-0587-4921-a436-b965da552660",
                    "LayerId": "b3158935-0a9a-4be1-a6a9-ef671c9325bd"
                }
            ]
        },
        {
            "id": "87e039a1-3c1e-4509-bbcc-b680e4fdd614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "compositeImage": {
                "id": "b89276a0-e7c4-4b51-91ba-0dde6b5f3f49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87e039a1-3c1e-4509-bbcc-b680e4fdd614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e91c8814-832f-4371-8dbd-b978c9b16dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87e039a1-3c1e-4509-bbcc-b680e4fdd614",
                    "LayerId": "b3158935-0a9a-4be1-a6a9-ef671c9325bd"
                }
            ]
        },
        {
            "id": "054c3cb3-05d6-4668-8ec8-b1a60aa843f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "compositeImage": {
                "id": "46a50f19-25a2-43c8-a5a8-d1dfe6dfa3d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "054c3cb3-05d6-4668-8ec8-b1a60aa843f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc2a37f0-7165-4dce-913d-b4ea7f9362f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "054c3cb3-05d6-4668-8ec8-b1a60aa843f2",
                    "LayerId": "b3158935-0a9a-4be1-a6a9-ef671c9325bd"
                }
            ]
        },
        {
            "id": "b6e0229b-739c-4027-af32-4c27e1a6d5bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "compositeImage": {
                "id": "c78d0924-05a9-4d9f-a1d1-5d267dcd99c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e0229b-739c-4027-af32-4c27e1a6d5bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2be66ac-4bc7-492b-8fcd-228a75bd9140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e0229b-739c-4027-af32-4c27e1a6d5bc",
                    "LayerId": "b3158935-0a9a-4be1-a6a9-ef671c9325bd"
                }
            ]
        },
        {
            "id": "d1588af1-9324-4b0a-8ea6-4c6e76f93056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "compositeImage": {
                "id": "0966d510-44fd-4d3a-a56a-9ae5e5595679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1588af1-9324-4b0a-8ea6-4c6e76f93056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82bea2da-3a86-4cdc-a3e8-bed0c73b1456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1588af1-9324-4b0a-8ea6-4c6e76f93056",
                    "LayerId": "b3158935-0a9a-4be1-a6a9-ef671c9325bd"
                }
            ]
        },
        {
            "id": "b718254d-cb03-4b52-a120-708eb2a12568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "compositeImage": {
                "id": "1d8ab7ce-eb02-4b52-96cc-462531e2288a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b718254d-cb03-4b52-a120-708eb2a12568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7911c36c-c522-4d40-8be9-f41f95f8a4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b718254d-cb03-4b52-a120-708eb2a12568",
                    "LayerId": "b3158935-0a9a-4be1-a6a9-ef671c9325bd"
                }
            ]
        },
        {
            "id": "b12d2107-9865-43ea-abfd-fdb43982dc7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "compositeImage": {
                "id": "14536fc6-ee0a-4147-8034-8831b5b7f5bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b12d2107-9865-43ea-abfd-fdb43982dc7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad59e72-2860-4ce8-b791-9ff38538e0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b12d2107-9865-43ea-abfd-fdb43982dc7d",
                    "LayerId": "b3158935-0a9a-4be1-a6a9-ef671c9325bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b3158935-0a9a-4be1-a6a9-ef671c9325bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a921393c-6316-457c-aa9a-2a9d5597a087",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}