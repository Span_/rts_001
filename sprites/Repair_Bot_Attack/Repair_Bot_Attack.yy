{
    "id": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Repair_Bot_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7e127a6-e4a6-414b-9483-230a59ba78e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
            "compositeImage": {
                "id": "a871994f-2ff1-40c3-b297-afab4411f6ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e127a6-e4a6-414b-9483-230a59ba78e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2375e665-9c9d-498d-8f6c-9ef5158bdcc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e127a6-e4a6-414b-9483-230a59ba78e5",
                    "LayerId": "d6954c77-a8ea-4f4b-8050-af28b1758484"
                }
            ]
        },
        {
            "id": "9bf85154-0cd8-4de9-b969-c3668f151b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
            "compositeImage": {
                "id": "66daef83-a54d-47a0-9141-ec0df6f9558e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf85154-0cd8-4de9-b969-c3668f151b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3695f442-dd73-4cc7-87f8-80545240403d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf85154-0cd8-4de9-b969-c3668f151b31",
                    "LayerId": "d6954c77-a8ea-4f4b-8050-af28b1758484"
                }
            ]
        },
        {
            "id": "248db04d-7b8e-41ae-87e0-71fc551d05a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
            "compositeImage": {
                "id": "b56751bf-57c4-48de-a579-72a2484c6e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "248db04d-7b8e-41ae-87e0-71fc551d05a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ca011c4-29e3-4dbd-a173-ebf5511d3f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "248db04d-7b8e-41ae-87e0-71fc551d05a5",
                    "LayerId": "d6954c77-a8ea-4f4b-8050-af28b1758484"
                }
            ]
        },
        {
            "id": "127c2ac9-6a1f-4e44-985d-68e8328f2a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
            "compositeImage": {
                "id": "b5e51e26-2d82-434d-a1f5-82343ec02873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "127c2ac9-6a1f-4e44-985d-68e8328f2a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98eb71c-12d1-487e-8e2c-cfbb86a95c62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "127c2ac9-6a1f-4e44-985d-68e8328f2a1c",
                    "LayerId": "d6954c77-a8ea-4f4b-8050-af28b1758484"
                }
            ]
        },
        {
            "id": "34107b42-62c7-4957-8e7c-166c30ae3358",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
            "compositeImage": {
                "id": "23ef4996-6f29-4088-b973-78297ddd57e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34107b42-62c7-4957-8e7c-166c30ae3358",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01cd96b-95ef-451f-905d-5d819ac267a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34107b42-62c7-4957-8e7c-166c30ae3358",
                    "LayerId": "d6954c77-a8ea-4f4b-8050-af28b1758484"
                }
            ]
        },
        {
            "id": "1a8e68f4-0213-4f24-9f16-7c6545f43c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
            "compositeImage": {
                "id": "1d700f1c-2435-451d-8ce7-ddddc5b180f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a8e68f4-0213-4f24-9f16-7c6545f43c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d6618f9-3a1a-46cc-b1bb-f9971fc22b66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a8e68f4-0213-4f24-9f16-7c6545f43c70",
                    "LayerId": "d6954c77-a8ea-4f4b-8050-af28b1758484"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d6954c77-a8ea-4f4b-8050-af28b1758484",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b31d5942-689b-452a-9aa7-e1b4d1439cd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}