{
    "id": "c15ab208-5b7c-4717-9915-ff0c9e3ec078",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "House",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a601f08-2507-42cc-a49d-a6ae6d77ee27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c15ab208-5b7c-4717-9915-ff0c9e3ec078",
            "compositeImage": {
                "id": "99b48967-7332-4524-a4be-fc2509950b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a601f08-2507-42cc-a49d-a6ae6d77ee27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fd8e1ac-b8ef-401b-8a8c-167a2e107966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a601f08-2507-42cc-a49d-a6ae6d77ee27",
                    "LayerId": "dc1b6536-2038-4c1b-9c6c-4c929a48b7cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dc1b6536-2038-4c1b-9c6c-4c929a48b7cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c15ab208-5b7c-4717-9915-ff0c9e3ec078",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}