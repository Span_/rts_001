{
    "id": "52df2ff8-6866-4940-b226-ec95eef4387c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Soldier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fa20657-baf7-4a88-a8a8-fbf294785ed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52df2ff8-6866-4940-b226-ec95eef4387c",
            "compositeImage": {
                "id": "829361c3-190d-4a20-9f89-1990174be5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa20657-baf7-4a88-a8a8-fbf294785ed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f48df9d-174a-4866-ac3e-b0beb258a35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa20657-baf7-4a88-a8a8-fbf294785ed5",
                    "LayerId": "7bdd2492-ed94-4138-af1f-176d3bc08c85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7bdd2492-ed94-4138-af1f-176d3bc08c85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52df2ff8-6866-4940-b226-ec95eef4387c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 6
}