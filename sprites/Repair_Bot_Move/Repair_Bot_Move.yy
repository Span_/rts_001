{
    "id": "0b19ac0d-6474-4597-8c3b-2d4df98a1abc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Repair_Bot_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "388951a9-fcf3-4861-ad3a-9f1e050cd23c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b19ac0d-6474-4597-8c3b-2d4df98a1abc",
            "compositeImage": {
                "id": "1c5b9f0f-1beb-45a2-a46d-9c59248975b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388951a9-fcf3-4861-ad3a-9f1e050cd23c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5add720-ed8c-484d-a12c-803e144c336c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388951a9-fcf3-4861-ad3a-9f1e050cd23c",
                    "LayerId": "c605ba96-d6ee-4d17-a6d5-6ba39b73d4ae"
                }
            ]
        },
        {
            "id": "9a229cbb-4900-4729-82f3-917d60a76cfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b19ac0d-6474-4597-8c3b-2d4df98a1abc",
            "compositeImage": {
                "id": "9e0ef817-696d-4c8c-8424-999abf0cdd50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a229cbb-4900-4729-82f3-917d60a76cfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bdb8a0c-a867-47bc-beab-1a2eb8e40d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a229cbb-4900-4729-82f3-917d60a76cfc",
                    "LayerId": "c605ba96-d6ee-4d17-a6d5-6ba39b73d4ae"
                }
            ]
        },
        {
            "id": "e05e9956-32da-465e-9afe-bb49262ffd2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b19ac0d-6474-4597-8c3b-2d4df98a1abc",
            "compositeImage": {
                "id": "a228b8c6-f141-444e-a5c5-315219f76d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05e9956-32da-465e-9afe-bb49262ffd2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84583492-f76b-4710-b3f0-3deb5fd943aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05e9956-32da-465e-9afe-bb49262ffd2d",
                    "LayerId": "c605ba96-d6ee-4d17-a6d5-6ba39b73d4ae"
                }
            ]
        },
        {
            "id": "7d57d0bd-d4b9-485f-8a0d-55c9451b80bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b19ac0d-6474-4597-8c3b-2d4df98a1abc",
            "compositeImage": {
                "id": "0d6c93fe-3c91-48b2-b75d-d4f872c5b4bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d57d0bd-d4b9-485f-8a0d-55c9451b80bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66d6bda-c9cc-4bd0-918b-7a2f88ef7ed9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d57d0bd-d4b9-485f-8a0d-55c9451b80bf",
                    "LayerId": "c605ba96-d6ee-4d17-a6d5-6ba39b73d4ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c605ba96-d6ee-4d17-a6d5-6ba39b73d4ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b19ac0d-6474-4597-8c3b-2d4df98a1abc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}