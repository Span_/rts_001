{
    "id": "594d9ceb-6318-49d0-8f29-d69fca17783a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Nothing16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fa100be-3925-4aac-bc72-dff6a1182ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "594d9ceb-6318-49d0-8f29-d69fca17783a",
            "compositeImage": {
                "id": "a158ea63-49b1-40ec-821f-ade26a1fa0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa100be-3925-4aac-bc72-dff6a1182ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f5cb739-5e08-4435-9ece-7d04436ac156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa100be-3925-4aac-bc72-dff6a1182ffe",
                    "LayerId": "5b77111a-3fab-4800-bc9d-87da500dd609"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5b77111a-3fab-4800-bc9d-87da500dd609",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "594d9ceb-6318-49d0-8f29-d69fca17783a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}