{
    "id": "b2166f53-d291-4ee0-b960-1864d899886d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Nothing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f687ab45-c35a-4413-98d2-347523d2d273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2166f53-d291-4ee0-b960-1864d899886d",
            "compositeImage": {
                "id": "d2bf71cf-6e4a-4ba9-8d7e-0c2a5f10f79f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f687ab45-c35a-4413-98d2-347523d2d273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17d9a04a-405b-4d56-8fbb-4573634563ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f687ab45-c35a-4413-98d2-347523d2d273",
                    "LayerId": "74956491-09cf-490f-bea4-267fb25bd350"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "74956491-09cf-490f-bea4-267fb25bd350",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2166f53-d291-4ee0-b960-1864d899886d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}