{
    "id": "9c1c16fb-3785-4618-a246-d668b92fc6d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 11,
    "bbox_right": 115,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a0e0bc0-3eaf-4125-ad55-fd7ad26bd4c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1c16fb-3785-4618-a246-d668b92fc6d4",
            "compositeImage": {
                "id": "58c56280-b13d-4080-9709-3ba94d425e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0e0bc0-3eaf-4125-ad55-fd7ad26bd4c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93dff8b6-0268-42bf-b0e8-968f550bedce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0e0bc0-3eaf-4125-ad55-fd7ad26bd4c7",
                    "LayerId": "92c5b658-3938-44b5-8de2-c8801df9034b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "92c5b658-3938-44b5-8de2-c8801df9034b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c1c16fb-3785-4618-a246-d668b92fc6d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 68
}