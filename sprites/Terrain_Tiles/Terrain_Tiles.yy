{
    "id": "4ad36ea1-da11-4a4f-bf83-2fb6b61b5a2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Terrain_Tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 495,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5c550a3-6801-475a-91ba-cdcae60d6d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ad36ea1-da11-4a4f-bf83-2fb6b61b5a2a",
            "compositeImage": {
                "id": "cf5ef7ce-4210-4863-be5b-367475d255b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c550a3-6801-475a-91ba-cdcae60d6d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f23e165e-ef03-41ec-bd30-b73e2af36b38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c550a3-6801-475a-91ba-cdcae60d6d59",
                    "LayerId": "e196c6ff-7722-434a-b2ad-b1ebf5c031eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "e196c6ff-7722-434a-b2ad-b1ebf5c031eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ad36ea1-da11-4a4f-bf83-2fb6b61b5a2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}