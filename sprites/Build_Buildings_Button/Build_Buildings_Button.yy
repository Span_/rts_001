{
    "id": "65ae33b0-3ad1-46a9-bc80-6de39fd9e41b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Build_Buildings_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56e21ffb-9e45-4fca-88a2-35692bc5d543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65ae33b0-3ad1-46a9-bc80-6de39fd9e41b",
            "compositeImage": {
                "id": "1ca7215b-236d-45f2-b23c-0534efc9baca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e21ffb-9e45-4fca-88a2-35692bc5d543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4777e97-80dc-42d9-9ce9-36e896c6e04d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e21ffb-9e45-4fca-88a2-35692bc5d543",
                    "LayerId": "842434d3-9185-4a51-b72a-8d4731154f3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "842434d3-9185-4a51-b72a-8d4731154f3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65ae33b0-3ad1-46a9-bc80-6de39fd9e41b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}