{
    "id": "681621ea-5317-4404-bfc2-0cf1155b50f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Chaos_Spawner_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 16,
    "bbox_right": 48,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7655bf91-2aa0-4cab-aa65-137774df0100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "ea0e2559-4d96-447a-8206-db778a4f4dc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7655bf91-2aa0-4cab-aa65-137774df0100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98556c26-a0e4-4cfa-8e2f-7350ce2c5d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7655bf91-2aa0-4cab-aa65-137774df0100",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        },
        {
            "id": "357da5db-5429-40bf-a029-e2ed5059b4de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "54097f80-158d-4954-96fe-0268f9a0cc0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "357da5db-5429-40bf-a029-e2ed5059b4de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4606c545-571e-40ed-b132-a527215c3663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "357da5db-5429-40bf-a029-e2ed5059b4de",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        },
        {
            "id": "8dffa136-a2a6-45ac-b9ef-3e111f0ddb9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "f076a766-3935-4e65-865a-9f61d6fdfd16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dffa136-a2a6-45ac-b9ef-3e111f0ddb9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf32bf8-94a8-4cc0-a6fe-015d3feb6c07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dffa136-a2a6-45ac-b9ef-3e111f0ddb9f",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        },
        {
            "id": "9c1a7d43-a5aa-4012-91ac-fd9936db0591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "6986c5af-dd30-416f-853d-520b2826d467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c1a7d43-a5aa-4012-91ac-fd9936db0591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5213a96d-c28b-48d8-b8ee-01aaf538c25b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c1a7d43-a5aa-4012-91ac-fd9936db0591",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        },
        {
            "id": "a664581c-ea56-4a85-8ad5-1467eea2e6e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "d4785e27-f12e-4f1c-8335-5bc9edb2e9c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a664581c-ea56-4a85-8ad5-1467eea2e6e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6b6b4c-21e2-4965-b07e-9fe24e50c3af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a664581c-ea56-4a85-8ad5-1467eea2e6e5",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        },
        {
            "id": "9aab9005-de48-425f-818d-89b91a5f744d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "64cab396-8ec3-43c3-85cc-f840d153a29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aab9005-de48-425f-818d-89b91a5f744d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50f926d-12ab-4049-9b12-d0c378fddbc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aab9005-de48-425f-818d-89b91a5f744d",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        },
        {
            "id": "c2da7380-297f-485a-9a44-63f2ece8573d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "78a26880-c4cd-4847-918f-13434276a206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2da7380-297f-485a-9a44-63f2ece8573d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c788ba23-5927-457d-af87-e5a7e2ff57f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2da7380-297f-485a-9a44-63f2ece8573d",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        },
        {
            "id": "bdac5ffb-4e3c-4000-8c47-35d3f1e72589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "compositeImage": {
                "id": "ca39978e-5fb8-4489-b495-804da721801e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdac5ffb-4e3c-4000-8c47-35d3f1e72589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e70558cf-dba6-4330-8254-d737a58292ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdac5ffb-4e3c-4000-8c47-35d3f1e72589",
                    "LayerId": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "87dffd43-a80f-4f4b-8ca5-3d45b9395d43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "681621ea-5317-4404-bfc2-0cf1155b50f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}