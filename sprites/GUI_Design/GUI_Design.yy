{
    "id": "18fd74d2-03a2-4d5f-8a7b-d70b0825cf66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GUI_Design",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae2859e2-2af5-4e69-a3cc-72cc7dd4432e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18fd74d2-03a2-4d5f-8a7b-d70b0825cf66",
            "compositeImage": {
                "id": "aa4c055a-1cd8-4322-91b6-b18d26e794ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae2859e2-2af5-4e69-a3cc-72cc7dd4432e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c4c0dd8-cbfb-4608-ab37-9c6a4dcd5210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae2859e2-2af5-4e69-a3cc-72cc7dd4432e",
                    "LayerId": "282cb360-4416-4596-b2bb-458b9f534c30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "282cb360-4416-4596-b2bb-458b9f534c30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18fd74d2-03a2-4d5f-8a7b-d70b0825cf66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}