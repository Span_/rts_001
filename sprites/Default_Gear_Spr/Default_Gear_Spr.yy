{
    "id": "79e1801b-f0ee-4bac-beda-642a3427d7b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Default_Gear_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46c6a739-bd4d-49ff-815b-ec80f72ff564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79e1801b-f0ee-4bac-beda-642a3427d7b9",
            "compositeImage": {
                "id": "4a52db6c-3c3b-4170-81c0-a54cd9d17e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c6a739-bd4d-49ff-815b-ec80f72ff564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb527d0-330f-48c3-90fb-7668ee193766",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c6a739-bd4d-49ff-815b-ec80f72ff564",
                    "LayerId": "ecfe4c93-9b63-4553-bc7f-71c6989d381a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ecfe4c93-9b63-4553-bc7f-71c6989d381a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79e1801b-f0ee-4bac-beda-642a3427d7b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}