{
    "id": "92d51235-b191-4a62-9ce6-3fc082a81e13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Chaos_Bolt_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e038220a-2f78-440f-bae4-5da6ebc7ce7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "ada1cb2f-8f4b-4c75-b5a0-23e896f8cd6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e038220a-2f78-440f-bae4-5da6ebc7ce7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa73aa8-484c-471c-9dce-58f040de4d6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e038220a-2f78-440f-bae4-5da6ebc7ce7b",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        },
        {
            "id": "1f211973-05b6-4531-8e59-658fbb209ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "b6ac27c1-1968-4ed2-9eb9-9c8fb3b6a750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f211973-05b6-4531-8e59-658fbb209ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "154cc365-7fa8-4be3-8d2c-ccf78dc5e545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f211973-05b6-4531-8e59-658fbb209ac1",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        },
        {
            "id": "9b0583d7-c3f6-44d2-8d73-f839793f5725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "51b76911-a702-423e-a43e-73755b549fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b0583d7-c3f6-44d2-8d73-f839793f5725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d9008ff-bc19-41a9-b52b-80df8eb5a343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b0583d7-c3f6-44d2-8d73-f839793f5725",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        },
        {
            "id": "5146b950-244a-41cc-99c2-43d75401c2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "c92052fe-2eb3-47ef-9bec-7b9da34d2652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5146b950-244a-41cc-99c2-43d75401c2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3654b081-29a1-4746-a2fe-0e2578f5b161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5146b950-244a-41cc-99c2-43d75401c2e3",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        },
        {
            "id": "ec8ce35a-1be1-4f65-b184-f1aabe058601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "af27a3ef-6362-489e-a9b0-adc7ce71662c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec8ce35a-1be1-4f65-b184-f1aabe058601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a42fb139-6621-485e-aace-0110f7ca2457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec8ce35a-1be1-4f65-b184-f1aabe058601",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        },
        {
            "id": "bac88893-8f25-4482-a61a-36bb6f0427a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "003559bc-c4e3-424f-9ca5-c4e61b9131fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bac88893-8f25-4482-a61a-36bb6f0427a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c69b0c-1c43-4738-a53e-e3978c98cc2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bac88893-8f25-4482-a61a-36bb6f0427a8",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        },
        {
            "id": "eaffd5d3-df1b-4011-aab2-c51425de0954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "2926a7e3-8855-4d9d-aaef-8cb90b21d24e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaffd5d3-df1b-4011-aab2-c51425de0954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01529fa7-ce67-495f-85d2-cfc21dfcff76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaffd5d3-df1b-4011-aab2-c51425de0954",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        },
        {
            "id": "d2cce736-fdca-4ce3-8045-3f5f76e58392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "compositeImage": {
                "id": "64694871-0e85-4ab7-b9a7-d66cdf64b1bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2cce736-fdca-4ce3-8045-3f5f76e58392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efcc6b03-7ad7-43f4-b96c-b98840a47e3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2cce736-fdca-4ce3-8045-3f5f76e58392",
                    "LayerId": "1c769bd7-0115-4af2-8216-cca0b223a5b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1c769bd7-0115-4af2-8216-cca0b223a5b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92d51235-b191-4a62-9ce6-3fc082a81e13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}