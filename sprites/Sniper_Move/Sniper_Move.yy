{
    "id": "1446a11e-f8a9-4003-8282-5c72ac21e2e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sniper_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 14,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8423f0b0-667c-418e-a752-3af68c862c13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1446a11e-f8a9-4003-8282-5c72ac21e2e5",
            "compositeImage": {
                "id": "78b57ed4-6739-4f9e-af74-9300ac14c43d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8423f0b0-667c-418e-a752-3af68c862c13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f039a75-4a12-49c9-8794-5fd80c493277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8423f0b0-667c-418e-a752-3af68c862c13",
                    "LayerId": "6b952c8c-61f9-4b91-abbc-949118432105"
                }
            ]
        },
        {
            "id": "6b93dde0-f331-450c-a976-e51d929bd182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1446a11e-f8a9-4003-8282-5c72ac21e2e5",
            "compositeImage": {
                "id": "c14a6833-8232-49e5-889a-c2963eba1b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b93dde0-f331-450c-a976-e51d929bd182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "448768b7-63de-4dcb-b4c8-395b83697bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b93dde0-f331-450c-a976-e51d929bd182",
                    "LayerId": "6b952c8c-61f9-4b91-abbc-949118432105"
                }
            ]
        },
        {
            "id": "d6eb82aa-7ee8-433b-82af-b2ce9382ff1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1446a11e-f8a9-4003-8282-5c72ac21e2e5",
            "compositeImage": {
                "id": "cb323c67-c3e2-425a-b84e-19e5a907d0aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6eb82aa-7ee8-433b-82af-b2ce9382ff1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19d7e751-1c65-48c7-960f-dfb924bbd60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6eb82aa-7ee8-433b-82af-b2ce9382ff1c",
                    "LayerId": "6b952c8c-61f9-4b91-abbc-949118432105"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6b952c8c-61f9-4b91-abbc-949118432105",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1446a11e-f8a9-4003-8282-5c72ac21e2e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 17,
    "yorig": 8
}