{
    "id": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sniper_Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 14,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47e5f2ac-9040-44c6-a9ed-0dbb2c49b40c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "compositeImage": {
                "id": "c3eb9bf6-9c4f-47a0-b874-7acd03cc1b60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e5f2ac-9040-44c6-a9ed-0dbb2c49b40c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91172413-5b1d-4f92-8980-9dd8d967db02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e5f2ac-9040-44c6-a9ed-0dbb2c49b40c",
                    "LayerId": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f"
                }
            ]
        },
        {
            "id": "abc7c089-992a-4ceb-a9c9-a2f34779bd1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "compositeImage": {
                "id": "2d4b4c69-9b26-417d-af9a-450a626c84fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abc7c089-992a-4ceb-a9c9-a2f34779bd1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8090f200-14b8-42f9-89f9-09773fd57f8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abc7c089-992a-4ceb-a9c9-a2f34779bd1a",
                    "LayerId": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f"
                }
            ]
        },
        {
            "id": "c2457a35-8f19-4f6d-81c0-7e085543d11e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "compositeImage": {
                "id": "8aef3d24-b20f-407e-a45c-6f6f48fc04f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2457a35-8f19-4f6d-81c0-7e085543d11e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db629403-2641-43c7-b6f8-19b69ea5a0f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2457a35-8f19-4f6d-81c0-7e085543d11e",
                    "LayerId": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f"
                }
            ]
        },
        {
            "id": "9f640f98-429c-4611-9ad2-46b72ff389bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "compositeImage": {
                "id": "ab49b78e-51ce-49ab-a7fd-0910fdf38efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f640f98-429c-4611-9ad2-46b72ff389bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "904862b0-79d7-4b04-aa89-9767a84a2972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f640f98-429c-4611-9ad2-46b72ff389bf",
                    "LayerId": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f"
                }
            ]
        },
        {
            "id": "a4be7c59-676b-4f7b-a135-a6d1cfc4f7ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "compositeImage": {
                "id": "9970eca4-17a1-4eb2-a6ba-e6774b42dab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4be7c59-676b-4f7b-a135-a6d1cfc4f7ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab5025b-cd18-412f-ade9-08ea276e48e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4be7c59-676b-4f7b-a135-a6d1cfc4f7ab",
                    "LayerId": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f"
                }
            ]
        },
        {
            "id": "00a795f9-b5cf-4e7d-b771-00b825b56a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "compositeImage": {
                "id": "7a77f574-3388-41ed-a62a-c1ce8ae499a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a795f9-b5cf-4e7d-b771-00b825b56a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5582ad7-d9e1-4c9a-b467-cb34acae3bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a795f9-b5cf-4e7d-b771-00b825b56a6a",
                    "LayerId": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f"
                }
            ]
        },
        {
            "id": "c94510cb-c73c-4fb3-8c00-273ea4523019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "compositeImage": {
                "id": "46e5ded7-7a41-4f56-8de4-05c114cf916e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c94510cb-c73c-4fb3-8c00-273ea4523019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148d285f-a087-4bfe-bc9e-21ed7ebf9c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c94510cb-c73c-4fb3-8c00-273ea4523019",
                    "LayerId": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b21eb0e8-9252-4b9d-890a-d9c02e34c58f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fd46bfd-0368-4768-a3d5-9f7b3bf90534",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 8
}