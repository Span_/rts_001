{
    "id": "66ec066d-27a6-47d5-a2da-5f7f9d0ed59a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 14,
    "bbox_right": 45,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3f7c44c-2d9e-448e-86ba-e76b182acc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ec066d-27a6-47d5-a2da-5f7f9d0ed59a",
            "compositeImage": {
                "id": "18537fa4-58a5-49f1-8ef4-981a9ef54d57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f7c44c-2d9e-448e-86ba-e76b182acc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f56dfc-0f71-4630-83af-766446775f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f7c44c-2d9e-448e-86ba-e76b182acc08",
                    "LayerId": "1354fb04-0a26-4c64-a164-2a9fa505ff7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1354fb04-0a26-4c64-a164-2a9fa505ff7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66ec066d-27a6-47d5-a2da-5f7f9d0ed59a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 29,
    "yorig": 29
}