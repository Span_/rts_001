{
    "id": "8812b3d5-fd14-4115-91a9-e0cbfa075dc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Farm_Sprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 4,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5aba0eb2-1fc1-4ec2-b0ee-cea79d05e0e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8812b3d5-fd14-4115-91a9-e0cbfa075dc5",
            "compositeImage": {
                "id": "764be135-4e8b-43f1-ac51-d1ab717717d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aba0eb2-1fc1-4ec2-b0ee-cea79d05e0e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "731c1b17-bcdc-4320-8a10-ce6c2fb0f61b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aba0eb2-1fc1-4ec2-b0ee-cea79d05e0e3",
                    "LayerId": "c7b24835-3327-41d0-8cda-2569f30807c6"
                },
                {
                    "id": "fc8570a3-aa4d-42f7-99de-968fa2e0c339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aba0eb2-1fc1-4ec2-b0ee-cea79d05e0e3",
                    "LayerId": "76fbeff6-05fb-4ff9-9887-af18d685d517"
                },
                {
                    "id": "c58f5a6e-a39c-4196-9439-7e2832136f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aba0eb2-1fc1-4ec2-b0ee-cea79d05e0e3",
                    "LayerId": "5452a99d-98bc-480d-ad2d-156a78db822b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c7b24835-3327-41d0-8cda-2569f30807c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8812b3d5-fd14-4115-91a9-e0cbfa075dc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "76fbeff6-05fb-4ff9-9887-af18d685d517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8812b3d5-fd14-4115-91a9-e0cbfa075dc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5452a99d-98bc-480d-ad2d-156a78db822b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8812b3d5-fd14-4115-91a9-e0cbfa075dc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}