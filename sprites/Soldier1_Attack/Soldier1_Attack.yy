{
    "id": "409bdc7f-6a83-486d-8726-d8899ad53773",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Soldier1_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 4,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a82e62ce-c068-4eeb-9867-5dabc60a11e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "8c281256-100e-4317-9133-80aad2737511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a82e62ce-c068-4eeb-9867-5dabc60a11e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f2ab7e-45a3-4dd6-9025-933b7d2307af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a82e62ce-c068-4eeb-9867-5dabc60a11e9",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "488545b6-9470-48a0-a1b3-a9d30972748c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a82e62ce-c068-4eeb-9867-5dabc60a11e9",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        },
        {
            "id": "712c2136-ecb9-4ab8-a5c2-811e74babefb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "e0b5e4c8-3618-4eab-ac52-32ae2653f2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "712c2136-ecb9-4ab8-a5c2-811e74babefb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c479e3b-fe07-4a2b-a8e3-4cb1ef0af80e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712c2136-ecb9-4ab8-a5c2-811e74babefb",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "99409025-bc2f-4a02-a2c2-aedeeb7f17dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712c2136-ecb9-4ab8-a5c2-811e74babefb",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        },
        {
            "id": "cffeceae-6b7c-401f-b641-893d36899e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "3e750138-a1c5-4a05-b367-4d26f98c317a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffeceae-6b7c-401f-b641-893d36899e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "995e9513-9452-4399-b6e5-c99b2271eedb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffeceae-6b7c-401f-b641-893d36899e20",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "f37f537c-1215-434a-bf01-d21d8a0f4b11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffeceae-6b7c-401f-b641-893d36899e20",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        },
        {
            "id": "a1dfe54c-1fce-4f1e-af2b-4a86f563e84c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "24074252-78fc-4693-ad5b-3dbc1a76399e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1dfe54c-1fce-4f1e-af2b-4a86f563e84c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e24944d-f922-417f-b486-2eb8bdd0bbef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1dfe54c-1fce-4f1e-af2b-4a86f563e84c",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "cfbdf000-81c8-4a97-b13f-45703e7723ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1dfe54c-1fce-4f1e-af2b-4a86f563e84c",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        },
        {
            "id": "024f02ad-e6be-49e6-97bc-e9cc9614db2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "17456c06-a93b-427c-8f21-a5e40fb43d63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "024f02ad-e6be-49e6-97bc-e9cc9614db2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a5e5da3-5b3c-4525-95ee-9cfaeb1aa4ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "024f02ad-e6be-49e6-97bc-e9cc9614db2d",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "28a597c7-67b6-4431-b77e-5cfcfa376911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "024f02ad-e6be-49e6-97bc-e9cc9614db2d",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        },
        {
            "id": "3da908a8-e272-4a33-bf6f-b3ada843dfb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "70e02d59-ae8f-4c17-8a1f-1e858baf370f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da908a8-e272-4a33-bf6f-b3ada843dfb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80e82730-8fb6-4a7f-9f95-72f3fb9dd140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da908a8-e272-4a33-bf6f-b3ada843dfb5",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "7b0de1d6-878b-4cce-a81e-93b7b3baee91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da908a8-e272-4a33-bf6f-b3ada843dfb5",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        },
        {
            "id": "767157a6-c894-481b-8e12-4fe9535d1e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "954f6b42-cb37-4897-96c5-5ac7d7e6285f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "767157a6-c894-481b-8e12-4fe9535d1e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c45a3c2-5fb1-4533-86a3-491b6ee30341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "767157a6-c894-481b-8e12-4fe9535d1e48",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "c511a175-c7e2-42a8-8c86-7e396de7c395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "767157a6-c894-481b-8e12-4fe9535d1e48",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        },
        {
            "id": "a2e28388-07fb-4b2d-93cb-21fd94c1cdc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "compositeImage": {
                "id": "329f719c-30ba-45ac-8bbc-3f4c3c13e629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e28388-07fb-4b2d-93cb-21fd94c1cdc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b1b573-fba5-4457-925b-3fc29b4ce993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e28388-07fb-4b2d-93cb-21fd94c1cdc1",
                    "LayerId": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7"
                },
                {
                    "id": "60e21462-8d6b-4618-91f8-aced7aa7c535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e28388-07fb-4b2d-93cb-21fd94c1cdc1",
                    "LayerId": "b066fef8-39f2-4f75-9c32-3f0ceca16f44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b066fef8-39f2-4f75-9c32-3f0ceca16f44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "842d8dcd-f60b-47d8-bbcf-c14ea1f6d3c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "409bdc7f-6a83-486d-8726-d8899ad53773",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 8
}