{
    "id": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Horse_Mage_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "966e24e6-a9c4-411c-800b-ab8c4cdbcb89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "a62cb8d4-c277-4c18-898c-fb73a968bcee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "966e24e6-a9c4-411c-800b-ab8c4cdbcb89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7797e1d-a742-4fba-a718-bacdbfc2ed79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "966e24e6-a9c4-411c-800b-ab8c4cdbcb89",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "be999634-20ee-4b96-9157-3530d4da56f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "966e24e6-a9c4-411c-800b-ab8c4cdbcb89",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        },
        {
            "id": "9fed4dc6-c136-44de-8163-8c194cad5495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "86781910-bd39-4cb6-844f-64ad4a0243d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fed4dc6-c136-44de-8163-8c194cad5495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0095a3c-41ab-43b2-bf84-cbcd6811287d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fed4dc6-c136-44de-8163-8c194cad5495",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "85cc666a-3619-426b-b35a-dde398a84097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fed4dc6-c136-44de-8163-8c194cad5495",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        },
        {
            "id": "cd1c3f03-f706-4afe-8090-876726182716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "4e8f02f1-0ed5-4b0c-b5ed-bbd9dd4e49b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1c3f03-f706-4afe-8090-876726182716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3714a0ce-cc39-4265-8b68-470222a8399e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1c3f03-f706-4afe-8090-876726182716",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "599b90eb-4bb1-4f8e-87f8-b5816de27e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1c3f03-f706-4afe-8090-876726182716",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        },
        {
            "id": "c0d68132-fc27-4347-ae7c-ea349301fd4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "1583c6b2-b911-4b69-a3af-6cda5a6f126b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d68132-fc27-4347-ae7c-ea349301fd4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de735707-de25-40f9-a735-133f7068bfe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d68132-fc27-4347-ae7c-ea349301fd4b",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "6a59b273-4a81-4fb6-b519-92588d881bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d68132-fc27-4347-ae7c-ea349301fd4b",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        },
        {
            "id": "fa66a4eb-8c21-463c-9e77-add041ea9081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "3bbded10-f227-4873-a7c8-3bef639ac65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa66a4eb-8c21-463c-9e77-add041ea9081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0167d604-8ec0-4e6f-a8cf-f67d7edfa8f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa66a4eb-8c21-463c-9e77-add041ea9081",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "8d5830cb-416e-4c95-b2de-fddaa4b7eaab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa66a4eb-8c21-463c-9e77-add041ea9081",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        },
        {
            "id": "16eca154-9604-47bb-aaae-af9547e92642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "08c3bf35-947e-49db-816d-981a7111032e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16eca154-9604-47bb-aaae-af9547e92642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d77db3-f45b-41b6-b2ae-abc0a2703506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16eca154-9604-47bb-aaae-af9547e92642",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "07b25d82-036b-4df5-83bf-163433bd3ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16eca154-9604-47bb-aaae-af9547e92642",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        },
        {
            "id": "83673eea-f220-4cc2-80b7-2ac4a3f43aeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "13b0f453-08d2-4791-9835-dc3c4b27a65d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83673eea-f220-4cc2-80b7-2ac4a3f43aeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92ee75d3-d75c-4cde-bdef-2674574fa71a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83673eea-f220-4cc2-80b7-2ac4a3f43aeb",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "b44afa4d-bc29-4fc3-80d8-04984d0bf348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83673eea-f220-4cc2-80b7-2ac4a3f43aeb",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        },
        {
            "id": "f9df1fac-02a6-433d-886f-6fcd6b8b3ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "compositeImage": {
                "id": "7016f348-2c14-4067-a9a6-05c2e3ba60a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9df1fac-02a6-433d-886f-6fcd6b8b3ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99c5fa3-346d-44c3-91e1-bebe2b6d5a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9df1fac-02a6-433d-886f-6fcd6b8b3ea8",
                    "LayerId": "119d684a-d51f-43f5-942e-be88c7e5299c"
                },
                {
                    "id": "0f98bb59-8f29-41cb-a9f9-dcf916be1453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9df1fac-02a6-433d-886f-6fcd6b8b3ea8",
                    "LayerId": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "119d684a-d51f-43f5-942e-be88c7e5299c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4dd430d8-e1b9-4cad-a9ba-958c0a1eb09c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c45255c9-4c29-459f-bf1a-8a0f236a1154",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}