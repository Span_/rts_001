{
    "id": "8ad4af53-a9d5-4e1f-8ea3-2b0283198a34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_Repair_Bot_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24a39ddf-f444-4218-93c6-2b840b5b62ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ad4af53-a9d5-4e1f-8ea3-2b0283198a34",
            "compositeImage": {
                "id": "b6dd85e1-93ce-4c84-b03a-2cb43d48037f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a39ddf-f444-4218-93c6-2b840b5b62ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493b4ada-7e53-4f53-827b-25ad1b476bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a39ddf-f444-4218-93c6-2b840b5b62ff",
                    "LayerId": "11383b37-2a4e-4670-9df6-d017a8f68bc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "11383b37-2a4e-4670-9df6-d017a8f68bc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ad4af53-a9d5-4e1f-8ea3-2b0283198a34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}