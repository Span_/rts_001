{
    "id": "0ee6ad00-b138-456a-8a7c-39b07b4db56f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rally_Point",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 7,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62c5443d-a8f0-47ac-ae0b-c3433bc0cea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ee6ad00-b138-456a-8a7c-39b07b4db56f",
            "compositeImage": {
                "id": "63371e9d-2fde-44d5-b6f3-4a9c6cabcf58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62c5443d-a8f0-47ac-ae0b-c3433bc0cea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6a5ac2f-dd2f-464e-aa12-b685fec70afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62c5443d-a8f0-47ac-ae0b-c3433bc0cea3",
                    "LayerId": "783c8911-e930-485d-a340-6e293734f3a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "783c8911-e930-485d-a340-6e293734f3a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ee6ad00-b138-456a-8a7c-39b07b4db56f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 14
}