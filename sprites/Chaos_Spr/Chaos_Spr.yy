{
    "id": "1602978c-9607-42c1-8f29-e4cea87b3220",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Chaos_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 21,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a3cde86-0bf2-4ce6-a291-dbcb4e181365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "7da0143b-6967-45a3-8d60-6dbf81af56cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3cde86-0bf2-4ce6-a291-dbcb4e181365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea139e97-8724-40ca-8ff0-7410ef0a0857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3cde86-0bf2-4ce6-a291-dbcb4e181365",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        },
        {
            "id": "d6b36f75-db82-4e4e-bca2-d8333c4388ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "483fe854-8de1-46e7-910d-250d748aeb6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b36f75-db82-4e4e-bca2-d8333c4388ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a34f240-d5d3-46b0-97b7-5eee7c82762f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b36f75-db82-4e4e-bca2-d8333c4388ac",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        },
        {
            "id": "c82a0533-104a-4dfe-b7f7-95cca76056be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "1b5b740f-c4ae-434e-abbc-65c42b2c6253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82a0533-104a-4dfe-b7f7-95cca76056be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7122cbba-0c0f-4d56-80f6-d23ffbd614fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82a0533-104a-4dfe-b7f7-95cca76056be",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        },
        {
            "id": "0d5e67ba-88f2-40b8-806b-5b217eb3468a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "ef4bde0f-e793-4a97-acda-33e589c9d824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d5e67ba-88f2-40b8-806b-5b217eb3468a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e9c8bc-8dd2-47e9-a28b-4490fb0251a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d5e67ba-88f2-40b8-806b-5b217eb3468a",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        },
        {
            "id": "a7831c2c-d1fa-4204-9389-f97b162aae79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "d942f6ef-3931-493f-96cd-0e1f2b5da11a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7831c2c-d1fa-4204-9389-f97b162aae79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28cf9020-db3d-4c1d-a30f-23d24e4fb351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7831c2c-d1fa-4204-9389-f97b162aae79",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        },
        {
            "id": "b3b39fec-0934-4dea-90b8-7248de0adb3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "7c3f9673-38e9-4b1b-a38f-521e52e61095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b39fec-0934-4dea-90b8-7248de0adb3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b3a272-4532-4f5c-bee1-74e5715aad4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b39fec-0934-4dea-90b8-7248de0adb3f",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        },
        {
            "id": "0e3466a1-f22d-466e-8a3d-8bab82df0523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "dd5a28e7-f021-4465-bbd7-b7e07aef2a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e3466a1-f22d-466e-8a3d-8bab82df0523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54c3693-fcce-4952-9cab-19a09a0f2814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e3466a1-f22d-466e-8a3d-8bab82df0523",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        },
        {
            "id": "43b87727-eff0-47e7-a803-53df97dbc53b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "compositeImage": {
                "id": "fa03be43-dc7f-4ffb-af16-9fc72b32a3c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43b87727-eff0-47e7-a803-53df97dbc53b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef23330d-d11c-4dd6-8ffd-c07d1c494a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43b87727-eff0-47e7-a803-53df97dbc53b",
                    "LayerId": "04ec9837-3d99-4a83-8007-ec964b07bb22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04ec9837-3d99-4a83-8007-ec964b07bb22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1602978c-9607-42c1-8f29-e4cea87b3220",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}