{
    "id": "5b213080-409c-4a88-863d-44cfa6a8c169",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Machine_Gun_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "355ab9b0-4f2d-4498-91bd-913786f6f850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b213080-409c-4a88-863d-44cfa6a8c169",
            "compositeImage": {
                "id": "3dae6b08-9225-4d3b-85ac-bb82346897d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "355ab9b0-4f2d-4498-91bd-913786f6f850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b0fec9c-1513-42a7-860c-731dfec87ece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "355ab9b0-4f2d-4498-91bd-913786f6f850",
                    "LayerId": "75d475f9-91d6-477b-8874-4d6167a17d76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "75d475f9-91d6-477b-8874-4d6167a17d76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b213080-409c-4a88-863d-44cfa6a8c169",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}