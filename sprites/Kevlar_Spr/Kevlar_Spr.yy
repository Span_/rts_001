{
    "id": "cd652b7d-fea5-4a81-bbf5-c950f80229e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Kevlar_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c7e3133-532b-4a5c-a765-67a1fe09a45e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd652b7d-fea5-4a81-bbf5-c950f80229e4",
            "compositeImage": {
                "id": "2c173643-cc07-4986-8986-654c4ae7b8bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c7e3133-532b-4a5c-a765-67a1fe09a45e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e130dac2-ab69-489e-9f33-8911116ae1e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c7e3133-532b-4a5c-a765-67a1fe09a45e",
                    "LayerId": "b5576b2c-3370-42c1-bf57-0ee61d0158f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b5576b2c-3370-42c1-bf57-0ee61d0158f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd652b7d-fea5-4a81-bbf5-c950f80229e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}