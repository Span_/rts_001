{
    "id": "90e55ff6-0a3a-46ca-9da1-32933ea5ad3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Repair_Bot_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47669cb3-9da4-497e-b4da-3ec58a731d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90e55ff6-0a3a-46ca-9da1-32933ea5ad3d",
            "compositeImage": {
                "id": "390786a9-daac-4bd1-850d-0d4644e553c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47669cb3-9da4-497e-b4da-3ec58a731d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9838ac6e-5398-46e2-a108-087091e34d09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47669cb3-9da4-497e-b4da-3ec58a731d00",
                    "LayerId": "8c25a1bc-0d09-48a7-b678-2f0e3926b935"
                }
            ]
        },
        {
            "id": "45627059-cb5d-49f4-8d19-e7d462fc1dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90e55ff6-0a3a-46ca-9da1-32933ea5ad3d",
            "compositeImage": {
                "id": "dab8feba-6151-47e8-bbd9-0db378960275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45627059-cb5d-49f4-8d19-e7d462fc1dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44937c70-b750-4c68-af6c-77d59849c797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45627059-cb5d-49f4-8d19-e7d462fc1dde",
                    "LayerId": "8c25a1bc-0d09-48a7-b678-2f0e3926b935"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8c25a1bc-0d09-48a7-b678-2f0e3926b935",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90e55ff6-0a3a-46ca-9da1-32933ea5ad3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}