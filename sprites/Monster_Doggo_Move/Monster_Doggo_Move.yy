{
    "id": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Monster_Doggo_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b86ddb6-e468-4da6-8fab-e3685429982d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
            "compositeImage": {
                "id": "744a54ee-e66b-4cfa-963d-2dac43d19e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b86ddb6-e468-4da6-8fab-e3685429982d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3524031-8c6f-463c-a470-868040d768e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b86ddb6-e468-4da6-8fab-e3685429982d",
                    "LayerId": "7d0fd9f8-19e1-4f2c-a819-a6a567a5aae0"
                }
            ]
        },
        {
            "id": "8553a2da-8697-4e37-b9fa-f23ffb44b8c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
            "compositeImage": {
                "id": "233b93e6-ad7d-4455-ab75-04d6fc08a5c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8553a2da-8697-4e37-b9fa-f23ffb44b8c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e3465a4-6946-4bcd-8405-058599f2ff5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8553a2da-8697-4e37-b9fa-f23ffb44b8c8",
                    "LayerId": "7d0fd9f8-19e1-4f2c-a819-a6a567a5aae0"
                }
            ]
        },
        {
            "id": "d7a7deaf-afbd-49f5-a8c4-1a6c55248ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
            "compositeImage": {
                "id": "82397c65-7f97-4a96-89ee-e038f629d32b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a7deaf-afbd-49f5-a8c4-1a6c55248ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8758b91-1a57-4fa3-a6e5-ea2af52cb97e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a7deaf-afbd-49f5-a8c4-1a6c55248ade",
                    "LayerId": "7d0fd9f8-19e1-4f2c-a819-a6a567a5aae0"
                }
            ]
        },
        {
            "id": "fd32ef7c-e1d4-46fd-84ee-bd851969b7ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
            "compositeImage": {
                "id": "caaa072d-d877-474a-9bab-ce4134ec628e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd32ef7c-e1d4-46fd-84ee-bd851969b7ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619f92dd-9e77-47f9-9eda-3ed06f19b378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd32ef7c-e1d4-46fd-84ee-bd851969b7ee",
                    "LayerId": "7d0fd9f8-19e1-4f2c-a819-a6a567a5aae0"
                }
            ]
        },
        {
            "id": "d606b7da-fb2e-4161-b49c-7920cc92f338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
            "compositeImage": {
                "id": "84f5758a-e8a2-4868-bc30-c79998cab745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d606b7da-fb2e-4161-b49c-7920cc92f338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59225908-ac59-4168-a536-b7ba1c0d87e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d606b7da-fb2e-4161-b49c-7920cc92f338",
                    "LayerId": "7d0fd9f8-19e1-4f2c-a819-a6a567a5aae0"
                }
            ]
        },
        {
            "id": "a6636496-f3b7-44dc-a2f5-3cd1ca8878bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
            "compositeImage": {
                "id": "9a021a90-6556-48b9-96cc-3252f4ed0e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6636496-f3b7-44dc-a2f5-3cd1ca8878bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebec753-1c59-47ee-863e-b258f37b5d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6636496-f3b7-44dc-a2f5-3cd1ca8878bc",
                    "LayerId": "7d0fd9f8-19e1-4f2c-a819-a6a567a5aae0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7d0fd9f8-19e1-4f2c-a819-a6a567a5aae0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d181017c-ea35-4f0a-8ea7-e07fca80387a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}