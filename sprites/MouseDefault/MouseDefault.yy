{
    "id": "9d4c513e-d35f-46e8-bfc7-462c401321ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MouseDefault",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "505f9f3f-cf66-4b7d-975a-a6ec3daceb86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d4c513e-d35f-46e8-bfc7-462c401321ce",
            "compositeImage": {
                "id": "3c0f85bc-d648-458e-ba24-6ddb64dac493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "505f9f3f-cf66-4b7d-975a-a6ec3daceb86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca908e7f-a15c-4f3d-9012-022932ae8116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "505f9f3f-cf66-4b7d-975a-a6ec3daceb86",
                    "LayerId": "f4d0ebf4-2b2e-4546-9fdc-14d1ee150f9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f4d0ebf4-2b2e-4546-9fdc-14d1ee150f9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d4c513e-d35f-46e8-bfc7-462c401321ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}