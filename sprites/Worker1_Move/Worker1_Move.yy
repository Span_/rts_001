{
    "id": "4c55c65b-74ab-4854-8577-bb5730527731",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Worker1_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d40ecdf-0c2d-4398-821f-c0f4026de76c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c55c65b-74ab-4854-8577-bb5730527731",
            "compositeImage": {
                "id": "33365e4a-936f-4295-aba3-9481dc61cf7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d40ecdf-0c2d-4398-821f-c0f4026de76c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbfca287-28c0-4435-9120-1f32c3c1acca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d40ecdf-0c2d-4398-821f-c0f4026de76c",
                    "LayerId": "2cbc0937-9988-4a1d-980d-85312c5857c1"
                }
            ]
        },
        {
            "id": "5e3e4dea-2fe4-477c-9e27-e6a49f0917ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c55c65b-74ab-4854-8577-bb5730527731",
            "compositeImage": {
                "id": "a877188c-11b3-47af-a48b-b03c3ed61827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e3e4dea-2fe4-477c-9e27-e6a49f0917ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4880681b-8ffb-4881-962f-552082d2845a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e3e4dea-2fe4-477c-9e27-e6a49f0917ed",
                    "LayerId": "2cbc0937-9988-4a1d-980d-85312c5857c1"
                }
            ]
        },
        {
            "id": "e8704126-35b7-4630-bf57-a0ac04851d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c55c65b-74ab-4854-8577-bb5730527731",
            "compositeImage": {
                "id": "09d4dc82-9a38-48dc-9203-7735fdb88326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8704126-35b7-4630-bf57-a0ac04851d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e6690f5-1963-41f5-aafb-f4d1c24c3f60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8704126-35b7-4630-bf57-a0ac04851d12",
                    "LayerId": "2cbc0937-9988-4a1d-980d-85312c5857c1"
                }
            ]
        },
        {
            "id": "4104addb-c987-4970-946a-525950e52023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c55c65b-74ab-4854-8577-bb5730527731",
            "compositeImage": {
                "id": "492d52bd-7d63-485c-ba7a-37503516919a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4104addb-c987-4970-946a-525950e52023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c31b39ba-7ce6-42b3-9cb2-94488b2d1fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4104addb-c987-4970-946a-525950e52023",
                    "LayerId": "2cbc0937-9988-4a1d-980d-85312c5857c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2cbc0937-9988-4a1d-980d-85312c5857c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c55c65b-74ab-4854-8577-bb5730527731",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}