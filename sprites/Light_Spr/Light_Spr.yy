{
    "id": "f2ae1ba9-c2c3-4406-b4b5-05009185858f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Light_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52a82f3c-2f86-4bba-97ed-b81ac08e9811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2ae1ba9-c2c3-4406-b4b5-05009185858f",
            "compositeImage": {
                "id": "24e4ace4-e610-40a9-89e6-044fc752d83c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52a82f3c-2f86-4bba-97ed-b81ac08e9811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f17a495-c125-4280-8cc3-7ca2dc154dff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52a82f3c-2f86-4bba-97ed-b81ac08e9811",
                    "LayerId": "4b4cbdf8-91ed-4c06-bed9-00a78a026713"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4b4cbdf8-91ed-4c06-bed9-00a78a026713",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2ae1ba9-c2c3-4406-b4b5-05009185858f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}