{
    "id": "c86819c3-011c-4180-a1aa-97002a4758d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_Farm_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a86b597c-19aa-4784-bc52-22568e6dd5a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c86819c3-011c-4180-a1aa-97002a4758d1",
            "compositeImage": {
                "id": "b78080bc-be5b-4ded-bfb4-f8e2eaf88486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a86b597c-19aa-4784-bc52-22568e6dd5a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9dc1d97-e99b-42c4-a8a0-1a38523db8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a86b597c-19aa-4784-bc52-22568e6dd5a3",
                    "LayerId": "23d07a6d-c954-416e-a08a-cabdcb5a622d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "23d07a6d-c954-416e-a08a-cabdcb5a622d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c86819c3-011c-4180-a1aa-97002a4758d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}