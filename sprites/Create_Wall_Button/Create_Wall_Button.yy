{
    "id": "c0d4342a-82ea-451f-9eb7-97a6be193c77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_Wall_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b3e1e71-5f91-4f90-836f-84fdb4040fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0d4342a-82ea-451f-9eb7-97a6be193c77",
            "compositeImage": {
                "id": "0a0dc276-d70e-4a11-889d-0515e11e2fdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b3e1e71-5f91-4f90-836f-84fdb4040fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c4b1638-7106-4154-b39e-65a87cf6db40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b3e1e71-5f91-4f90-836f-84fdb4040fb9",
                    "LayerId": "21f6c361-c815-467e-b823-abb8a66aefb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "21f6c361-c815-467e-b823-abb8a66aefb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0d4342a-82ea-451f-9eb7-97a6be193c77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}