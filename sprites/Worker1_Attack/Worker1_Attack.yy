{
    "id": "6a76b7ae-6e20-4d1d-b0b7-522d0bc9237c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Worker1_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12c6af57-27c4-404a-9d1a-34a8e942b397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a76b7ae-6e20-4d1d-b0b7-522d0bc9237c",
            "compositeImage": {
                "id": "f2d69a7c-843b-419a-a8a3-ed2e7f0f60ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c6af57-27c4-404a-9d1a-34a8e942b397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b14c50d8-513b-4c30-b9cc-f334c79fb1f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c6af57-27c4-404a-9d1a-34a8e942b397",
                    "LayerId": "f1d3d6a0-8727-4e80-bf0c-724a2a412b8b"
                }
            ]
        },
        {
            "id": "2877d0f1-6051-4ce0-abde-303d4ae9742b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a76b7ae-6e20-4d1d-b0b7-522d0bc9237c",
            "compositeImage": {
                "id": "e28131b6-3591-49e5-a219-17c10613597e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2877d0f1-6051-4ce0-abde-303d4ae9742b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da7795f9-b01d-4068-b75b-8438dc4ff5a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2877d0f1-6051-4ce0-abde-303d4ae9742b",
                    "LayerId": "f1d3d6a0-8727-4e80-bf0c-724a2a412b8b"
                }
            ]
        },
        {
            "id": "83cc9e59-7b68-4bb4-9de2-472f6b447fe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a76b7ae-6e20-4d1d-b0b7-522d0bc9237c",
            "compositeImage": {
                "id": "8a260380-e40a-4c8a-8952-c0e418d55c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83cc9e59-7b68-4bb4-9de2-472f6b447fe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f8434ef-bdec-4459-83bc-b7bf7325497f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cc9e59-7b68-4bb4-9de2-472f6b447fe3",
                    "LayerId": "f1d3d6a0-8727-4e80-bf0c-724a2a412b8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1d3d6a0-8727-4e80-bf0c-724a2a412b8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a76b7ae-6e20-4d1d-b0b7-522d0bc9237c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}