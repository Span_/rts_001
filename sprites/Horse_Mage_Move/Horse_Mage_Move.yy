{
    "id": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Horse_Mage_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecd41949-7fda-4eee-8ba9-e85d3e02af72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "0ea3e16a-e584-406c-8d86-e2d2b47078e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecd41949-7fda-4eee-8ba9-e85d3e02af72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e340ed68-ef01-460e-acbe-90044d854831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd41949-7fda-4eee-8ba9-e85d3e02af72",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "25ce2c7d-b561-4385-9eef-179dc8dd1ffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd41949-7fda-4eee-8ba9-e85d3e02af72",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        },
        {
            "id": "743edcb5-b0c2-4e37-8319-a8889c8b36d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "db84fb35-7ddc-4113-bac4-6510536bf271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "743edcb5-b0c2-4e37-8319-a8889c8b36d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afafa055-0c67-4320-b07e-799b28f35041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "743edcb5-b0c2-4e37-8319-a8889c8b36d0",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "2ea4b871-a079-4331-bad6-2a80a90b6698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "743edcb5-b0c2-4e37-8319-a8889c8b36d0",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        },
        {
            "id": "cc8f25ae-9e57-473b-8c55-ea553c81cdf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "d5d9cca1-76ea-4871-9f2c-a306d2b58c53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc8f25ae-9e57-473b-8c55-ea553c81cdf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "201169c2-be4b-4a85-8d91-9523d52765ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc8f25ae-9e57-473b-8c55-ea553c81cdf1",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "489de170-25d8-40a5-8a4f-37e0de54311c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc8f25ae-9e57-473b-8c55-ea553c81cdf1",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        },
        {
            "id": "ead7fc62-91f0-40b0-b483-5393e74f1ceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "c9535829-f96d-46e3-be2e-345c53061b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ead7fc62-91f0-40b0-b483-5393e74f1ceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0556f88-8e20-48f5-b68d-9c6f96207e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ead7fc62-91f0-40b0-b483-5393e74f1ceb",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "e0c12b8a-2ca7-478f-b64d-21229077a52b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ead7fc62-91f0-40b0-b483-5393e74f1ceb",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        },
        {
            "id": "f2402400-b64c-4fe9-be9d-e17b8e34a9a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "a8695e62-b968-4a5b-9317-7e2f8c37a642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2402400-b64c-4fe9-be9d-e17b8e34a9a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0257b8da-e056-4218-9024-083ccc4bf23b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2402400-b64c-4fe9-be9d-e17b8e34a9a7",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "0994c6f7-434c-4b59-9bd1-3e7f3c1a889b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2402400-b64c-4fe9-be9d-e17b8e34a9a7",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        },
        {
            "id": "27aa36cc-8925-4013-a296-5f26160456ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "7f57058b-9a96-4d48-95a4-446b0e1c4f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27aa36cc-8925-4013-a296-5f26160456ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f6d8e0-97ff-4928-a9c0-fc7bcf96cc5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27aa36cc-8925-4013-a296-5f26160456ce",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "85a58038-fc32-4bf5-9283-6a8668b8d2e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27aa36cc-8925-4013-a296-5f26160456ce",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        },
        {
            "id": "c3820641-c3c2-4559-ad52-47cad2285bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "744994f7-7b01-4578-90e7-c170e60512f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3820641-c3c2-4559-ad52-47cad2285bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72dfbd4f-654b-48a6-887f-4adc81e4099e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3820641-c3c2-4559-ad52-47cad2285bee",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "83df50d7-51db-410b-a37e-ddc536144dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3820641-c3c2-4559-ad52-47cad2285bee",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        },
        {
            "id": "2210f6d4-69d6-4311-98df-bdd578a158ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "compositeImage": {
                "id": "2e544e90-c503-4fde-bf21-cf3a0151db3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2210f6d4-69d6-4311-98df-bdd578a158ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b642fc4-9bdc-4137-b550-ce73576b4839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2210f6d4-69d6-4311-98df-bdd578a158ce",
                    "LayerId": "f1709e72-33ad-40f9-9a79-37e8cc583a8e"
                },
                {
                    "id": "5670a489-4c2c-4a74-aee1-7a4475404a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2210f6d4-69d6-4311-98df-bdd578a158ce",
                    "LayerId": "2c48f2f5-9f95-478e-83f7-a00bdaa52658"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f1709e72-33ad-40f9-9a79-37e8cc583a8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2c48f2f5-9f95-478e-83f7-a00bdaa52658",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aa7ef18-1815-4e57-a7a9-a4083beb2334",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 18,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}