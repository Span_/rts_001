{
    "id": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Entrenchment_Attack_L",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83b3f03b-0159-4201-8488-cd87b2da6a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "df0f0165-538c-4358-ae47-5a1b402607b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83b3f03b-0159-4201-8488-cd87b2da6a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d92db42-e154-4b2f-82b1-af95aea509da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83b3f03b-0159-4201-8488-cd87b2da6a38",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        },
        {
            "id": "ced0d45c-d30b-4fcf-9af4-91465ae8a663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "c5315ecc-e291-4224-b366-f1f38faf2307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced0d45c-d30b-4fcf-9af4-91465ae8a663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ef54b5-45be-4e6b-b522-fcacc997adb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced0d45c-d30b-4fcf-9af4-91465ae8a663",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        },
        {
            "id": "8b0c0dba-f731-4a69-b8fb-c4d6f5a27c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "bdd8b49f-1b84-4f55-a8f5-3515812bb4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0c0dba-f731-4a69-b8fb-c4d6f5a27c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299c88f5-14c8-4a2f-8b54-bab66aa4852e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0c0dba-f731-4a69-b8fb-c4d6f5a27c66",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        },
        {
            "id": "7ab4b4a4-95ec-454b-98c9-f9818951f258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "89d9d4f2-0b96-4621-a18a-bbb1dd73a5a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab4b4a4-95ec-454b-98c9-f9818951f258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4385ea56-9267-4ac9-baae-de6c1e100487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab4b4a4-95ec-454b-98c9-f9818951f258",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        },
        {
            "id": "45f3f036-c6f2-4100-b63d-48754947801b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "4122d106-4d47-4cd8-b8ea-3d6f1714c520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45f3f036-c6f2-4100-b63d-48754947801b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e3569d-72ab-4c5b-85bd-83c69baa2490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45f3f036-c6f2-4100-b63d-48754947801b",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        },
        {
            "id": "1bdff07f-5045-4436-b854-724e02dd2516",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "b95b1223-79bf-4800-8b63-39178523f712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bdff07f-5045-4436-b854-724e02dd2516",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7d6b0a3-229c-44e4-a4f4-5c41319975d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bdff07f-5045-4436-b854-724e02dd2516",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        },
        {
            "id": "12b64cf7-2feb-43fd-9555-85f3a708fe20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "a0eb29bb-feb0-4d86-aa84-9ba8e4e72e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12b64cf7-2feb-43fd-9555-85f3a708fe20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e073b0c-121d-463f-8d40-92c7496bb910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12b64cf7-2feb-43fd-9555-85f3a708fe20",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        },
        {
            "id": "473149cd-d383-425f-8d0d-9229fc928165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "compositeImage": {
                "id": "042e7cf4-4347-4e7b-a9db-5cbf4d6728b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473149cd-d383-425f-8d0d-9229fc928165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80bef739-1389-4f3b-bc83-177f4cd176c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473149cd-d383-425f-8d0d-9229fc928165",
                    "LayerId": "37490e28-761d-430c-b74a-5f7b63b9b746"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "37490e28-761d-430c-b74a-5f7b63b9b746",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6faa4fbd-e1ec-426a-aa75-a174d15f8cba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}