{
    "id": "69071c74-0102-4e68-b862-c08a9b53647b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_Soldier_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b0b8ffc-54a9-4d73-a319-f9e0b23487a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69071c74-0102-4e68-b862-c08a9b53647b",
            "compositeImage": {
                "id": "64df08f8-9ee7-41f0-88eb-81e138c8530d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b0b8ffc-54a9-4d73-a319-f9e0b23487a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b81dc63e-3ad2-4292-9a7b-e7d802ee5532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b0b8ffc-54a9-4d73-a319-f9e0b23487a3",
                    "LayerId": "4d9fc642-cb68-4c07-a21d-b8c6846d0876"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4d9fc642-cb68-4c07-a21d-b8c6846d0876",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69071c74-0102-4e68-b862-c08a9b53647b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}