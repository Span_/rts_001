{
    "id": "040b298e-86be-474b-91e2-2b0c55490f00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Horse_Mage_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71a1ddda-2024-44e6-99e3-d90f29c3a993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b298e-86be-474b-91e2-2b0c55490f00",
            "compositeImage": {
                "id": "04f70227-e93f-435d-a6ce-f215edb027dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a1ddda-2024-44e6-99e3-d90f29c3a993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5ddce9-dce3-4f7e-aaf5-209d4c579ff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a1ddda-2024-44e6-99e3-d90f29c3a993",
                    "LayerId": "0d3625b3-2efb-4654-b78c-cfe879db3b6c"
                },
                {
                    "id": "1b7684db-5855-41b6-b0c0-0e5146af6886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a1ddda-2024-44e6-99e3-d90f29c3a993",
                    "LayerId": "3bbd1c58-f61f-4d0c-8cbb-cef2a1f5e594"
                }
            ]
        },
        {
            "id": "a6557008-a8b7-4141-9bc5-75594610fd46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b298e-86be-474b-91e2-2b0c55490f00",
            "compositeImage": {
                "id": "9140e198-e5d8-42cd-982b-1f1613c82434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6557008-a8b7-4141-9bc5-75594610fd46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c566b1-9b05-4ec6-98d2-190f2185d5bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6557008-a8b7-4141-9bc5-75594610fd46",
                    "LayerId": "0d3625b3-2efb-4654-b78c-cfe879db3b6c"
                },
                {
                    "id": "9bd7aacb-ed8b-44dc-b4bb-5d1b571dba31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6557008-a8b7-4141-9bc5-75594610fd46",
                    "LayerId": "3bbd1c58-f61f-4d0c-8cbb-cef2a1f5e594"
                }
            ]
        },
        {
            "id": "82478635-6fb7-4955-95c9-94596e2fc48c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b298e-86be-474b-91e2-2b0c55490f00",
            "compositeImage": {
                "id": "df5dd6a2-e06e-456a-8302-108be6ad9307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82478635-6fb7-4955-95c9-94596e2fc48c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80ef7a66-6891-4ff4-a55d-92e8a5c3c749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82478635-6fb7-4955-95c9-94596e2fc48c",
                    "LayerId": "3bbd1c58-f61f-4d0c-8cbb-cef2a1f5e594"
                },
                {
                    "id": "945476ba-2d5e-475b-879f-41d49af5ccee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82478635-6fb7-4955-95c9-94596e2fc48c",
                    "LayerId": "0d3625b3-2efb-4654-b78c-cfe879db3b6c"
                }
            ]
        },
        {
            "id": "f826d166-05c2-4945-bc81-c665340f4a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b298e-86be-474b-91e2-2b0c55490f00",
            "compositeImage": {
                "id": "f979688b-4393-4830-bfff-a4809ddc7639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f826d166-05c2-4945-bc81-c665340f4a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e17638-ac7e-4e84-9d90-3ea9611e6180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f826d166-05c2-4945-bc81-c665340f4a1e",
                    "LayerId": "3bbd1c58-f61f-4d0c-8cbb-cef2a1f5e594"
                },
                {
                    "id": "23fc24b1-e6e7-46d1-b6fd-080b1f435339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f826d166-05c2-4945-bc81-c665340f4a1e",
                    "LayerId": "0d3625b3-2efb-4654-b78c-cfe879db3b6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3bbd1c58-f61f-4d0c-8cbb-cef2a1f5e594",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "040b298e-86be-474b-91e2-2b0c55490f00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0d3625b3-2efb-4654-b78c-cfe879db3b6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "040b298e-86be-474b-91e2-2b0c55490f00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}