{
    "id": "1bf5158e-e1af-4ad7-b221-d4772e1e68e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Monster_Doggo_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d77dfcfa-6f4f-49e8-ac8a-246576679ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bf5158e-e1af-4ad7-b221-d4772e1e68e9",
            "compositeImage": {
                "id": "1d5fa1bc-1ca7-4262-b937-59eb7433e009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d77dfcfa-6f4f-49e8-ac8a-246576679ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e991dab4-5c25-4dad-8555-de8fa21216df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d77dfcfa-6f4f-49e8-ac8a-246576679ee6",
                    "LayerId": "518a7590-148a-4c3b-b9fb-8e5f6accbea3"
                }
            ]
        },
        {
            "id": "e2d4559e-4841-4026-9831-531186996e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bf5158e-e1af-4ad7-b221-d4772e1e68e9",
            "compositeImage": {
                "id": "7d2ca118-5aea-48d9-86fc-8eb80ecde165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d4559e-4841-4026-9831-531186996e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79fc5e50-7683-4090-a89c-c583ff376be4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d4559e-4841-4026-9831-531186996e6b",
                    "LayerId": "518a7590-148a-4c3b-b9fb-8e5f6accbea3"
                }
            ]
        },
        {
            "id": "906e2aa4-80c8-477b-9e70-4c9bf45da7c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bf5158e-e1af-4ad7-b221-d4772e1e68e9",
            "compositeImage": {
                "id": "913fbefa-fd1c-471d-9516-8daa6a87274f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "906e2aa4-80c8-477b-9e70-4c9bf45da7c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d33ec0-3419-4644-9f28-621976842d77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "906e2aa4-80c8-477b-9e70-4c9bf45da7c5",
                    "LayerId": "518a7590-148a-4c3b-b9fb-8e5f6accbea3"
                }
            ]
        },
        {
            "id": "2ecbec6e-1303-414e-9eee-eec535b07d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bf5158e-e1af-4ad7-b221-d4772e1e68e9",
            "compositeImage": {
                "id": "a4a04278-fa82-4907-adb5-debe9f26e40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ecbec6e-1303-414e-9eee-eec535b07d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13bf1444-946b-45e4-80ff-34738bb06583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ecbec6e-1303-414e-9eee-eec535b07d3b",
                    "LayerId": "518a7590-148a-4c3b-b9fb-8e5f6accbea3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "518a7590-148a-4c3b-b9fb-8e5f6accbea3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bf5158e-e1af-4ad7-b221-d4772e1e68e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}