{
    "id": "d0a99f56-42de-497c-a3fb-8c7312a33ceb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_Worker_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "712a772d-de8e-44b4-a310-5a621800814d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a99f56-42de-497c-a3fb-8c7312a33ceb",
            "compositeImage": {
                "id": "d7358167-0a53-4ad5-86ee-b14cf922279b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "712a772d-de8e-44b4-a310-5a621800814d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7da1e860-0c69-47f6-8289-3fad56bc0641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712a772d-de8e-44b4-a310-5a621800814d",
                    "LayerId": "9bac418e-852a-4c99-874f-d5da31c84eca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9bac418e-852a-4c99-874f-d5da31c84eca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0a99f56-42de-497c-a3fb-8c7312a33ceb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}