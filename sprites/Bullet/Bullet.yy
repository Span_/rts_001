{
    "id": "feb188bc-9b3a-42be-b4ee-27e473a45a97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 4,
    "bbox_right": 5,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9486b5a-396c-429a-978c-4946f377615c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "feb188bc-9b3a-42be-b4ee-27e473a45a97",
            "compositeImage": {
                "id": "55e7d65f-4c26-486b-8545-2906391ef919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9486b5a-396c-429a-978c-4946f377615c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "070c5232-5ac5-4809-980b-ba980644f99a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9486b5a-396c-429a-978c-4946f377615c",
                    "LayerId": "6f788c3e-2e51-43af-9141-1c07a00212e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "6f788c3e-2e51-43af-9141-1c07a00212e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "feb188bc-9b3a-42be-b4ee-27e473a45a97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 5,
    "yorig": 3
}