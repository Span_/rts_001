{
    "id": "3c5fbd7b-edfc-4c25-87a0-075ab2bf07c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Steel_Plate_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90cde5af-4993-4178-8a81-527c0b74d453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c5fbd7b-edfc-4c25-87a0-075ab2bf07c2",
            "compositeImage": {
                "id": "e01c91d6-f9bf-448a-b2ed-f59ceb212da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90cde5af-4993-4178-8a81-527c0b74d453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b24c172-b19b-4e41-a83e-137f488c08fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90cde5af-4993-4178-8a81-527c0b74d453",
                    "LayerId": "671618e3-e060-4781-84d0-4316d125c37b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "671618e3-e060-4781-84d0-4316d125c37b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c5fbd7b-edfc-4c25-87a0-075ab2bf07c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}