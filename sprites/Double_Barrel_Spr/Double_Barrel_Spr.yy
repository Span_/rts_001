{
    "id": "00c62864-cf74-4590-b785-5a6eac068887",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Double_Barrel_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adb3471c-24ca-48fd-8a22-9c4da2bcfac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c62864-cf74-4590-b785-5a6eac068887",
            "compositeImage": {
                "id": "571a1809-6594-4a19-b6de-65ba28903278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adb3471c-24ca-48fd-8a22-9c4da2bcfac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b75ad728-9f84-4b6b-868a-d907b39850f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb3471c-24ca-48fd-8a22-9c4da2bcfac6",
                    "LayerId": "c3ec90fa-0f2c-4b27-8fb2-e752640f50d9"
                },
                {
                    "id": "352b7d56-5e36-48f7-adab-212987ee3105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb3471c-24ca-48fd-8a22-9c4da2bcfac6",
                    "LayerId": "7d85a7c8-29c2-4ba9-ac8f-0caa9c01cea4"
                }
            ]
        },
        {
            "id": "1753415f-04d0-456b-80a5-9d80c20dfc3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c62864-cf74-4590-b785-5a6eac068887",
            "compositeImage": {
                "id": "7665f27b-be90-48d6-88c2-161855a8523f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1753415f-04d0-456b-80a5-9d80c20dfc3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac63d89-01a7-4113-bee4-a71f8d69977f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1753415f-04d0-456b-80a5-9d80c20dfc3e",
                    "LayerId": "c3ec90fa-0f2c-4b27-8fb2-e752640f50d9"
                },
                {
                    "id": "3f0a10dc-e1fc-4d49-b3ae-62c7a017ec2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1753415f-04d0-456b-80a5-9d80c20dfc3e",
                    "LayerId": "7d85a7c8-29c2-4ba9-ac8f-0caa9c01cea4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c3ec90fa-0f2c-4b27-8fb2-e752640f50d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00c62864-cf74-4590-b785-5a6eac068887",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7d85a7c8-29c2-4ba9-ac8f-0caa9c01cea4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00c62864-cf74-4590-b785-5a6eac068887",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}