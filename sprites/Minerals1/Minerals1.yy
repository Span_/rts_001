{
    "id": "67c24776-6b60-488e-8578-f2a8715373ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Minerals1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "132a6b02-af42-4544-866c-033166f8dd9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67c24776-6b60-488e-8578-f2a8715373ec",
            "compositeImage": {
                "id": "cc869228-7c52-4a2a-a27d-0a0d7b81fff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132a6b02-af42-4544-866c-033166f8dd9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dd0fa56-1f9e-44e6-af54-94778b9201b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132a6b02-af42-4544-866c-033166f8dd9c",
                    "LayerId": "3196cee8-7f5d-4b7e-a692-6466ca3127c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3196cee8-7f5d-4b7e-a692-6466ca3127c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67c24776-6b60-488e-8578-f2a8715373ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}