{
    "id": "f37db43b-2888-4e16-89ae-e747b20f3327",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_House_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7d4fa11-18d1-46d3-9c48-572dcdf8c9e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f37db43b-2888-4e16-89ae-e747b20f3327",
            "compositeImage": {
                "id": "b5dbc512-9ab2-4156-af6f-85d0c256e7bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7d4fa11-18d1-46d3-9c48-572dcdf8c9e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad67c2ab-baca-41e2-9403-759754b8b7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7d4fa11-18d1-46d3-9c48-572dcdf8c9e6",
                    "LayerId": "6f530059-6a2b-4c51-b2d8-3551a0968039"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6f530059-6a2b-4c51-b2d8-3551a0968039",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f37db43b-2888-4e16-89ae-e747b20f3327",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}