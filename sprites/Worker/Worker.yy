{
    "id": "ac8a36eb-b76d-4d91-9699-78679919bcce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Worker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b15006c8-b18b-4280-94b4-d6a032f07539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac8a36eb-b76d-4d91-9699-78679919bcce",
            "compositeImage": {
                "id": "c1ef07af-a412-45d6-bb4b-1366639d2528",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b15006c8-b18b-4280-94b4-d6a032f07539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9af68d8-0229-453f-9190-ff953280026b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b15006c8-b18b-4280-94b4-d6a032f07539",
                    "LayerId": "56f9c289-5c46-4a6f-8eae-39ee38f5b4af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "56f9c289-5c46-4a6f-8eae-39ee38f5b4af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac8a36eb-b76d-4d91-9699-78679919bcce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 6
}