{
    "id": "38ba17c3-940b-47a6-9b5a-19e1ccdae30c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Med_Kit_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9a521eb-e735-407b-be96-ee4a31a1f95b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38ba17c3-940b-47a6-9b5a-19e1ccdae30c",
            "compositeImage": {
                "id": "1fd8828e-3952-4086-b0f7-d90f7338b2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9a521eb-e735-407b-be96-ee4a31a1f95b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff513698-f8a6-40a4-b8fa-1c592768f6cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9a521eb-e735-407b-be96-ee4a31a1f95b",
                    "LayerId": "08adbac7-57ff-4a73-900e-f221ff1dc7cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "08adbac7-57ff-4a73-900e-f221ff1dc7cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38ba17c3-940b-47a6-9b5a-19e1ccdae30c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}