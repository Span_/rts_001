{
    "id": "83d58843-47ae-44d2-a1fb-43db37933084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Build_Units_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d65657b-d3ff-49aa-bb8c-78d544baa2bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d58843-47ae-44d2-a1fb-43db37933084",
            "compositeImage": {
                "id": "df56104d-7fc0-4d61-b6e2-d74b0cb56efe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d65657b-d3ff-49aa-bb8c-78d544baa2bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86baefac-592b-4d28-a800-79456bebbd8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d65657b-d3ff-49aa-bb8c-78d544baa2bf",
                    "LayerId": "36a872a9-f924-4f48-916c-e11a62600870"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "36a872a9-f924-4f48-916c-e11a62600870",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83d58843-47ae-44d2-a1fb-43db37933084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}