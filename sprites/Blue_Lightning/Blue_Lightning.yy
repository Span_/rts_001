{
    "id": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Blue_Lightning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed5cd485-48a1-4c9f-a219-4352e559ea82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
            "compositeImage": {
                "id": "f6352524-2893-46b3-ad8d-05439df0e985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed5cd485-48a1-4c9f-a219-4352e559ea82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd9ae30-dbc4-4e26-b5ba-fec0e6d9ae9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed5cd485-48a1-4c9f-a219-4352e559ea82",
                    "LayerId": "4ba518dd-4b89-4916-ade2-f1dc7d33022e"
                }
            ]
        },
        {
            "id": "84b5f97e-194b-44ac-ac2b-b47bbfb721d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
            "compositeImage": {
                "id": "2b5966b5-260e-47f7-90c2-d86cbe6b7c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b5f97e-194b-44ac-ac2b-b47bbfb721d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b65a1b03-d81f-49fb-b2dc-4b6f80f0dd2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b5f97e-194b-44ac-ac2b-b47bbfb721d4",
                    "LayerId": "4ba518dd-4b89-4916-ade2-f1dc7d33022e"
                }
            ]
        },
        {
            "id": "59ebf920-4a07-462d-be93-59c8c8a6ec80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
            "compositeImage": {
                "id": "aea13b78-5a88-4bc8-ac58-f00dceb44cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59ebf920-4a07-462d-be93-59c8c8a6ec80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7aac437-eada-494d-a469-6b7c8df3f750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59ebf920-4a07-462d-be93-59c8c8a6ec80",
                    "LayerId": "4ba518dd-4b89-4916-ade2-f1dc7d33022e"
                }
            ]
        },
        {
            "id": "dea7d108-daed-45b1-9be1-dd035aeb73f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
            "compositeImage": {
                "id": "2aaa5acd-fd0a-46b1-a7c3-2570ebd14d36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea7d108-daed-45b1-9be1-dd035aeb73f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ef99281-855d-47bb-be83-8aff369b4621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea7d108-daed-45b1-9be1-dd035aeb73f7",
                    "LayerId": "4ba518dd-4b89-4916-ade2-f1dc7d33022e"
                }
            ]
        },
        {
            "id": "05ee8e3e-a574-4d2c-900f-c04e757849d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
            "compositeImage": {
                "id": "4b020712-e68b-4426-bdb6-4b6764759c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05ee8e3e-a574-4d2c-900f-c04e757849d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b4e6cbb-0946-4573-94a2-7cf2544ce4f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05ee8e3e-a574-4d2c-900f-c04e757849d0",
                    "LayerId": "4ba518dd-4b89-4916-ade2-f1dc7d33022e"
                }
            ]
        },
        {
            "id": "232be7ed-0a4d-48fd-8b5f-af2240bf2188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
            "compositeImage": {
                "id": "8013e112-76b0-47f5-b299-fcd3c9a35f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "232be7ed-0a4d-48fd-8b5f-af2240bf2188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87570ef-bec6-4cc1-936c-65070d6ac16c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "232be7ed-0a4d-48fd-8b5f-af2240bf2188",
                    "LayerId": "4ba518dd-4b89-4916-ade2-f1dc7d33022e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4ba518dd-4b89-4916-ade2-f1dc7d33022e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9d1cff6-f1e3-4a32-94df-404a5039218c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}