{
    "id": "6d411f1d-f5b7-4ba1-b39d-9dcd959505e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Back_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "258553d8-4e61-4cd6-955d-61fd28951453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d411f1d-f5b7-4ba1-b39d-9dcd959505e9",
            "compositeImage": {
                "id": "7cbc3f95-a33a-44c8-8c8a-c2c42f1f008c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "258553d8-4e61-4cd6-955d-61fd28951453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ce5b9c-c158-4376-9882-21a2baa7dec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "258553d8-4e61-4cd6-955d-61fd28951453",
                    "LayerId": "12ac0f17-ca03-4fe3-8fab-38f662e972fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "12ac0f17-ca03-4fe3-8fab-38f662e972fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d411f1d-f5b7-4ba1-b39d-9dcd959505e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}