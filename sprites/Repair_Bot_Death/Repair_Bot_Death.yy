{
    "id": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Repair_Bot_Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63f159f7-71c0-498a-868c-82cbed4e745d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
            "compositeImage": {
                "id": "e5be1ca5-5c66-45d0-9d42-e87ed51e9ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63f159f7-71c0-498a-868c-82cbed4e745d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d50b56-8bf5-44c4-8fe5-2ba9e95f08e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63f159f7-71c0-498a-868c-82cbed4e745d",
                    "LayerId": "63d77c6f-af22-4779-bd4d-181865df7abd"
                }
            ]
        },
        {
            "id": "701eaf37-7693-4d60-a042-e297992847fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
            "compositeImage": {
                "id": "0f55740c-7749-4c1c-887a-654a10a4d3a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "701eaf37-7693-4d60-a042-e297992847fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbdc5aa2-57f6-4a21-8d11-cfd699c8b27c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "701eaf37-7693-4d60-a042-e297992847fd",
                    "LayerId": "63d77c6f-af22-4779-bd4d-181865df7abd"
                }
            ]
        },
        {
            "id": "17e61a91-e233-4d2a-9f4f-fe63f6a12e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
            "compositeImage": {
                "id": "a6e50c31-319e-45db-8920-1dc8778e718e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e61a91-e233-4d2a-9f4f-fe63f6a12e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c579f5e-6ce0-4de0-becb-8201bb984aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e61a91-e233-4d2a-9f4f-fe63f6a12e67",
                    "LayerId": "63d77c6f-af22-4779-bd4d-181865df7abd"
                }
            ]
        },
        {
            "id": "e8ade0ba-573e-4df6-8ad5-c402d99effc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
            "compositeImage": {
                "id": "90f8f0cc-8656-4482-a166-06f191b7badf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ade0ba-573e-4df6-8ad5-c402d99effc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9226a8c9-ed6f-42d5-946b-61a98d77df58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ade0ba-573e-4df6-8ad5-c402d99effc5",
                    "LayerId": "63d77c6f-af22-4779-bd4d-181865df7abd"
                }
            ]
        },
        {
            "id": "3c7d222a-60f4-4580-af4b-16ccc028ee5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
            "compositeImage": {
                "id": "e703f06c-a327-4009-9b07-54152d43750c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7d222a-60f4-4580-af4b-16ccc028ee5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfae7925-b6b8-4431-b064-3e84d2e2e508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7d222a-60f4-4580-af4b-16ccc028ee5f",
                    "LayerId": "63d77c6f-af22-4779-bd4d-181865df7abd"
                }
            ]
        },
        {
            "id": "25e0b489-b49c-4e4e-9eac-9d7438dacde4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
            "compositeImage": {
                "id": "7602579a-e0ce-4937-8685-4a624be54372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e0b489-b49c-4e4e-9eac-9d7438dacde4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "582c2b2c-f0ae-4b6f-8cf5-7481f37f3f64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e0b489-b49c-4e4e-9eac-9d7438dacde4",
                    "LayerId": "63d77c6f-af22-4779-bd4d-181865df7abd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "63d77c6f-af22-4779-bd4d-181865df7abd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "608eca51-b6d4-4cdd-97de-9f9c60c4be60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}