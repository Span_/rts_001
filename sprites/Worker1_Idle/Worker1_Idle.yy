{
    "id": "1646189e-00c4-4212-9696-5ccd180120f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Worker1_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbfa3b48-b7e9-4357-a39e-87aa2e97d027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "66c9a56f-2c9b-4376-b5a1-c9306308f8c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbfa3b48-b7e9-4357-a39e-87aa2e97d027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8acbbfe-764b-4b56-a2f1-ffa8d55a8156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbfa3b48-b7e9-4357-a39e-87aa2e97d027",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "59d9f5a6-6bf6-4c63-80df-ad378a080a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "7b7b51bd-0d07-4354-bd16-f4e7a3acda94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59d9f5a6-6bf6-4c63-80df-ad378a080a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d440bb68-10ae-4224-84c8-00fe6955bed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59d9f5a6-6bf6-4c63-80df-ad378a080a5b",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "db1dc0e2-8df0-41a4-bfe2-85239dc73f5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "29406f47-6b12-4d68-8f55-f0662b8f2064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1dc0e2-8df0-41a4-bfe2-85239dc73f5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "128f3bc0-b6f0-4d7f-8cfa-938509a7ae30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1dc0e2-8df0-41a4-bfe2-85239dc73f5c",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "6fa707bb-ad00-4cd4-af1f-14219457720d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "1e139696-f4d4-4ac1-886f-3110e6d03dca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa707bb-ad00-4cd4-af1f-14219457720d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5742ff44-a80a-41e1-965f-3c69870ea5d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa707bb-ad00-4cd4-af1f-14219457720d",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "70cf347f-6303-4865-acf3-e09fe8e9af23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "e77ffc89-5777-416b-98bb-8cfa14b6b081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70cf347f-6303-4865-acf3-e09fe8e9af23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2446d825-8c78-4339-8498-f03eae2a379a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70cf347f-6303-4865-acf3-e09fe8e9af23",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "53ce7d54-d7aa-4a36-9698-aab098835e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "c714217d-6ab0-420a-b007-cc7ea66c870d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ce7d54-d7aa-4a36-9698-aab098835e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a04712dd-212d-4465-b754-5266a34e9165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ce7d54-d7aa-4a36-9698-aab098835e06",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "ab421343-24eb-4ba9-9d32-9847e3c87825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "4fc89538-ebd5-4276-83c6-046029cd5e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab421343-24eb-4ba9-9d32-9847e3c87825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef0f2feb-5de4-4b7e-bd55-b51518f60386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab421343-24eb-4ba9-9d32-9847e3c87825",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "9ef0b8ad-9990-45c8-8141-63a18319d71f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "af379875-b9f7-4403-9057-046fe17971c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef0b8ad-9990-45c8-8141-63a18319d71f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eae01dd-3a8f-4c84-adf1-c403cf134975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef0b8ad-9990-45c8-8141-63a18319d71f",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "045f5b20-011b-40d2-874d-a216947ac79c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "e4bb0b01-cca1-4d90-83cd-85aec8cc09b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "045f5b20-011b-40d2-874d-a216947ac79c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d727c73d-ddf4-4e0c-8fc9-bfed0ba3e4c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "045f5b20-011b-40d2-874d-a216947ac79c",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        },
        {
            "id": "fb58ff86-02a4-4973-aa09-6feb5ac0298c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "compositeImage": {
                "id": "4c616ab5-bd21-40a5-a564-bcf29205b154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb58ff86-02a4-4973-aa09-6feb5ac0298c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8023b1eb-72f5-4943-94cf-82df3e93da52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb58ff86-02a4-4973-aa09-6feb5ac0298c",
                    "LayerId": "85c5be99-570c-44da-ab25-b9e26e20b9cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "85c5be99-570c-44da-ab25-b9e26e20b9cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1646189e-00c4-4212-9696-5ccd180120f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}