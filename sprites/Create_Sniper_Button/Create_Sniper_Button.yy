{
    "id": "426c8039-e3e1-4549-855a-7d7e040bdd70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Create_Sniper_Button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f82af20c-995e-4de9-8d3c-fc35d9deccb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "426c8039-e3e1-4549-855a-7d7e040bdd70",
            "compositeImage": {
                "id": "212b7435-0e46-4dbe-9e80-c3cb5a2872cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f82af20c-995e-4de9-8d3c-fc35d9deccb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ae01866-f573-47a4-9d07-bb01445cc60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f82af20c-995e-4de9-8d3c-fc35d9deccb4",
                    "LayerId": "f49a49f2-cae8-47e8-b8ee-00e85908d3fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f49a49f2-cae8-47e8-b8ee-00e85908d3fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "426c8039-e3e1-4549-855a-7d7e040bdd70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 32
}