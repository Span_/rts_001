{
    "id": "c2478461-534a-469e-9a36-d582e71b9294",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Entrenchment_Attack_R",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7586ee5a-ad80-442a-a996-feae4451235b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "6488b72f-abee-4f20-ba1c-d9136f5200e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7586ee5a-ad80-442a-a996-feae4451235b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8742b7fd-cfc3-49cb-9e91-484fac082baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7586ee5a-ad80-442a-a996-feae4451235b",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        },
        {
            "id": "62a707f4-a204-425a-9ce7-32ede197d1d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "be064a0c-a520-4598-9650-f7089652a3d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a707f4-a204-425a-9ce7-32ede197d1d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b74e9fc9-78cf-46e6-844a-5524eea72dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a707f4-a204-425a-9ce7-32ede197d1d4",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        },
        {
            "id": "5e69d12d-2be1-41b6-9a0f-dcc7043a02f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "51687a65-4c99-4209-ac54-ec17aa9be86a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e69d12d-2be1-41b6-9a0f-dcc7043a02f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a814a5c-b986-4d2b-894a-ac7dea48bf42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e69d12d-2be1-41b6-9a0f-dcc7043a02f3",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        },
        {
            "id": "35d3b6fc-c369-4a52-9c75-76334ab8e28c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "d9b25e56-b8cd-498a-abaa-6e93c954bf0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d3b6fc-c369-4a52-9c75-76334ab8e28c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623d4a63-0e4f-458a-adf7-881cdb4510f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d3b6fc-c369-4a52-9c75-76334ab8e28c",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        },
        {
            "id": "b8b4b696-0892-4d1c-8887-cfd266e367de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "cebd1550-726c-4c04-ae04-5ea524b1944d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8b4b696-0892-4d1c-8887-cfd266e367de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "543049ed-8970-41f9-9899-7558dccee4fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b4b696-0892-4d1c-8887-cfd266e367de",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        },
        {
            "id": "5f7ac08b-6c3c-41f7-9ecb-9c9890f9c1bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "47f9dff8-f4ea-4724-b26f-573886425ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7ac08b-6c3c-41f7-9ecb-9c9890f9c1bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb00bd6-6ce4-448d-903d-34efcaea97e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7ac08b-6c3c-41f7-9ecb-9c9890f9c1bd",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        },
        {
            "id": "14720fc5-5027-442c-95b8-3fa36903daec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "80968f87-7e8a-49c2-8ee6-50e6e0ff2526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14720fc5-5027-442c-95b8-3fa36903daec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c31357ba-5bd9-48cc-ad20-0547da9420d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14720fc5-5027-442c-95b8-3fa36903daec",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        },
        {
            "id": "6b52a5c2-d0eb-4be2-9a7a-d006cc030a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "compositeImage": {
                "id": "a71c7973-5552-4479-950e-991ac8ee184e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b52a5c2-d0eb-4be2-9a7a-d006cc030a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9dabaa-d7cb-499a-bcc2-9e0453f63196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b52a5c2-d0eb-4be2-9a7a-d006cc030a39",
                    "LayerId": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eb7cb3a8-47a2-4ba3-9df3-884abfc960d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2478461-534a-469e-9a36-d582e71b9294",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}