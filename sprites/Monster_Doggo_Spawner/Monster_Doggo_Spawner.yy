{
    "id": "52028736-4df1-4c30-871c-5d5f4a3198a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Monster_Doggo_Spawner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 6,
    "bbox_right": 59,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5a54db9-3869-4c72-bbda-92fae4e4a7fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52028736-4df1-4c30-871c-5d5f4a3198a7",
            "compositeImage": {
                "id": "2ee01e77-20bf-4905-bd80-a7a680984dac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5a54db9-3869-4c72-bbda-92fae4e4a7fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db782118-1344-42a2-8789-68794cecd224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a54db9-3869-4c72-bbda-92fae4e4a7fe",
                    "LayerId": "898139f9-70ac-4ba0-9946-eff15daca573"
                }
            ]
        },
        {
            "id": "f6e7c8ac-f45e-4d90-8763-2a1d17ad4bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52028736-4df1-4c30-871c-5d5f4a3198a7",
            "compositeImage": {
                "id": "9658851d-97a2-4744-bd64-cd77f10f7b1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e7c8ac-f45e-4d90-8763-2a1d17ad4bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38d9e46e-6115-4e65-bff1-cf2d705af96d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e7c8ac-f45e-4d90-8763-2a1d17ad4bd6",
                    "LayerId": "898139f9-70ac-4ba0-9946-eff15daca573"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "898139f9-70ac-4ba0-9946-eff15daca573",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52028736-4df1-4c30-871c-5d5f4a3198a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}