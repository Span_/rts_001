{
    "id": "9b6ac377-9210-4587-896b-3cd9a7b37a21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Soldier1_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4941be04-9e42-449f-a234-593796803f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b6ac377-9210-4587-896b-3cd9a7b37a21",
            "compositeImage": {
                "id": "eb678c3a-5ee9-4085-8ac8-2792c4249220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4941be04-9e42-449f-a234-593796803f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b842d9-7230-4650-9a4c-0123a396e188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4941be04-9e42-449f-a234-593796803f69",
                    "LayerId": "2444f378-e778-4c19-a948-8da0c7fc7c25"
                }
            ]
        },
        {
            "id": "878c4d3a-bd98-47ea-bae9-5eddb08d1d15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b6ac377-9210-4587-896b-3cd9a7b37a21",
            "compositeImage": {
                "id": "d594e7fd-4ef6-4f93-8e4e-1f584d26cf6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "878c4d3a-bd98-47ea-bae9-5eddb08d1d15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09bfebd-8516-4263-a43b-47e5125cc7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "878c4d3a-bd98-47ea-bae9-5eddb08d1d15",
                    "LayerId": "2444f378-e778-4c19-a948-8da0c7fc7c25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2444f378-e778-4c19-a948-8da0c7fc7c25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b6ac377-9210-4587-896b-3cd9a7b37a21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}