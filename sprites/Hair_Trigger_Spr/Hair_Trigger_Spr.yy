{
    "id": "a4d7d925-a5e3-4aa6-9a89-56d4f22d3d64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Hair_Trigger_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "169e66ac-1d67-4f58-9b3a-f05d0944d7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4d7d925-a5e3-4aa6-9a89-56d4f22d3d64",
            "compositeImage": {
                "id": "77aad824-f5e6-4e25-bb51-7e6696693a01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "169e66ac-1d67-4f58-9b3a-f05d0944d7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78b342e9-1e27-42e1-b9cb-b9c1f4219f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "169e66ac-1d67-4f58-9b3a-f05d0944d7b4",
                    "LayerId": "be02d814-a459-4c27-b737-725133d1ca17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "be02d814-a459-4c27-b737-725133d1ca17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4d7d925-a5e3-4aa6-9a89-56d4f22d3d64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}