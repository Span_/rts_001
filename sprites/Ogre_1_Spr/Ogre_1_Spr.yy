{
    "id": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ogre_1_Spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71b7521a-0d07-46ca-887e-3701f0337bbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "1d5ac456-95a7-4cb3-9e0b-94718640e6f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b7521a-0d07-46ca-887e-3701f0337bbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7655ad0b-2239-4116-b53e-6f4d69ea3dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b7521a-0d07-46ca-887e-3701f0337bbf",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "3705875e-2751-423a-910f-6212bd29c245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "029a0625-aa68-4a6c-b54d-f9a88f355185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3705875e-2751-423a-910f-6212bd29c245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c7e07bf-ec7c-41ef-9837-7a37691e6670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3705875e-2751-423a-910f-6212bd29c245",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "f6424b92-b6b2-462a-8a02-4387d4fa0b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "a0bf6d85-2959-413f-9173-899cd5673799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6424b92-b6b2-462a-8a02-4387d4fa0b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f869227f-2b3b-48a0-947a-5387727d0f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6424b92-b6b2-462a-8a02-4387d4fa0b0d",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "a0769585-0ec3-4732-8cbf-fdc994c68bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "826264b9-d29e-41da-84a0-9b24a7d7f7be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0769585-0ec3-4732-8cbf-fdc994c68bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c32d95-5bbd-453b-825b-8ff42c145a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0769585-0ec3-4732-8cbf-fdc994c68bc7",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "6cf3e59f-3446-4a93-9af6-d2dd3cb8ff26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "43de3782-4f0d-4aee-bc08-4bfeab48fffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf3e59f-3446-4a93-9af6-d2dd3cb8ff26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e70832ba-1327-4147-9b70-69454df5af98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf3e59f-3446-4a93-9af6-d2dd3cb8ff26",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "ebe26caf-8189-4e8e-a9f1-97b0bf28742d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "e560ab2b-4f13-4418-97d4-f1824d0291a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebe26caf-8189-4e8e-a9f1-97b0bf28742d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82569589-3a69-4dfb-ba14-301425850a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebe26caf-8189-4e8e-a9f1-97b0bf28742d",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "b939e168-96d0-47e7-8a4e-b1efb50089e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "5cb9ae2d-2e79-4a24-8954-3392f988daaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b939e168-96d0-47e7-8a4e-b1efb50089e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f78792-d6bb-4cf6-ad94-3a093d75be70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b939e168-96d0-47e7-8a4e-b1efb50089e1",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "fba7a965-0f17-49f9-914e-1ce5f9fded1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "3574f0ac-1716-483b-ba76-6481500f9c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba7a965-0f17-49f9-914e-1ce5f9fded1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d3b32f0-9dcc-43dd-aa6a-b007431f2931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba7a965-0f17-49f9-914e-1ce5f9fded1f",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "d5f644f3-e28d-410a-9dd6-4910fe3bc5c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "f9132dc4-da1c-4225-ad77-f829d4ac91a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5f644f3-e28d-410a-9dd6-4910fe3bc5c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a233bcfd-87a0-4c1a-a7be-1b98d6b8502a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5f644f3-e28d-410a-9dd6-4910fe3bc5c3",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "a969f8eb-5487-49d9-b86a-560284e0afe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "e8ef53d4-a5a9-4c72-b56f-1fba0c596b64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a969f8eb-5487-49d9-b86a-560284e0afe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a21add9-2783-42ea-8d95-e2b709356bc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a969f8eb-5487-49d9-b86a-560284e0afe0",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "6a19c9b5-8378-42af-94be-f7ba5a5fe875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "40543af6-12dd-4087-bdd8-29e99f8f9404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a19c9b5-8378-42af-94be-f7ba5a5fe875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd48c179-8ef3-4ab1-adb2-8e7a939e765e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a19c9b5-8378-42af-94be-f7ba5a5fe875",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "dce210d3-135e-4ccf-ab55-7cb80833247f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "7780753a-4028-407b-923a-7b3d641185b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce210d3-135e-4ccf-ab55-7cb80833247f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3cca339-b14b-4162-9191-9e4fb38628d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce210d3-135e-4ccf-ab55-7cb80833247f",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        },
        {
            "id": "d7913d47-e48f-432a-a40c-7ef2b04ca43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "compositeImage": {
                "id": "b3526399-3780-4e21-bcce-5a019bc6a19e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7913d47-e48f-432a-a40c-7ef2b04ca43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95b21a1d-ae03-46b2-9fc0-303b7139e696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7913d47-e48f-432a-a40c-7ef2b04ca43c",
                    "LayerId": "9183b08f-6c25-4b50-aad9-0a898f626d94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9183b08f-6c25-4b50-aad9-0a898f626d94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbcfcaf9-ceab-49a6-ac05-9a14d03c41dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}