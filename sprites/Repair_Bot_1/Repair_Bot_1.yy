{
    "id": "b3dd024d-162f-4b81-8250-2cdb1ae674fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Repair_Bot_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07c4e52a-965d-457f-a362-d9e3c7a672b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3dd024d-162f-4b81-8250-2cdb1ae674fa",
            "compositeImage": {
                "id": "b554d5a3-800e-4cb4-9720-530ae343aba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07c4e52a-965d-457f-a362-d9e3c7a672b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "904f4017-d0dd-46f4-98de-5de5c3324ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07c4e52a-965d-457f-a362-d9e3c7a672b2",
                    "LayerId": "0f030e0d-ef00-4022-9f0c-8c39074fa9bb"
                }
            ]
        },
        {
            "id": "a36c3e39-40ef-4ab9-80b7-64108b55c81a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3dd024d-162f-4b81-8250-2cdb1ae674fa",
            "compositeImage": {
                "id": "8a3bb5cd-dae7-49dc-ab81-fc87a7faa853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36c3e39-40ef-4ab9-80b7-64108b55c81a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3460a245-ea60-429a-93f9-5227b3b17ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36c3e39-40ef-4ab9-80b7-64108b55c81a",
                    "LayerId": "0f030e0d-ef00-4022-9f0c-8c39074fa9bb"
                }
            ]
        },
        {
            "id": "7bbbc0f8-efc1-4e37-a45d-84b4cbe7c855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3dd024d-162f-4b81-8250-2cdb1ae674fa",
            "compositeImage": {
                "id": "e710035d-4e65-4f89-9a59-69c911311c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bbbc0f8-efc1-4e37-a45d-84b4cbe7c855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2faff269-0817-43a9-8449-341b51d9bd6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bbbc0f8-efc1-4e37-a45d-84b4cbe7c855",
                    "LayerId": "0f030e0d-ef00-4022-9f0c-8c39074fa9bb"
                }
            ]
        },
        {
            "id": "1a9a184e-81a9-4eaf-b01f-817f48fbf5a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3dd024d-162f-4b81-8250-2cdb1ae674fa",
            "compositeImage": {
                "id": "d97efb6d-2c2e-430d-9edf-bcbe55962b4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9a184e-81a9-4eaf-b01f-817f48fbf5a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4925ceeb-a7fa-44ed-902d-6a55dde1b25c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9a184e-81a9-4eaf-b01f-817f48fbf5a8",
                    "LayerId": "0f030e0d-ef00-4022-9f0c-8c39074fa9bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0f030e0d-ef00-4022-9f0c-8c39074fa9bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3dd024d-162f-4b81-8250-2cdb1ae674fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}