{
    "id": "3215dc94-d254-402f-ac6e-1138b1aa3de7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Entrenchment",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3907a504-69cd-4e43-8223-178e9ce0a4f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3215dc94-d254-402f-ac6e-1138b1aa3de7",
            "compositeImage": {
                "id": "c7f7c3f8-b849-4896-9cab-d6461c4e3ec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3907a504-69cd-4e43-8223-178e9ce0a4f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e72b747-760d-4965-9e8a-e20d9aa832b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3907a504-69cd-4e43-8223-178e9ce0a4f9",
                    "LayerId": "7fa7dd31-9440-4a77-8db1-6ecb759c2d9c"
                }
            ]
        },
        {
            "id": "08dd9f45-c5b2-4f7e-882a-2615ab486a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3215dc94-d254-402f-ac6e-1138b1aa3de7",
            "compositeImage": {
                "id": "c141f837-e97f-467a-a780-36327287ac0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08dd9f45-c5b2-4f7e-882a-2615ab486a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffea8049-d3a4-4c6f-acec-94dd0bbea417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08dd9f45-c5b2-4f7e-882a-2615ab486a68",
                    "LayerId": "7fa7dd31-9440-4a77-8db1-6ecb759c2d9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7fa7dd31-9440-4a77-8db1-6ecb759c2d9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3215dc94-d254-402f-ac6e-1138b1aa3de7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}