{
    "id": "daea5649-3e3b-4bd1-a1f4-79b2d7a8337b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Soldier1_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b28b6597-4427-4497-b0e2-e0244a1133b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daea5649-3e3b-4bd1-a1f4-79b2d7a8337b",
            "compositeImage": {
                "id": "33fa47b2-9ea3-4b14-b78b-38d7342d237d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b28b6597-4427-4497-b0e2-e0244a1133b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f00599-960a-4f8e-9c6b-384d19a36fb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b28b6597-4427-4497-b0e2-e0244a1133b3",
                    "LayerId": "1e953635-6f11-4e86-b02b-ac2203b9481a"
                }
            ]
        },
        {
            "id": "c947d3f8-e086-4dd3-ac6b-d4ced95660d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daea5649-3e3b-4bd1-a1f4-79b2d7a8337b",
            "compositeImage": {
                "id": "e8fd2ced-df90-49ce-ad05-959fffbfa229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c947d3f8-e086-4dd3-ac6b-d4ced95660d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf337e4-2c7f-4bb7-b1f4-0975c4d29419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c947d3f8-e086-4dd3-ac6b-d4ced95660d2",
                    "LayerId": "1e953635-6f11-4e86-b02b-ac2203b9481a"
                }
            ]
        },
        {
            "id": "bb1f75af-9073-43ac-994d-963cc20e94bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daea5649-3e3b-4bd1-a1f4-79b2d7a8337b",
            "compositeImage": {
                "id": "4bcd3f14-5282-4cb1-aefc-781f35d7e80d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1f75af-9073-43ac-994d-963cc20e94bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "479c5bf5-41c5-4b86-bc71-4ea8140b6d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1f75af-9073-43ac-994d-963cc20e94bc",
                    "LayerId": "1e953635-6f11-4e86-b02b-ac2203b9481a"
                }
            ]
        },
        {
            "id": "c5138620-4351-4ba3-a752-4b6639236ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daea5649-3e3b-4bd1-a1f4-79b2d7a8337b",
            "compositeImage": {
                "id": "d39bea00-cea0-45dc-aca6-9eee63b1f06f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5138620-4351-4ba3-a752-4b6639236ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa674e89-6643-4bd1-af7e-aa681b6303f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5138620-4351-4ba3-a752-4b6639236ba3",
                    "LayerId": "1e953635-6f11-4e86-b02b-ac2203b9481a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e953635-6f11-4e86-b02b-ac2203b9481a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daea5649-3e3b-4bd1-a1f4-79b2d7a8337b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}