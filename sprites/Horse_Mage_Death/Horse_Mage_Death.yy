{
    "id": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Horse_Mage_Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddd38b5e-746c-4270-b75f-bc8f852afda6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "compositeImage": {
                "id": "b6a08105-5486-498d-896f-770723bf689c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddd38b5e-746c-4270-b75f-bc8f852afda6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d1913ff-a49f-4be1-a885-36e21e24cde0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddd38b5e-746c-4270-b75f-bc8f852afda6",
                    "LayerId": "704ef472-d472-4190-a7f1-3c49ff37698c"
                }
            ]
        },
        {
            "id": "f38d7e80-4647-4860-8fec-311af5934418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "compositeImage": {
                "id": "062ba806-0360-4d7a-8fcc-e6627292c217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38d7e80-4647-4860-8fec-311af5934418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a01c2f-2c1b-4d1a-8b6c-9fc13b6930dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38d7e80-4647-4860-8fec-311af5934418",
                    "LayerId": "704ef472-d472-4190-a7f1-3c49ff37698c"
                }
            ]
        },
        {
            "id": "deb7e1f2-0089-4ed6-9318-b739fdb799b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "compositeImage": {
                "id": "8486b3f7-12af-4e21-8e0b-427f666bfc84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deb7e1f2-0089-4ed6-9318-b739fdb799b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847a8103-9070-4a05-97cb-47cad72de8ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deb7e1f2-0089-4ed6-9318-b739fdb799b0",
                    "LayerId": "704ef472-d472-4190-a7f1-3c49ff37698c"
                }
            ]
        },
        {
            "id": "b072e8b8-5007-44de-935f-0583b59d2d0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "compositeImage": {
                "id": "c2378b6b-bf5a-4d19-a918-c4cd4f76395d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b072e8b8-5007-44de-935f-0583b59d2d0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4a9ec42-4b09-4c9c-a939-1027e0e3504a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b072e8b8-5007-44de-935f-0583b59d2d0f",
                    "LayerId": "704ef472-d472-4190-a7f1-3c49ff37698c"
                }
            ]
        },
        {
            "id": "f393a1b6-08a0-4142-a4f4-c2342f32fdd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "compositeImage": {
                "id": "30634bb9-8095-44b7-af57-84b77bd2cea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f393a1b6-08a0-4142-a4f4-c2342f32fdd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2927956-8e2e-4ddc-abea-287a23865c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f393a1b6-08a0-4142-a4f4-c2342f32fdd6",
                    "LayerId": "704ef472-d472-4190-a7f1-3c49ff37698c"
                }
            ]
        },
        {
            "id": "f870d61a-d3f6-4c3e-843e-4878c5c204ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "compositeImage": {
                "id": "5d6e8dbd-fb3e-48bf-9538-6d1f2efdac11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f870d61a-d3f6-4c3e-843e-4878c5c204ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a89891ad-140a-417e-ab19-05fa9f90694c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f870d61a-d3f6-4c3e-843e-4878c5c204ad",
                    "LayerId": "704ef472-d472-4190-a7f1-3c49ff37698c"
                }
            ]
        },
        {
            "id": "a11bbc61-e9c1-4181-9db6-56c09effbb6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "compositeImage": {
                "id": "cd2fcad4-69fc-4f05-9b80-ad8609cac9e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a11bbc61-e9c1-4181-9db6-56c09effbb6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7020ed-6c45-4846-a026-1828dd9a2f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a11bbc61-e9c1-4181-9db6-56c09effbb6a",
                    "LayerId": "704ef472-d472-4190-a7f1-3c49ff37698c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "704ef472-d472-4190-a7f1-3c49ff37698c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a7331a9-bd29-4ff0-a7e5-cef0a83e3b49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}