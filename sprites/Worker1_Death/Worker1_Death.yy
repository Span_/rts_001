{
    "id": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Worker1_Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbd1eb3f-9cd7-441e-911a-a07cd7cd867f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "compositeImage": {
                "id": "94a914fa-b63f-47e9-88d5-9a3b81640e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd1eb3f-9cd7-441e-911a-a07cd7cd867f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ce97c2-eda2-4efe-9808-bc9562945931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd1eb3f-9cd7-441e-911a-a07cd7cd867f",
                    "LayerId": "17e737de-fd37-4ce7-bae8-874ca1b39ce0"
                }
            ]
        },
        {
            "id": "2cfdc6ab-7175-40fd-9674-58f21cdb10fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "compositeImage": {
                "id": "15b240c1-f364-4e4b-bc06-0fe50997194f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cfdc6ab-7175-40fd-9674-58f21cdb10fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f001cab2-d25b-4605-a720-1801123f3588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cfdc6ab-7175-40fd-9674-58f21cdb10fd",
                    "LayerId": "17e737de-fd37-4ce7-bae8-874ca1b39ce0"
                }
            ]
        },
        {
            "id": "0fea8a4c-10f2-4cb9-be41-99389ba69675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "compositeImage": {
                "id": "24d2d924-9b00-4d63-9245-51c8cd483bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fea8a4c-10f2-4cb9-be41-99389ba69675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9afe01aa-6967-4bd4-9ec4-9b705a969cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fea8a4c-10f2-4cb9-be41-99389ba69675",
                    "LayerId": "17e737de-fd37-4ce7-bae8-874ca1b39ce0"
                }
            ]
        },
        {
            "id": "945926e9-c6e2-40a7-b159-73c6afa40dd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "compositeImage": {
                "id": "ef127718-06c8-4edd-9b8a-44972d65c77e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "945926e9-c6e2-40a7-b159-73c6afa40dd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b49ed09-a8bd-43e4-8941-2c572506def6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945926e9-c6e2-40a7-b159-73c6afa40dd1",
                    "LayerId": "17e737de-fd37-4ce7-bae8-874ca1b39ce0"
                }
            ]
        },
        {
            "id": "377be839-6b7c-4157-943a-b564e49f2cd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "compositeImage": {
                "id": "312fff96-a699-483c-9517-53a2523588ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377be839-6b7c-4157-943a-b564e49f2cd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4291c81-dd85-4601-a43e-80aeda721212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377be839-6b7c-4157-943a-b564e49f2cd5",
                    "LayerId": "17e737de-fd37-4ce7-bae8-874ca1b39ce0"
                }
            ]
        },
        {
            "id": "ddc81907-3808-46e3-b720-aa254e710555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "compositeImage": {
                "id": "f253ebf0-29a6-4894-8b35-df6ff4f56c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddc81907-3808-46e3-b720-aa254e710555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9fe3211-4b6c-47b6-a190-272fec4fb95a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddc81907-3808-46e3-b720-aa254e710555",
                    "LayerId": "17e737de-fd37-4ce7-bae8-874ca1b39ce0"
                }
            ]
        },
        {
            "id": "15f517fe-ad58-43ed-88ff-29d1bf80af5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "compositeImage": {
                "id": "8f22f3dc-b2de-46ea-a2ad-8a06db3e137e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f517fe-ad58-43ed-88ff-29d1bf80af5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af9f561-8205-4d21-868d-5a634e8eeeb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f517fe-ad58-43ed-88ff-29d1bf80af5f",
                    "LayerId": "17e737de-fd37-4ce7-bae8-874ca1b39ce0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "17e737de-fd37-4ce7-bae8-874ca1b39ce0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed9baa2f-abe8-4d0f-a4dc-b31491e41857",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}