{
    "id": "836cdf25-0f39-4251-9376-9819db12d315",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bullet_Impact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34bdf557-1ad9-41d7-8430-c2944ae4f7d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836cdf25-0f39-4251-9376-9819db12d315",
            "compositeImage": {
                "id": "2dcb8abe-0a9a-4ef5-bc1a-86e66a6e79ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34bdf557-1ad9-41d7-8430-c2944ae4f7d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772261cb-6207-4982-92af-dc7c283c1817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34bdf557-1ad9-41d7-8430-c2944ae4f7d0",
                    "LayerId": "ea43287e-2a49-4523-bfdf-3dad79cf81b8"
                }
            ]
        },
        {
            "id": "b83ecee3-2ffc-49d8-bcac-4a182c97ff3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836cdf25-0f39-4251-9376-9819db12d315",
            "compositeImage": {
                "id": "7af89724-8ba9-4ece-89af-1f7f82ade2eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b83ecee3-2ffc-49d8-bcac-4a182c97ff3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f9ba9d4-1ea6-4265-9835-84ed701aeb15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b83ecee3-2ffc-49d8-bcac-4a182c97ff3b",
                    "LayerId": "ea43287e-2a49-4523-bfdf-3dad79cf81b8"
                }
            ]
        },
        {
            "id": "e269adf5-560c-4271-bf18-138575e4447b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836cdf25-0f39-4251-9376-9819db12d315",
            "compositeImage": {
                "id": "934c614d-fabc-478d-9b7f-d68467736b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e269adf5-560c-4271-bf18-138575e4447b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec0498b-4741-4e43-81ab-e85ab60a080a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e269adf5-560c-4271-bf18-138575e4447b",
                    "LayerId": "ea43287e-2a49-4523-bfdf-3dad79cf81b8"
                }
            ]
        },
        {
            "id": "327533ba-d553-44a9-ab24-e18849e6fd07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836cdf25-0f39-4251-9376-9819db12d315",
            "compositeImage": {
                "id": "592f48e5-edee-4be9-b625-099951e0bce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "327533ba-d553-44a9-ab24-e18849e6fd07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57cb1711-8a55-410e-ab99-9bbac157f2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "327533ba-d553-44a9-ab24-e18849e6fd07",
                    "LayerId": "ea43287e-2a49-4523-bfdf-3dad79cf81b8"
                }
            ]
        },
        {
            "id": "c9d67ba8-7cc9-4ebe-9240-217c136c73df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836cdf25-0f39-4251-9376-9819db12d315",
            "compositeImage": {
                "id": "8614dd72-05af-4d5a-93b4-672bf3cea177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d67ba8-7cc9-4ebe-9240-217c136c73df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67a626f9-d6aa-4bf3-9449-355bce30dfc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d67ba8-7cc9-4ebe-9240-217c136c73df",
                    "LayerId": "ea43287e-2a49-4523-bfdf-3dad79cf81b8"
                }
            ]
        },
        {
            "id": "fe0cf5a4-d7ee-4067-9e86-7a7802837416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836cdf25-0f39-4251-9376-9819db12d315",
            "compositeImage": {
                "id": "4318cf69-7017-4fec-83f2-90a9c4a58d3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe0cf5a4-d7ee-4067-9e86-7a7802837416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ef2ca2-dcc6-4463-876e-5a62d6e340a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe0cf5a4-d7ee-4067-9e86-7a7802837416",
                    "LayerId": "ea43287e-2a49-4523-bfdf-3dad79cf81b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ea43287e-2a49-4523-bfdf-3dad79cf81b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "836cdf25-0f39-4251-9376-9819db12d315",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 24,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 10
}