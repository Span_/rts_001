{
    "id": "f8d841f0-05d1-49af-89b4-37a58775ee38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Soldier1_Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e774d5e0-fecb-407d-bf01-8f620a2489a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "compositeImage": {
                "id": "8d343b82-b266-4b23-8470-cf88f6098256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e774d5e0-fecb-407d-bf01-8f620a2489a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eecbbf5-87c0-4297-bf67-b7a477fa3443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e774d5e0-fecb-407d-bf01-8f620a2489a5",
                    "LayerId": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b"
                }
            ]
        },
        {
            "id": "7dd43291-b535-40a6-91c1-141c9e48d57a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "compositeImage": {
                "id": "a3796243-e6f8-4ec9-b9b5-64727f0c4e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd43291-b535-40a6-91c1-141c9e48d57a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84db0bcb-6dc0-425f-8c37-8e6a6fed609d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd43291-b535-40a6-91c1-141c9e48d57a",
                    "LayerId": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b"
                }
            ]
        },
        {
            "id": "6f3ce9cb-6f5d-4989-9378-72adc812c21c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "compositeImage": {
                "id": "a93b257c-8817-4fb4-adc3-7f7e956bab5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3ce9cb-6f5d-4989-9378-72adc812c21c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4f964c7-364e-4e81-a36b-cfd80de1bf30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3ce9cb-6f5d-4989-9378-72adc812c21c",
                    "LayerId": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b"
                }
            ]
        },
        {
            "id": "c0c42e62-a744-438d-a12a-94f5ae48d787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "compositeImage": {
                "id": "b545a393-8995-48f0-8c59-906ecf2829d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c42e62-a744-438d-a12a-94f5ae48d787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ba48317-090f-485f-9a7b-6674af3b7d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c42e62-a744-438d-a12a-94f5ae48d787",
                    "LayerId": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b"
                }
            ]
        },
        {
            "id": "5089d93f-2822-4cc0-af07-b77e20e3b596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "compositeImage": {
                "id": "f44a7bfc-76a6-4f06-92ea-548402de8178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5089d93f-2822-4cc0-af07-b77e20e3b596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f37e658e-af05-4b56-9d68-d7009558a48b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5089d93f-2822-4cc0-af07-b77e20e3b596",
                    "LayerId": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b"
                }
            ]
        },
        {
            "id": "5e91fd57-e6f4-4d18-b7a8-5045e65ff873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "compositeImage": {
                "id": "ec7f9823-5df9-4905-b87c-5095a7d7c346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e91fd57-e6f4-4d18-b7a8-5045e65ff873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0036b253-c389-4562-bcf8-582b0ca30aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e91fd57-e6f4-4d18-b7a8-5045e65ff873",
                    "LayerId": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b"
                }
            ]
        },
        {
            "id": "55319892-b042-4969-a2ae-728dc6bae5e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "compositeImage": {
                "id": "e447cf56-2f8a-49e9-98c3-c864a8a7a2ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55319892-b042-4969-a2ae-728dc6bae5e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f525d18-ed33-4c19-bf3a-599b9a2b90fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55319892-b042-4969-a2ae-728dc6bae5e1",
                    "LayerId": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b3ccda2a-0651-4fe7-8e29-91a177c5bb7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8d841f0-05d1-49af-89b4-37a58775ee38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}