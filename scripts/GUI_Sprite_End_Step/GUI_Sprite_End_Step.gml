///GUI_Sprite_End_Step(bool fixed);
///@param bool_fixed
///@param bool_scale
var fixed = argument0;
var toScale = argument1;

if (fixed) {
	//keep the positions relative to camera regardless of zoom factor
	x = xPosInit * camera_get_view_width(view_camera[0]) / global.wCamInit + camera_get_view_x(view_camera[0]);
	y = yPosInit * camera_get_view_height(view_camera[0]) / global.hCamInit + camera_get_view_y(view_camera[0]);
}

if (toScale) {
	
	//scale images in case of camera zoom
	image_xscale = wSpriteInit * camera_get_view_width(view_camera[0]) / global.wCamInit;
	image_yscale = hSpriteInit * camera_get_view_height(view_camera[0]) / global.hCamInit;
} else {
	image_xscale = wSpriteInit;
	image_yscale = hSpriteInit;
}