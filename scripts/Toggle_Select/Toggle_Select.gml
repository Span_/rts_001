///Toggle_Select(press_x,press_y,release_x,release_y,click_radius, building_bool)
///@param px
///@param py
///@param rx
///@param ry
///@param click_radius
///@param building_bool


//press
var px = argument0;
var py = argument1;

//release
var rx = argument2;
var ry = argument3;

//how close a click can be to select it
var click_radius = argument4;
//is it a building
var isBuilding = argument5;
//case
//var clck = distance_to_point(x,px)<1 && distance_to_point(y,py)<1
//var tlbr = (x>px && x<rx && y>py && y<ry)
//var bltr = (x>px && x<rx && y<py && y>ry)
//var brtl = (x<px && x>rx && y<py && y>ry)
//var trbl = (x<px && x>rx && y>py && y<ry)

//if (tlbr || brtl || bltr || trbl || clck) {
//	selected = true;
//} else {
//	selected = false;
//}

if((median(px,x,rx) == x && median(py,y,ry) == y) || (distance_to_point(px,py)<click_radius && distance_to_point(rx,ry)<click_radius))
{
	//if ( !selected && instance_exists(GUI_Obj)) {
		//selected_index = GUI_Obj.units_selected; //for determining path destination for multi-select 
		//GUI_Obj.units_selected ++;
	if (!isBuilding) {
		if (ds_list_find_index(global.units_selected_list, id) == -1) {
			//add it if it's not already in the list
			ds_list_add(global.units_selected_list,id);
		}
	}
	//}
	selected = true;
} else if (isBuilding) {
	//if ( selected && instance_exists(GUI_Obj)) {
	//	//selected_index = 0;
	//	//GUI_Obj.units_selected --;
	//}
		selected = false;
}