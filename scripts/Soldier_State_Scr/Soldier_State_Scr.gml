///Soldier_State_Scr(idle,attack,move,particle);

///@param spr_idle
///@param spr_attack
///@param spr_move
///@param particle

var spr_idle = argument0;
var spr_attack = argument1;
var spr_move = argument2;
var part = argument3;


switch (state){
	case state_enum.idle:
		Unit_Idle();//friction = 0;
		
		sprite_index = spr_idle;

		var foe = instance_nearest(x,y,Enemy_Unit_Obj);
		var foe_bldg = instance_nearest(x,y,Enemy_Building_Obj);
		
		if (foe != noone && distance_to_object(foe) < aggroRange){
			
				state = state_enum.attack;
			
		} else if (foe_bldg != noone && distance_to_object(foe_bldg) < aggroRange) {

				state = state_enum.attack;

		}
		
		break;
	case state_enum.attack:
	
		
		var foe = instance_nearest(x,y,Enemy_Unit_Obj);
		var foe_bldg = instance_nearest(x,y,Enemy_Building_Obj);
		
		if (foe != noone && distance_to_object(foe) < aggroRange){
			
			Soldier_Attack_Scr(foe, part, spr_attack);
			
		} else if (foe_bldg != noone && distance_to_object(foe_bldg) < aggroRange) {
			
			Soldier_Attack_Scr(foe_bldg, part, spr_attack);
			
		} else { state = state_enum.idle; }
		
		break;
	case state_enum.move:
		sprite_index = spr_move;
		Unit_Move(spr_move);
		break;
	case state_enum.attack_move:
		//if you see enemy, attack. otherwise, move step.
		
		var foe = instance_nearest(x,y,Enemy_Unit_Obj);
		var foe_bldg = instance_nearest(x,y,Enemy_Building_Obj);
		
		if (foe != noone && distance_to_object(foe) < aggroRange){
			
			Soldier_Attack_Scr(foe, part, spr_attack);
			//state = state_enum.attack;
			//break;
			
		} else if (foe_bldg != noone && distance_to_object(foe_bldg) < aggroRange) {

			Soldier_Attack_Scr(foe_bldg, part, spr_attack);
			//state = state_enum.attack;
			//break;
		} else {
			
			//sprite_index = spr_move;
			Unit_Move(spr_move);
		}
		
		
		break;
	default:
		Unit_Idle();
		state = state_enum.idle;
}