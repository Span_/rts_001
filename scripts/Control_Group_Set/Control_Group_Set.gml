///Control_Group_Set(key, list);
///@param key
///@param key_list

var k = argument0; //key
var l = argument1; //list


if (keyboard_check_pressed(k) && keyboard_check(vk_control)) {
	//assign selected list hot key to vk_numpad0
	if (!ds_list_empty(global.units_selected_list)) {
		ds_list_copy(l, global.units_selected_list);
	}
} else if (keyboard_check_pressed(k) && !ds_list_empty(l) ) {
	//clear list if units are selected already
	if (!ds_list_empty(global.units_selected_list)) {
		for (var i = 0; i < ds_list_size(global.units_selected_list); i ++) {
			global.units_selected_list[| i ].selected = false;
		}
		ds_list_clear(global.units_selected_list);
	}
	
	//select the list assigned to the key
	ds_list_copy(global.units_selected_list, l);
	for (var i = 0; i < ds_list_size(global.units_selected_list); i ++) {
		global.units_selected_list[| i ].selected = true;
	}
}