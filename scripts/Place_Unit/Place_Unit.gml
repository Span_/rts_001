///Place_Unit(x, y, offset, layer, unit_obj, resource_cost, unit_cost);
///@param x
///@param y
///@param offset
///@param layer
///@param unit_obj

var xu = argument0;
var yu = argument1;
var off = argument2; //16
var l = argument3;
var obj = argument4;
var cr = argument5;
var cu = argument6;

//soldier = instance_create_layer(xu,yu,l,obj);

if (instance_exists(Score_Obj)){
	if (Score_Obj.resources >= cr && Score_Obj.units + cu <= Score_Obj.maxUnits) {
			
		//place free?
		
		for ( var i = -3; i < 3; i ++) {
			if(instance_position(xu + i * off,yu,all) == noone) {
				var unit = instance_create_layer(xu + i * off,yu,l,obj);
			
				Score_Obj.resources -= cr;
				if (Base_Obj.target != -1){
					//var tempTarget = Base_Obj.target;
					unit.target = instance_create_layer(Base_Obj.target.x, Base_Obj.target.y, global.Inst_Units_Layer, Target_Obj);
					unit.state = state_enum.move;
				}
				
				i = 10; //exit loop
			
			}//if there was no space??
			
		}
		//if(instance_position(xu,yu,all) == noone) {
		//	var unit = instance_create_layer(xu,yu,l,obj);
			
		//	Score_Obj.resources -= cr;
		//	if (Base_Obj.target != -1){
		//		//var tempTarget = Base_Obj.target;
		//		unit.target = instance_create_layer(Base_Obj.target.x, Base_Obj.target.y, global.Inst_Units_Layer, Target_Obj);
		//		unit.state = "move";
		//	}
			
		//} else if (instance_position(xu + off,yu,all) == noone){
		//	var unit = instance_create_layer(xu + off,yu,l,obj);
			
		//	Score_Obj.resources -= cr;
		//	if (Base_Obj.target != -1){
		//		//var tempTarget = Base_Obj.target;
		//		unit.target = instance_create_layer(Base_Obj.target.x, Base_Obj.target.y, global.Inst_Units_Layer, Target_Obj);
		//		unit.state = "move";
		//	}
			
		//} else if (instance_position(xu - off,yu,all) == noone){
		//	//place
		//	var unit = instance_create_layer(xu - off,yu,l,obj);
			
		//	Score_Obj.resources -= cr;
		//	if (Base_Obj.target != -1){
		//		//var tempTarget = Base_Obj.target;
		//		unit.target = instance_create_layer(Base_Obj.target.x, Base_Obj.target.y, global.Inst_Units_Layer, Target_Obj);
		//		unit.state = "move";
		//	}
			
		//}//else unable to place free

	}
}