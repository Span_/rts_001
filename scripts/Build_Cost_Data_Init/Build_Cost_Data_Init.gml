///@description Build_Cost_Data_Init();

//units
global.buildCost_Soldier = 100; //build cost
global.buildCost_Worker = 50;
global.buildCost_Repair_Bot = 50;
global.buildCost_Sniper = 150;

//buildings
global.buildCost_House = 125;
global.buildCost_Entrenchment = 200;
global.buildCost_Farm = 200;
global.buildCost_Wall = 100;

//how much unit population each costs
global.unitCost_Soldier = 1; //build cost
global.unitCost_Worker = 1;
global.unitCost_Sniper = 1;
global.unitCost_Repair_Bot = 1;


