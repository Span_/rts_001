/// GUI_Array_Init();
//
var xCamInit = camera_get_view_x(view_camera[0]);
var yCamInit = camera_get_view_y(view_camera[0]);

//var wCamInit = camera_get_view_width(view_camera[0]);
//var hCamInit = camera_get_view_height(view_camera[0]);

//instance_create_layer(64,64,"GUI_Layer_1", Square_Button_Obj);

//testbutton
//GUI_Array[0,0] = 64; //x
//GUI_Array[0,1] = 64; //y
//GUI_Array[0,2] = global.GUI_Middle_Layer; //layer
//GUI_Array[0,3] = Square_Button_Obj; //object

//top left corner of button array
var gx = 520;
var gy = 240;
var s = 40; //spacing between buttons


//			|	gx | gx + s | gx + 2 * s
//gy		|
//gy + s	|
//gy + 2 * s|


//back
global.GUI_Array_Build_Menu[0,0] = gx + 2 * s + xCamInit; //x
global.GUI_Array_Build_Menu[0,1] = gy + 2 * s + yCamInit; //y
global.GUI_Array_Build_Menu[0,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Build_Menu[0,3] = Back_Button_Obj; //object
global.GUI_Array_Build_Menu[0,4] = "Back"; //tool tip

//build units
global.GUI_Array_Build_Menu[1,0] = gx + xCamInit; //x
global.GUI_Array_Build_Menu[1,1] = gy + 2 * s + yCamInit; //y
global.GUI_Array_Build_Menu[1,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Build_Menu[1,3] = Build_Units_Button_Obj; //object
global.GUI_Array_Build_Menu[1,4] = "Units"; //tool tip

//build buildings
global.GUI_Array_Build_Menu[2,0] = gx + s + xCamInit; //x
global.GUI_Array_Build_Menu[2,1] = gy + 2 * s + yCamInit; //y
global.GUI_Array_Build_Menu[2,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Build_Menu[2,3] = Build_Buildings_Button_Obj; //object
global.GUI_Array_Build_Menu[2,4] = "Buildings"; //tool tip



//bottom right bar: tl
global.GUI_Array_Units[0,0] = gx + xCamInit; //x
global.GUI_Array_Units[0,1] = gy + yCamInit; //y
global.GUI_Array_Units[0,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Units[0,3] = Create_Worker_Button_Obj; //object
global.GUI_Array_Units[0,4] = "Worker: $" + string(global.buildCost_Worker); //tool tip

//bottom right bar: tm
global.GUI_Array_Units[1,0] = gx + s + xCamInit; //x
global.GUI_Array_Units[1,1] = gy + yCamInit; //y
global.GUI_Array_Units[1,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Units[1,3] = Create_Soldier_Button_Obj; //object
global.GUI_Array_Units[1,4] = "Soldier: $" + string(global.buildCost_Soldier); //tool tip

//bottom right bar: tr
global.GUI_Array_Units[2,0] = gx + 2 * s + xCamInit; //x
global.GUI_Array_Units[2,1] = gy + yCamInit; //y
global.GUI_Array_Units[2,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Units[2,3] = Create_Sniper_Button_Obj; //object
global.GUI_Array_Units[2,4] = "Sniper: $" + string(global.buildCost_Sniper); //tool tip

//bottom right bar: bl
global.GUI_Array_Units[3,0] = gx + xCamInit; //x
global.GUI_Array_Units[3,1] = gy + s + yCamInit; //y
global.GUI_Array_Units[3,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Units[3,3] = Create_Repair_Bot_Button_Obj; //object
global.GUI_Array_Units[3,4] = "Repair Bot: $" + string(global.buildCost_Repair_Bot); //tool tip





////bottom right bar: ml
global.GUI_Array_Buildings[0,0] = gx + xCamInit; //x
global.GUI_Array_Buildings[0,1] = gy + yCamInit; //y
global.GUI_Array_Buildings[0,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Buildings[0,3] = Create_Entrenchment_Button_Obj; //object
global.GUI_Array_Buildings[0,4] = "Entrenchment: $" + string(global.buildCost_Entrenchment); //tool tip

////bottom right bar: mm
global.GUI_Array_Buildings[1,0] = gx + s + xCamInit; //x
global.GUI_Array_Buildings[1,1] = gy + yCamInit; //y
global.GUI_Array_Buildings[1,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Buildings[1,3] = Create_Farm_Button_Obj; //object
global.GUI_Array_Buildings[1,4] = "Farm: $" + string(global.buildCost_Farm); //tool tip

//bottom right bar: mr
global.GUI_Array_Buildings[2,0] = gx + 2 * s + xCamInit; //x
global.GUI_Array_Buildings[2,1] = gy + yCamInit; //y
global.GUI_Array_Buildings[2,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Buildings[2,3] = Create_House_Button_Obj; //object
global.GUI_Array_Buildings[2,4] = "House: $" + string(global.buildCost_House); //tool tip

//bottom right bar: bm
global.GUI_Array_Buildings[3,0] = gx + xCamInit; //x
global.GUI_Array_Buildings[3,1] = gy + s + yCamInit; //y
global.GUI_Array_Buildings[3,2] = global.GUI_Middle_Layer; //layer
global.GUI_Array_Buildings[3,3] = Create_Wall_Button_Obj; //object
global.GUI_Array_Buildings[3,4] = "Wall: $" + string(global.buildCost_Wall); //tool tip

////bottom right bar: br
//GUI_Array[9,0] = gx + 2 * s + xCamInit; //x
//GUI_Array[9,1] = gy + 2 * s + yCamInit; //y
//GUI_Array[9,2] = "GUI_Layer_1"; //layer
//GUI_Array[9,3] = Create_Soldier_Button_Obj; //object


