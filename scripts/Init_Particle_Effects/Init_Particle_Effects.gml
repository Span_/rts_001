///Init_Particle_Effects();


global.Part_System = part_system_create_layer(global.Effects_Layer, false);
global.Part_Emitter = part_emitter_create(global.Part_System);

global.Bullet_Impact_Part = part_type_create();
part_type_sprite(global.Bullet_Impact_Part, Bullet_Impact, true, false, false);
part_type_life(global.Bullet_Impact_Part, 6, 6);


global.Blue_Lightning_Part = part_type_create();
part_type_sprite(global.Blue_Lightning_Part, Blue_Lightning, true, false, false);
part_type_life(global.Blue_Lightning_Part, 6, 6);

global.Chaos_Bolt_Part = part_type_create();
part_type_sprite(global.Chaos_Bolt_Part, Chaos_Bolt_Spr, true, false, false);
part_type_life(global.Chaos_Bolt_Part, 8, 8);