///Init the MP Grid  object
///Init_Grid_Path();

global.mp_grid = mp_grid_create(0, 0, room_width / global.cell_size, room_height / global.cell_size, 
		global.cell_size, global.cell_size);

//add objects to avoid
//mp_grid_add_instances(global.mp_grid, Path_Avoid_Obj, true);
//mp_grid_add_instances(global.mp_grid, Terrain_Collide_Obj, false);
//mp_grid_add_instances(global.mp_grid, Mineral_Obj, true);
////mp_grid_add_instances(global.mp_grid, Building_Obj, true);
//mp_grid_add_instances(global.mp_grid, Enemy_Building_Obj, true);