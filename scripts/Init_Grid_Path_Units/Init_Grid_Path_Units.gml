///Init the MP Grid  object
///Init_Grid_Path();

global.mp_grid_units = mp_grid_create(0, 0, room_width / global.cell_size, room_height / global.cell_size, 
		global.cell_size, global.cell_size);

//add objects to avoid
//Init_Grid_Path();

mp_grid_add_instances(global.mp_grid_units, Terrain_Collide_Obj, true);
mp_grid_add_instances(global.mp_grid_units, Mineral_Obj, true);
mp_grid_add_instances(global.mp_grid_units, Enemy_Building_Obj, true);
mp_grid_add_instances(global.mp_grid_units, Building_Obj, true);
