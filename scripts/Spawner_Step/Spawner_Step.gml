///Spawner_Step(enemy_Obj);
///@param enemy_obj

var enemy_obj = argument0;
/// @description spawn 
if (curHealth <= 0) instance_destroy();

if (countdown < 1) {
	countdown = spawnRate;
	
	//get A* path
	if (path == -1) Spawner_Init_Path();
	
//where to place the unit
	var xu = x;
	var yu = y + sprite_height/2;
	
	if(instance_position(xu,yu,all) == noone) {
		var unit = instance_create_layer(xu,yu,global.Inst_Units_Layer,enemy_obj);
	
	
 //path_duplicate(path);
		////draw_path(path,xu,yu,true);
		//for (var i = 1; i < path_get_number(path); i ++) {
			
		//		var px = path_get_x(path, i);
		//		var py = path_get_y(path, i);
		//		instance_create_layer(px,py,global.Inst_Units_Layer, Rally_Point_Obj);
		//}
		if (path != -1) {
			
			unit.path = path
		} else {
			//uses MP potential step instead of A* for pathing
			unit.destination = instance_create_layer(room_width / 2, room_height / 2, 
													global.Inst_Units_Layer, Target_Obj);
		}
		
		unit.state = state_enum.attack_move;
		
	}
										
	//make unit in free spot
	//Place_Unit(xu, yu, 16, global.Inst_Units_Layer, Monster_Doggo_Obj, 0, 0);

} else {
	countdown --;
}