///Move_Potential_Step(sprite_index, destinationX, destinationY);
///@param sprite_index
///@param destination_X
///@param destination_Y

var spr = argument0;
var dx = argument1;
var dy = argument2;

sprite_index = spr;
mp_potential_step_object(dx, dy, movementSpeed, Path_Avoid_Obj);
					
if (dx < x) {
	image_xscale = -1;
} else {
	image_xscale = 1;
}