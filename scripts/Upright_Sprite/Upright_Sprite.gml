/// Upright_Sprite(scale);
scale = argument0;
if (image_angle < 270 && image_angle > 90) {
	image_yscale = -1 * scale;
} else {
	image_yscale = scale;
}