
///@param foe
///@param parttype
///@param Sprite_Index

var foe = argument0;
var parttype = argument1;
var spriteInd = argument2;

if (distance_to_object(foe) < aggroRange){
	
	if(cooldown < 1){
		//speed = 0;
		sprite_index = spriteInd;
		image_index = 0;//restart the animation
		//keep image facing enemy
		if (foe.x < x) {
			image_xscale = -1;
		} else {
			image_xscale = 1;
		}
					
		//global.Bullet_Impact_Part
		//effect_create_above(, foe.x, foe.y, 1, c_white);
		part_system_position(global.Part_Emitter, foe.x, foe.y);
		part_emitter_burst(global.Part_System, global.Part_Emitter, parttype, 1);
					
					
		foe.curHealth -= damage;
		//bullet = instance_create_layer(x,y,"Inst_Units_Layer", Bullet_Obj);
		//bullet.damage = damage;
		//bullet.direction = point_direction(x,y,foe.x,foe.y);
		//bullet.direction = bullet.direction + random_range(-1,1);
		//bullet.image_angle = bullet.direction;
			
		cooldown = attackSpeed;
	//pause at end of attack animation
	} else if (image_index > image_number - 1) image_index = image_number - 1;
} else {
	state = state_enum.idle;
}