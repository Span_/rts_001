///Auto_Tile(spriteType, sIndex, cX, cY, map_id);
///@param spriteType_int
///@param sIndex_int
///@param x
///@param y
///@param tilemap_id


var spriteType = argument0; //first index of 2D array. 
var sIndex = argument1; //which sprite pattern to place, i.e. tl, br, mid... goes from 0 to 8
var cX = argument2; //center piece to place
var cY = argument3;
var map_id = argument4; //ID of the tile map for the layer to paint in

for (var j = -16; j <= 16; j += 16){
	for (var i = -16; i <= 16; i += 16){
		//how the edges touch determine how to draw edges
		
		var bVal = -1; //bit array value
		
		var ind = tile_get_index(tilemap_get_at_pixel(map_id,cX + i, cY + j)); //index in the tile map array
		
		//change index in tile map array to index in sprite_array, get corresponding bVal
		for (var l = 0; l < array_length_2d(global.sprite_array,spriteType); l ++){
			//loop through sprite array and see if var is in there
			
			
			if(ind == global.sprite_array[spriteType,l]){
				//we got index l
				bVal = global.sprite_bool_array[sIndex] | global.sprite_bool_array[l]; //bitwise or them together
			}
		}
		
		if ( bVal == -1 ) bVal = global.sprite_bool_array[sIndex]; //if the tile in the tile map is not found / blank, assume 0000
		
		for (var b = 0; b < array_length_1d(global.sprite_bool_array); b++ ){
			//find bVal in b array, set index equal to new sprite
			if(bVal == global.sprite_bool_array[b]) {
				tilemap_set_at_pixel(map_id, global.sprite_array[spriteType,b], cX + i, cY + j);
			}
		}
		
		sIndex ++; //cycle through all 9 sprites
	}
}