///Enemy_Unit_States(spr_idle, spr_attack, spr_move, particle);
///@param sprite_idle
///@param sprite_attack
///@param sprite_move
///@param particle

var spr_idle = argument0;
var spr_attack = argument1;
var spr_move = argument2;
var part = argument3;


switch (state) {
	case state_enum.idle:
		Enemy_Unit_Idle(); //set speed = 0;
		sprite_index = spr_idle;
		
		
		if ( (destination != -1 && instance_exists(destination) ) || path != -1) {
			state = state_enum.attack_move;
			
		} else {
		
			//if enemy spotted, move towards it.
			if (instance_exists(Unit_Obj) || instance_exists(Building_Obj)){
		
				var foe = instance_nearest(x,y,Unit_Obj);
				var foe_building = instance_nearest(x,y,Building_Obj);
		
				//look for nearby foe
				if (distance_to_object(foe) < aggroRange)
				{
					target = foe;
					state = state_enum.move;
				} else if (distance_to_object(foe_building) < aggroRange){ 
					target = foe_building;
					state = state_enum.move;
				}
			
			
			}
		}
		break;
		
	case state_enum.attack:
		//attack enemy, if no more enemy, go to idle
		
		if (!instance_exists(target)) {
			//if no more enemy go idle
			target = -1;
			state = state_enum.idle;
		} else if (distance_to_object(target)  < attackRange + target.sprite_width / 3) { 
			//attack while in range
			sprite_index = spr_attack;
			speed = 0;
			if (cooldown <= 0) {
				image_index = 0;
				target.curHealth -= damage;
				cooldown = attackSpeed;
				
				if (part != -1) {
				
					part_system_position(global.Part_Emitter, target.x, target.y);
					part_emitter_burst(global.Part_System, global.Part_Emitter, part, 1);	
				}
				
			//pause at end of attack animation
			} else if (image_index > image_number - 1) image_index = image_number - 1;
		} else if (distance_to_object(target) > attackRange && distance_to_object(target) < aggroRange) {
			// if out of range, move
			state = state_enum.move;
		} else {
			//get new target
			//instance_destroy(target);
			target = -1;
			state = state_enum.idle;
		}
		
		break;
	case state_enum.move:
		
		//if within range of enemy, attack it.
		//if no more enemy, go to idle
		if (!instance_exists(target)) {
			//if no more enemy go idle
			target = -1;
			
			if (instance_exists(destination) || path != -1) {
				state = state_enum.attack_move;
			} else {
				state = state_enum.idle;
			}
			
		} else if (distance_to_object(target) < attackRange + target.sprite_width / 3) { //
				//attack
			state = state_enum.attack
		} else if (distance_to_object(target) < aggroRange){
			//chase
			Move_Potential_Step(spr_move, target.x, target.y);
			
		} else {
			//target moved out of range
			target = -1;
			if (instance_exists(destination) || path != -1) {
				state = state_enum.attack_move;
			} else {
				state = state_enum.idle;
			}
			
		}
		
		break;
	case state_enum.attack_move:
	
		//if enemy spotted, move towards it.
		if (instance_exists(Unit_Obj) || instance_exists(Building_Obj)){ 
			//TODO: separate these in case of one but not other exists.
		
			var foe = instance_nearest(x,y,Unit_Obj);
			var foe_building = instance_nearest(x,y,Building_Obj);
		
			//look for nearest foe
			if (distance_to_object(foe) < aggroRange 
				&& distance_to_object(foe) < distance_to_object(foe_building))
			{
				//pathDepart_X = x;
				//pathDepart_Y = y;
				target = foe;
				state = state_enum.move;
				//pathing = false;
				break;
			} else if (distance_to_object(foe_building) < aggroRange){ 
				//pathDepart_X = x;
				//pathDepart_Y = y;
				target = foe_building;
				state = state_enum.move;
				//pathing = false;
				break;
			} //else {
				//nothing nearby - 
				//Move_Potential_Step(Monster_Doggo_Move, destination.x, destination.y);
			//}
				
		} //else {
			//no units??!!
			//state = state_enum.idle;
			//Move_Potential_Step(Monster_Doggo_Move, destination.x, destination.y);
		//}
	
		//move towards destination, if any enemies come in range, attack nearest, then continue
		if (path != -1) {
			//we have a path
			//if (pathDepart_X != -1) {
				//we departed from the path and need to move back to it
				
			//check if we arrived
			var px = path_get_point_x(path, pathPos);
			var py = path_get_point_y(path, pathPos);
			if (distance_to_point(px, py) < 8) {
				
				pathPos +=1;
				px = path_get_point_x(path, pathPos);
				py = path_get_point_y(path, pathPos);
				Move_Potential_Step(spr_move, px, py);
				
			} else {
				//didn't arrive yet
				Move_Potential_Step(spr_move, px, py);
			}
			
			if (pathPos == path_get_number(path)) {
				state = state_enum.idle;
				path = -1;
			}
			
			//} else {
				//we are on the path and need to progress along it
				//if (!pathing) {
					//start the path if not already on it
					//path_start(path, movementSpeed, path_action_stop, true);
					//pathing = true;
					//break;
				//} 
				
			//}
		//no path to move along, check if destination exists
		} else if (instance_exists(destination)) {
			//if any enemies nearby, attack, else move
			
			if (distance_to_object(destination) < 5) {
				//we reached the destination
				state = state_enum.idle;
				instance_destroy(destination);
				destination = -1;
				//break;
			} else {
				Move_Potential_Step(spr_move, destination.x, destination.y);
			}
		} else {
			//no destination
			state = state_enum.idle;
		}
		break;
	default:
		state = state_enum.idle;
	
}