///Unit_Move(spr_move)
///@param spr_move

var spr_move = argument0;

friction = 0;
if (instance_exists(target)){
//if (target != -1){

	if (point_distance(x, y, target.x, target.y) < 2){
		with (target) instance_destroy();
		if (path_exists(path)) path_delete(path);
		path = -1;
		pathPos = 1;
		state = state_enum.idle;
	} else {
		//move_towards_point(target.x,target.y,movementSpeed);
		//mp_potential_step_object(target.x, target.y, movementSpeed, Path_Avoid_Obj);
		
		if (path == -1 || !path_exists(path)) {
			//init path if none
			path = path_add();
			if (!mp_grid_path(global.mp_grid_units, path, x, y, target.x, target.y, 1)){
				if (path_exists(path) ) path_delete(path);
				path = -1;
			}
			pathPos = 1;
		} 
		
		if (path_exists(path) && path != -1) { //path != -1
			
			//check if we arrived
			var px = path_get_point_x(path, pathPos);
			var py = path_get_point_y(path, pathPos);
			if (distance_to_point(px, py) < 2) {
				
				pathPos +=1;
				px = path_get_point_x(path, pathPos);
				py = path_get_point_y(path, pathPos);
				Move_Potential_Step(spr_move, px, py);
				
			} else {
				//didn't arrive yet
				Move_Potential_Step(spr_move, px, py);
			}
			
			if (pathPos == path_get_number(path)) {
				state = state_enum.idle;
				if (path_exists(path)) path_delete(path);
				path = -1;
				pathPos = 1;
			}
		} else {
			//if no path, move the destination a step towards the unit
			//if (place_free(
			
			// if no path, just walk ahead
			Move_Potential_Step(spr_move, target.x, target.y);
		}
		//keep facing the target
		//if (target.x < x) {
		//	image_xscale = -1;
		//} else {
		//	image_xscale = 1;
		//}
	}
	
	
} else {
	state = state_enum.idle;
}

