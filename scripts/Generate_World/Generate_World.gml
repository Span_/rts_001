///@description build room

global.cell_size = 16; //each tile is 16x16 pixels

//Layer Setup:
global.GUI_Top_Layer = layer_get_id("GUI_Top_Layer");
global.GUI_Middle_Layer = layer_get_id("GUI_Middle_Layer");
global.GUI_Bottom_Layer = layer_get_id("GUI_Bottom_Layer");
global.Lighting_Layer = layer_get_id("Lighting_Layer");
global.Effects_Layer = layer_get_id("Effects_Layer");
global.Inst_Units_Layer = layer_get_id("Inst_Units_Layer");
global.Inst_Buildings_Layer = layer_get_id("Inst_Buildings_Layer");
global.Tile_Top_Layer = layer_get_id("Tile_Top_Layer");
global.Tile_Bottom_Layer = layer_get_id("Tile_Bottom_Layer");
global.Background_Layer = layer_get_id("Background_Layer");

enum state_enum { 
	idle,
	attack,
	move,
	attack_move //move to destination, engage nearest thing in range along the way
}

Init_Particle_Effects();


//lay tiles
// -> lay border tiles
// -> tile collision
// -> starting zone
// -> place tile objects

//setup tiles

//water w/ grass

//sprite_list = ds_list_create();
//ds_list_add(sprite_list, 0, 0 , 1 , 
global.sprite_array[0,0] = 1;//top left //position in tile set list
global.sprite_array[0,1] = 2;//top middle
global.sprite_array[0,2] = 3;//top right
global.sprite_array[0,3] = 1 + 1 * 32; //mid left
global.sprite_array[0,4] = 2 + 1 * 32; //mid mid (water)
global.sprite_array[0,5] = 3 + 1 * 32; //mid right
global.sprite_array[0,6] = 1 + 2 * 32; //bot left
global.sprite_array[0,7] = 2 + 2 * 32; //bot mid
global.sprite_array[0,8] = 3 + 2 * 32; //bot right
global.sprite_array[0,9] = 4; //diag tlbr
global.sprite_array[0,10] = 4 + 1 * 32; //diag trbl
global.sprite_array[0,11] = 4 + 2 * 32; //middle (grass)
global.sprite_array[0,12] = 5; //br
global.sprite_array[0,13] = 5 + 1 * 32; //tr
global.sprite_array[0,14] = 6; //bl
global.sprite_array[0,15] = 6 + 1 * 32; //tl

//cliffs
global.sprite_array[1,0] = 19 + 3 * 32;//top left //position in tile set list
global.sprite_array[1,1] = 20 + 3 * 32;//top middle
global.sprite_array[1,2] = 21 + 3 * 32;//top right
global.sprite_array[1,3] = 19 + 4 * 32; //mid left
global.sprite_array[1,4] = 20 + 4 * 32; //mid mid (water)
global.sprite_array[1,5] = 21 + 4 * 32; //mid right
global.sprite_array[1,6] = 19 + 5 * 32; //bot left
global.sprite_array[1,7] = 20 + 5 * 32; //bot mid
global.sprite_array[1,8] = 21 + 5 * 32; //bot right
global.sprite_array[1,9] = 22 + 3 * 32; //diag tlbr
global.sprite_array[1,10] = 22 + 4 * 32; //diag trbl
global.sprite_array[1,11] = 22 + 5 * 32; //middle (grass)
global.sprite_array[1,12] = 23 + 3 * 32; //br
global.sprite_array[1,13] = 23 + 4 * 32; //tr
global.sprite_array[1,14] = 24 + 3 * 32; //bl
global.sprite_array[1,15] = 24 + 4 * 32; //tl

//trees????
global.sprite_array[2,0] = 7 + 3 * 32;//top left //position in tile set list
global.sprite_array[2,1] = 8 + 3 * 32;//top middle
global.sprite_array[2,2] = 9 + 3 * 32;//top right
global.sprite_array[2,3] = 7 + 4 * 32; //mid left
global.sprite_array[2,4] = 8 + 4 * 32; //mid mid (water)
global.sprite_array[2,5] = 9 + 4 * 32; //mid right
global.sprite_array[2,6] = 7 + 5 * 32; //bot left
global.sprite_array[2,7] = 8 + 5 * 32; //bot mid
global.sprite_array[2,8] = 9 + 5 * 32; //bot right
global.sprite_array[2,9] = 10 + 3 * 32; //diag tlbr
global.sprite_array[2,10] = 10 + 4 * 32; //diag trbl
global.sprite_array[2,11] = 10 + 5 * 32; //middle (grass)
global.sprite_array[2,12] = 11 + 3 * 32; //br
global.sprite_array[2,13] = 11 + 4 * 32; //tr
global.sprite_array[2,14] = 12 + 3 * 32; //bl
global.sprite_array[2,15] = 12 + 4 * 32; //tl



//bitwise or these numbers to get the autotiling tile
//11
//11
global.sprite_bool_array[0] = 1;//0001
global.sprite_bool_array[1] = 3;//0011
global.sprite_bool_array[2] = 2;//0010
global.sprite_bool_array[3] = 5;//0101
global.sprite_bool_array[4] = 15;//1111
global.sprite_bool_array[5] = 10;//1010
global.sprite_bool_array[6] = 4;//0100
global.sprite_bool_array[7] = 12;//1100
global.sprite_bool_array[8] = 8;//1000
global.sprite_bool_array[9] = 9;//1001
global.sprite_bool_array[10] = 6;//0110
global.sprite_bool_array[11] = 0;//0000
global.sprite_bool_array[12] = 14;//1110
global.sprite_bool_array[13] = 11;//1011
global.sprite_bool_array[14] = 13;//1101
global.sprite_bool_array[15] = 7;//0111

//some flavor items to sprinkle around the world
global.flavor_sprite_array[0] = 0 + 3 * 32;
global.flavor_sprite_array[1] = 1 + 3 * 32;
global.flavor_sprite_array[2] = 2 + 3 * 32;
global.flavor_sprite_array[3] = 3 + 3 * 32;
global.flavor_sprite_array[4] = 4 + 3 * 32;
global.flavor_sprite_array[5] = 0 + 4 * 32;
global.flavor_sprite_array[6] = 1 + 4 * 32;


//safe area point distance:
global.safe_area_point_distance = 10 * 16;
global.safe_area_x = room_width / 2;
global.safe_area_y = room_height / 2;

var lay_id = global.Tile_Bottom_Layer;
var map_id = layer_tilemap_create(lay_id, 0, 0, Terrain_Tile_Set, room_width, room_height);
var lay_id_top = global.Tile_Top_Layer;
var map_id_top = layer_tilemap_create(lay_id_top, 0, 0, Terrain_Tile_Set, room_width, room_height);
//var lay_id_fog = global.Tile_Top_Layer;
//var map_id_fog = layer_tilemap_create(lay_id_fog, 0, 0, Terrain_Tile_Set, room_width, room_height);

//place grass everywhere
for (var i = 0; i < room_width; i += 16){
	for (var j = 0; j < room_height; j += 16) {
		tilemap_set_at_pixel(map_id, global.sprite_array[0,11], i, j);
	}
}

//place fog everywhere
//for (var i = 0; i < room_width; i += 16){
//	for (var j = 0; j < room_height; j += 16) {
//		tilemap_set_at_pixel(lay_id_fog, 32, i, j);
//	}
//}



//draw water with all the borders
var spriteType = 0;
var sIndex = 0;

//Auto_Tile(spriteType, sIndex, cX, cY, map_id);
//Auto_Tile(spriteType, sIndex, cX+16, cY+16, map_id);
//Auto_Tile(spriteType, sIndex, cX+64, cY+64, map_id);
//Auto_Tile(spriteType, sIndex, cX+80, cY+80, map_id);

//place a round lake
var cX = room_width/4; //center piece to place
var cY = room_height/4;
for (var i = 32; i < room_width - 32; i += 16){
	for (var j = 32; j < room_height - 32; j += 16) {
		if (irandom(99) > 500 * point_distance(i,j,cX,cY) /  room_height ) {
			Auto_Tile(spriteType, sIndex, i, j, map_id);
		}
	}
}
//place a round forest
var cX = room_width*3/4; //center piece to place
var cY = room_height*3/4;
for (var i = 32; i < room_width - 32; i += 16){
	for (var j = 32; j < room_height - 32; j += 16) {
		if (irandom(99) > 500 * point_distance(i,j,cX,cY) /  room_height ) {
			Auto_Tile(2, sIndex, i, j, map_id_top);
		}
	}
}

//var cX = room_width*3/4; //center piece to place
//var cY = room_height/4;
//for (var i = 32; i < room_width - 32; i += 16){
//	for (var j = 32; j < room_height - 32; j += 16) {
//		if (irandom(99) > 500 * point_distance(i,j,cX,cY) /  room_height ) {
//			Auto_Tile(1, sIndex, i, j, map_id);
//		}
//	}
//}

//put cliffs around the edges
for (var i = 16; i < room_width - 16; i += 16){
	for (var j = 16; j < room_height - 16; j += 16) {
		
		if (i < 64 || i > room_width - 64 || j < 64 || j > room_height - 64) {
			Auto_Tile(1, sIndex, i, j, map_id_top);
		}
		
	}
}

//place every element in the set
//for (var i = 0; i < 16 * 32; i += 16){
//	for (var j = 0; j < 16 * 6; j += 16) {
		
//		//place tile at 
//		tilemap_set_at_pixel(map_id_top, i/16 + j / 16 * 32, i, j);
		
//	}
//}

//slap down collision boxes on water tiles & cliffs & forests
for (var i = 0; i < room_width; i += 16){
	for (var j = 0; j < room_height; j += 16) {
		//collision_rectangle( x1, y1, x2, y2, obj, prec, notme );
		for (var k = 0; k < 16; k ++){
			if (k == 11) continue; //skip 11 because it is just grass / empty
			if (tilemap_get_at_pixel(map_id, i, j) == global.sprite_array[spriteType,k]){
				//collision_rectangle(i, j, i + 16, j + 16, Path_Avoid_Obj, false, false );
				//mp_grid_add_cell(global.grid, i / 16, j / 16);
				instance_create_layer(i,j,global.Tile_Bottom_Layer,Terrain_Collide_Obj);
			}
			
			
			if (tilemap_get_at_pixel(map_id_top, i, j) == global.sprite_array[1,k]){
				//cliff collision
				instance_create_layer(i,j,global.Tile_Top_Layer,Terrain_Collide_Obj);
			}
			
			
			if (tilemap_get_at_pixel(map_id_top, i, j) == global.sprite_array[2,k]){
				//cliff collision
				instance_create_layer(i,j,global.Tile_Top_Layer,Terrain_Collide_Obj);
			}
			
		}
	}
}



//sprinkle flavor items around
for (var i = 32; i < room_width - 32; i += 16){
	for (var j = 32; j < room_height - 32; j += 16) {
		//randomly place, as long as they are not near the safe area
		if ((irandom(99) < 5) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance)) {
			
			//place on grass
			//if(tilemap_get_at_pixel(map_id, i, j) == global.sprite_array[0,11] && tilemap_get_at_pixel(map_id_top, i, j) < 0) {
			if(instance_position(i, j,all) == noone) {
			//place a random sprite on top of the terrain layer
				tilemap_set_at_pixel(map_id_top, global.flavor_sprite_array[irandom(array_length_1d(global.flavor_sprite_array)-1)], i, j);
				//create collision for it
				//instance_create_layer(i,j,global.Tile_Bottom_Layer,Terrain_Collide_Obj);
			}
		}
	}
}

//sprinkle minerals around
for (var i = 32; i < room_width - 32; i += 16){
	for (var j = 32; j < room_height - 32; j += 16) {
		//randomly place, as long as they are not near the safe area
		if ((irandom(200) < 1) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance)) {
			
			//place free
			if(instance_position(i,j,all) == noone) {
				//place a random sprite on top of the terrain layer
				instance_create_layer(i+8,j+8,global.Inst_Buildings_Layer,Mineral_Obj);
			}
		}
	}
}

//place instances
// -> starting buildings / units
// -> enemies

//initialize the grid for unit movement
Init_Grid_Path();
//some pathing init instances
mp_grid_add_instances(global.mp_grid, Terrain_Collide_Obj, false);
mp_grid_add_instances(global.mp_grid, Mineral_Obj, true);

//player starting zone:
instance_create_layer(room_width / 2, room_height / 2, global.Inst_Buildings_Layer, Base_Obj);
instance_create_layer(Base_Obj.x, Base_Obj.y + Base_Obj.sprite_height, global.Inst_Units_Layer, Worker_Obj);
instance_create_layer(Base_Obj.x, Base_Obj.y - Base_Obj.sprite_height, global.Inst_Buildings_Layer, Mineral_Obj);
instance_create_layer(room_width / 2 - Base_Obj.sprite_width, room_height / 2, global.Inst_Units_Layer, Soldier_Obj);
instance_create_layer(room_width / 2 + Base_Obj.sprite_width, room_height / 2, global.Inst_Units_Layer, Soldier_Obj);


//place enemy buildings
for (var i = 32; i < room_width - 32; i += 16){
	for (var j = 32; j < room_height - 32; j += 16) {
		
		//place chaos
		if ((irandom(1500) < 1) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance + 300)) {
			
			//place place somewhere free
			if(instance_position(i,j,all) == noone) {
				instance_create_layer(i,j,global.Inst_Buildings_Layer,Chaos_Spawner_Obj);
			}
		}
		
		//place horse mage
		if ((irandom(700) < 1) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance + 300)) {
			
			//place place somewhere free
			if(instance_position(i,j,all) == noone) {
				instance_create_layer(i,j,global.Inst_Buildings_Layer,Horse_Mage_Spawner_Obj);
			}
		}
		
		//place monster doggo
		//randomly place, as long as they are not near the safe area
		if ((irandom(500) < 1) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance + 300)) {
			
			//place place somewhere free
			if(instance_position(i,j,all) == noone) {
				instance_create_layer(i,j,global.Inst_Buildings_Layer,Monster_Doggo_Spawner_Obj);
			}
		}
		
		
		
	}
}



//sprinkle some enemies around
for (var i = 32; i < room_width - 32; i += 16){
	for (var j = 32; j < room_height - 32; j += 16) {
		//randomly place, as long as they are not near the safe area
		if ((irandom(50) < 1) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance + 64)) {
			
			//place place somewhere free
			if(instance_position(i,j,all) == noone) {
				instance_create_layer(i + 8,j + 8,global.Inst_Units_Layer,Monster_Doggo_Obj);
			}
		}
		//place horse mages
		if ((irandom(200) < 1) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance + 64)) {
			
			//place place somewhere free
			if(instance_position(i,j,all) == noone) {
				instance_create_layer(i + 8,j + 8,global.Inst_Units_Layer,Horse_Mage_Obj);
			}
		}
		//place chaos
		if ((irandom(300) < 1) && 
			(point_distance(i,j,global.safe_area_x, global.safe_area_y)
			> global.safe_area_point_distance + 64)) {
			
			//place place somewhere free
			if(instance_position(i,j,all) == noone) {
				instance_create_layer(i + 8,j + 8,global.Inst_Units_Layer,Chaos_Obj);
			}
		}
	}
}


//add objects to avoid
//mp_grid_add_instances(global.mp_grid, Path_Avoid_Obj, true);
//mp_grid_add_instances(global.mp_grid, Building_Obj, true);
mp_grid_add_instances(global.mp_grid, Enemy_Building_Obj, true);


/************* trash pile ******************/

//place a water tile.
//tilemap_set_at_pixel(map_id, sprite_array[0,0], 16, 16);//top left //position in tile set list
//tilemap_set_at_pixel(map_id, sprite_array[0,1], 32, 16);//top middle
//tilemap_set_at_pixel(map_id, sprite_array[0,2], 48, 16);//top right
//tilemap_set_at_pixel(map_id, sprite_array[0,3], 16, 32); //mid left
//tilemap_set_at_pixel(map_id, sprite_array[0,4], 32, 32); //mid mid (water)
//tilemap_set_at_pixel(map_id, sprite_array[0,5], 48, 32); //mid right
//tilemap_set_at_pixel(map_id, sprite_array[0,6], 16, 48); //mid left
//tilemap_set_at_pixel(map_id, sprite_array[0,7], 32, 48); //mid mid
//tilemap_set_at_pixel(map_id, sprite_array[0,8], 48, 48); //mid right

//for (var j = -16; j <= 16; j += 16){
//	for (var i = -16; i <= 16; i += 16){
//		//how the edges touch determine how to draw edges
		
//		var ind = tile_get_index(tilemap_get_at_pixel(map_id,cX + i, cY + j));
//		for (var l = 0; l < array_length_2d(sprite_array,spriteType); l ++){
//			//loop through sprite array and see if var is in there
//			if(ind == sprite_array[spriteType,l]){
//				//we got index l
//				var bVal = sprite_bool_array[sIndex] | sprite_bool_array[l]; //bitwise or them together
//				for (var b = 0; b < array_length_1d(sprite_bool_array); b++ ){
//					//find bVal in b array, set index equal to new sprite
//					if(bVal == sprite_bool_array[b]) {
//						tilemap_set_at_pixel(map_id, sprite_array[spriteType,b], cX + i, cY + j);
//					}
//				}
//			}
//		}
		
//		//tilemap_set_at_pixel(map_id, sprite_array[spriteType,sIndex], cX + i, cY + j);
//		sIndex ++;
//	}
	
//}


//scan room for water & grass contact. 
//replace tiles with contact tile types
//for (var i = 1; i < room_width - 1; i += 16){
//	for (var j = 1; j < room_height - 1; j += 16) {
		
//		//if you find a tile
//		if (tilemap_get_at_pixel(map_id,i,j) == sprite_array[0,4]){
			
//			//for (var ii = i - 16; ii < i + 16; ii += 16){
//			//	for (var jj = j - 16; jj < j + 16; jj += 16) {
//					//get tile at 8 places around it
//					//if any are grass, replace with grass border tile
//			if (tilemap_get_at_pixel(map_id,i - 16, j - 16) == sprite_array[0,11]){
//				tilemap_set_at_pixel(map_id, sprite_array[0,0], i - 16, j - 16);
//			}
				
			
//		}
//	}
//}

////all the same
//sprite_autotile_array[0,0] = sprite_array[0,0];
//sprite_autotile_array[1,1] = sprite_array[0,1];
//sprite_autotile_array[2,2] = sprite_array[0,2];
//sprite_autotile_array[3,3] = sprite_array[0,3];
//sprite_autotile_array[4,4] = sprite_array[0,4];
//sprite_autotile_array[5,5] = sprite_array[0,5];
//sprite_autotile_array[6,6] = sprite_array[0,6];
//sprite_autotile_array[7,7] = sprite_array[0,7];
//sprite_autotile_array[8,8] = sprite_array[0,8];

////
//sprite_autotile_array[0,1] = sprite_array[0,1];
//sprite_autotile_array[0,2] = sprite_array[0,1];
//sprite_autotile_array[0,3] = sprite_array[0,3];
//sprite_autotile_array[0,4] = sprite_array[0,4];
//sprite_autotile_array[0,5] = sprite_array[0,13]; //tr
//sprite_autotile_array[0,6] = sprite_array[0,3];
//sprite_autotile_array[0,7] = sprite_array[0,14];
//sprite_autotile_array[0,8] = sprite_array[0,9];
//var data = tilemap_get(map_id, 1, 0);
//tilemap_set_at_pixel(map_id, data, 0, 0);
//var map_id = layer_tilemap_get_id(lay_id);
//var data = tilemap_get(map_id, 1, 1);//grass tile
//draw_tile(map_id, data, 0, 16, 16);
//tilemap_set(map_id, 1, 1);
//for (var i = 0; i < 100; i ++) {
//	tilemap_set_at_pixel(map_id, i, i * 16, 0);
//}

//var data = tilemap_get(map_id, 4, 1);
//draw_tile(Terrain_Tile_Set, 1, 0, 16, 16);

//draw_tilemap(map_id, 0, 0);
//init room
// -> make layers
//global.thisRoom = room_add();

////set room
//room_set_width(global.thisRoom, 1280);
//room_set_height(global.thisRoom, 720);
//room_set_persistent(global.thisRoom, true);
//room_set_background_color(global.thisRoom, c_dkgray, true);

////set camera
//room_set_camera(global.thisRoom, 0, view_camera[0]);
//camera_set_view_size(view_camera[0], 1280, 720);
//room_set_view_enabled(global.thisRoom, true);
//room_set_viewport(global.thisRoom, 0, true, 0, 0, 640, 360); //change to middle 

//make layers
//global.Background_Layer = layer_create(500);
//global.Tile_Layer_Bottom = layer_create(400);
//global.Tile_Layer_Top = layer_create(300);
//global.Inst_Layer_Buildings = layer_create(200);
//global.Inst_Layer_Projectiles = layer_create(100);
//global.Inst_Layer_Objects = layer_create(0);
//global.Inst_Layer_GUI_Bottom = layer_create(-100);
//global.Inst_Layer_GUI_Top = layer_create(-200);
//var lay_id = layer_get_id("Terrain_Tile_Set");
//var map_id = layer_tilemap_get_id(global.Tile_Layer_Bottom);
//tilemap_tileset(map_id, Terrain_Tile_Set);
//var data = tilemap_get(map_id, 4, 2);//grass tile
//draw_tile(map_id, data, 0, 16, 16);
//instance_create_layer(0,0,global.Inst_Layer_GUI_Bottom,Score_Obj);
//instance_create_layer(0,0,global.Inst_Layer_GUI_Bottom,View_Obj);
//instance_create_layer(0,0,global.Inst_Layer_GUI_Bottom,GUI_Obj);
//instance_create_layer(0,0,global.Inst_Layer_GUI_Top,Mouse_Obj);