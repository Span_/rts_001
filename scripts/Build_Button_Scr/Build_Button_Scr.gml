///Build_Button_Scr(obj,spr,cost);
///@param obj
///@param spr
///@param cost

var obj = argument0;
var spr = argument1;
var cost = argument2;

image_alpha = image_alpha * 2;

//make soldier on left mouse release

//instance_create_layer(200,100,"Instances",Worker_Obj);

//make worker on left mouse release
if (instance_exists(Base_Obj)) {
	
	if (instance_exists(Score_Obj)){
		if (Score_Obj.resources >= cost) {
			if (instance_exists(Mouse_Obj)) {
				//replace mouse sprite with house sprite, 
				//place house on left click,
				Mouse_Obj.sprite_index = spr;
				Mouse_Obj.objectToPlace = obj;
				
				Score_Obj.resources -= cost;
			}
		}
	}
		
}